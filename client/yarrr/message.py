import gtk
import gobject
import person
import document
import xml
import xml.sax.saxutils
import xml.utils.iso8601
import time
import datetime

import yarrr.user

HEADER_COLOR="#d0d0d0"
CHILD_INDENT=16
POLLING_TIME=10000

def escape_text(text):
    return xml.sax.saxutils.escape(text);

def bold_text(text):
    return "<b>" + escape_text(text) + "</b>"

class MessageWidget(gtk.VBox):
    __gsignals__ = { 
        'reply-clicked' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_STRING,))
    }
    
    def __init__ (self, server, message_id, author, subject, date, document_id, propertyhandler):
        gtk.VBox.__init__ (self, False, 0)

        self.message_id = message_id

        eventbox = gtk.EventBox()
        eventbox.show()
        self.pack_start(eventbox)
        self.header = eventbox
        
        vbox = gtk.VBox()
        vbox.show()
        eventbox.add(vbox)
        
        hbox = gtk.HBox(False, 0)
        hbox.show()
        
        subject_label = gtk.Label()
        subject_label.set_markup(bold_text(subject))
        subject_label.show()
        hbox.pack_start(subject_label)

        #TODO: get icons from propertyhandler and add them to hbox
        image = gtk.Image()
        image.set_from_stock("gtk-dialog-warning", gtk.ICON_SIZE_BUTTON)
        image.show()
        hbox.pack_start(image, False, False, 16)

        expander = gtk.Expander()
        expander.set_expanded(True)
        expander.connect ('notify::expanded', self.expander_changed)
        expander.set_label_widget (hbox)
        expander.show()
        
        vbox.pack_start (expander)

        hbox = gtk.HBox(False, 0)
        hbox.show()
        self.extra_hbox = hbox;
        vbox.pack_start(hbox)
        
        label = gtk.Label("by ");
        label.show()
        label.set_alignment(0.0,0.5)
        hbox.pack_start (label, False, False, 0)

        personwidget = person.PersonWidget(author)
        personwidget.show()
        hbox.pack_start (personwidget, False, False, 0)

        date_text = self.xmlrpcdate_to_string (date)
        label = gtk.Label()
        label.set_markup(" on <i>" + date_text + "</i>" )
        label.show()
        label.set_alignment(0.0,0.5)
        hbox.pack_start (label, True, True, 0)
        
        button = gtk.Button("Reply")
        button.show()
        hbox.pack_end(button, False, False, 0)
        button.connect('clicked', self.reply_clicked)
        
        documentwidget = document.DocumentWidget(server, document_id, 1)
        documentwidget.show()
        self.documentwidget = documentwidget
        self.pack_start(documentwidget)

        alignment = gtk.Alignment()
        alignment.set_padding(0, 0, CHILD_INDENT, 0)
        alignment.show()
        self.pack_start(alignment)

        child_vbox = gtk.VBox(False, 0)
        alignment.add(child_vbox)
        child_vbox.show()
        self.child_vbox = child_vbox
        
        self.expander_changed(expander, None)
        
    def reply_clicked(self, button):
        self.emit('reply-clicked', self.message_id)

    def expander_changed(self, expander, param):
        if expander.get_expanded():
            self.header.modify_bg(gtk.STATE_NORMAL, gtk.gdk.color_parse(HEADER_COLOR))
            self.extra_hbox.show()
            self.documentwidget.show()
        else:
            self.header.modify_bg(gtk.STATE_NORMAL, None)
            self.extra_hbox.hide()
            self.documentwidget.hide()
        
    def set_expanded(self, expanded):
        self.expander.set_expended(expanded)

    def add_child(self, child):
        self.child_vbox.pack_start(child, False, False, 0)

    def xmlrpcdate_to_string (self, xmlrpcdate):
        timestamp = time.mktime(time.strptime (xmlrpcdate.value, "%Y%m%dT%H:%M:%S"))
        dt = datetime.datetime.fromtimestamp (timestamp);
        date = datetime.date.fromtimestamp (timestamp);
        today = datetime.date.today ()
        if date == today:
            return dt.strftime ('%I:%M')
        else:
            return dt.strftime ('%d %B')

class MessageListWidget(gtk.ScrolledWindow):
    __gsignals__ = { 
        'reply-clicked' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_STRING,))
    }
    def __init__ (self):
        gtk.ScrolledWindow.__init__(self)
        self.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        self.vbox = gtk.VBox (False, 0)
        self.vbox.show()
        self.add_with_viewport(self.vbox)
        self.messages = {}
        self.updating = False
        self.timeout = None
        self.update_again = False
        self.version = '0'
        self.user_version = '0'
        self.request = None
        
        self.summary = document.DocumentWidget()
        self.summary.show()
        self.vbox.pack_start (self.summary, False, False, 0)

    def set_topic(self, topic):
        self.reset_update()
        self.remove_all()
        if topic:
            self.topic = topic.get_id()
            self.server = topic.get_server()
            self.summary_id = topic.get_summary_id()
            self.update()
        else:
            self.topic = None
            self.server = None
            self.summary_id = None
 
    def remove_all(self):
        for m in self.messages.values():
            m.destroy()
        self.messages = {}
        self.summary.clear()

    def add_messages(self, children):
        for message in children:
            message_id = message['id']
            subject = message['subject']
            author_key = message['author'].data
            document_id = message['documentID']
            sent_time = message['sentTime']
                
            author = person.get_person(author_key, self.server)
            m = MessageWidget(self.server, message_id, author, subject, sent_time, document_id, None)
            m.show()
            m.connect('reply-clicked', self.child_reply_clicked)
                
            self.messages[message_id] = m
                
            parent = None
            if 'replyTo' in message:
                reply_to = message['replyTo']
                if reply_to in self.messages:
                    parent = self.messages[reply_to]

            if parent != None:
                parent.add_child(m)
            else:    
                self.add_message(m)
       
    def update_timeout(self):
        self.timeout = None
        self.update()
        
    def got_topic_updates(self, res):
        (self.version, self.user_version, new_messages, new_links,
         changed_messages, changed_user_data, removed_messages) = res

        self.add_messages(new_messages)
        self.update_completed ()

    def clear_scheduled_update (self):
        if self.timeout != None:
            gobject.source_remove(self.timeout)
            self.timeout = None

    def reset_update (self):
        self.last_index = -1
        self.updating = False
        self.update_again = False
        self.clear_scheduled_update ()
        if self.request:
            self.request.cancel ()
            self.request = None

    def update_completed (self):
        self.request = None
        self.updating = False
        if self.update_again:
            self.update_again = False
            self.update ()
        else:
            self.timeout = gobject.timeout_add (POLLING_TIME, self.update)

    def update_summary(self):
        if self.topic != None and self.server != None:
            self.summary.set_document (self.server, self.summary_id)
 
    def update(self):
        self.update_summary()
        
        self.clear_scheduled_update ()
        if self.updating:
            self.update_again = True
            return

	public_key = yarrr.user.get_user ().get_public_key ()
        self.request = self.server.update_topic (self.topic, public_key.data, self.version,
                                                 self.user_version, self.got_topic_updates)
        self.updating = True

    def child_reply_clicked(self, child, message_id):
        self.emit('reply-clicked', message_id)
        
    def add_message(self, message):
        self.vbox.pack_start(message, False, False, 0)

gobject.type_register(MessageWidget)
gobject.type_register(MessageListWidget)
