from async_server import AsyncServer

class CachedServer (AsyncServer):

    def __init__ (self, server):
        AsyncServer.__init__ (self, server)
        self.message_cache = {}

    def update_topic_children (self, id, last_seen_index, callback, data=None):
        # TODO cache this
        return AsyncServer.update_topic_children (self, id, last_seen_index, callback, data)

    def cache_resource (self, resource, user_data):
        (document_id, callback, data) = user_data
        self.message_cache [document_id] = resource
        if data:
            callback (resource, data)
        else:
            callback (resource)

    def get_resource (self, document_id, resource, version, callback, data=None):
        # Don't cache if no version specified, we don't know if there is a new version
        if version != None and self.message_cache.has_key (document_id):
            resource = self.message_cache [document_id]
            if data:
                callback (resource, data)
            else:
                callback (resource)
            return None

        cb = self.cache_resource;
        data = (document_id, callback, data)
        return AsyncServer.get_resource (self, document_id, resource, version, cb, data)
