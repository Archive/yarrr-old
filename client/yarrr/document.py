import gtk
import xmlrpclib

class DocumentWidget(gtk.Label):
    def __init__(self, server = None, document_id = None, version = None):
        gtk.Label.__init__(self)
        self.server = server
        self.document_id = document_id
        self.version = version
        self.set_line_wrap(True)
        self.set_alignment(0.0,0.5)
        self.set_padding(4, 0)

        self.connect ('destroy', self.on_destroy)
        self.request = None
        if document_id:
            self.update()

    def update(self):
        self.set_text("")
        if self.server == None:
            return
        version = self.version
        if self.document_id != None:
            if version != None:
                self.request = self.server.get_resource (self.document_id, "main", version, self.got_resource)
            else:
                self.request = self.server.get_resource_and_version (self.document_id, "main", self.got_resource_and_version)
        
    def cancel_request(self):
        if self.request:
            self.request.cancel () 
            self.request = None
        
    def on_destroy(self, widget):
        self.cancel_request()

    def got_resource(self, resource):
        self.set_text(resource.data)

    def got_resource_and_version(self, data):
        (version, resource) = data
        self.version = version
        self.set_text(resource.data)

    def set_server(self, server):
        self.server = server
        
    def clear(self):
        self.set_text("")
        
    def set_document(self, server, document_id, version = None):
        self.server = server
        self.cancel_request()
        self.document_id = document_id
        self.version = version
        self.update()
        
class DocumentEditor(gtk.TextView):
    def __init__(self, version = None):
        gtk.TextView.__init__(self)
        self.version = version
        self.document_id = None
        self.set_data("")
        
    def set_data(self, data):
        buffer = self.get_buffer()
        buffer.set_text(data)

    def got_document (self, result):
        (version, content) = result
        self.version = version
        self.set_data(content.data)
        self.set_editable(True)

    def get_from_document(self, server, document_id):
        self.set_data("")
        self.set_editable(False)
        self.document_id = document_id
        self.request = server.get_resource_and_version (document_id, "main", self.got_document)

    def get_version(self):
        return self.version
        
    def get_data(self):    
        buffer = self.get_buffer ()
        return buffer.get_text (buffer.get_start_iter(),
                                buffer.get_end_iter())
                                
    def saved_data(self, res, callback):
        callback()
        
    def save_data(self, server, callback):
        assert(self.document_id != None)
        server.update_resource(self.document_id, "main", self.version, xmlrpclib.Binary (self.get_data()), self.saved_data, callback)
