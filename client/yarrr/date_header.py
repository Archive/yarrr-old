
import gtk
import datetime
from gettext import gettext as _

class DateHeader (gtk.HBox):

    def __init__ (self):
        gtk.HBox.__init__ (self, False, 12)

        # Set up the widgetry
        self.back_button = gtk.Button ()
        arrow = gtk.Arrow (gtk.ARROW_LEFT, gtk.SHADOW_NONE)
        self.back_button.add (arrow)
        self.back_button.set_relief (gtk.RELIEF_NONE)
        self.pack_start (self.back_button, False, False, 0)
        
        self.label = gtk.Label ()
        self.label.set_alignment (0.0, 0.5)
        self.pack_start (self.label, True, True, 0)

        self.next_button = gtk.Button ()
        arrow = gtk.Arrow (gtk.ARROW_RIGHT, gtk.SHADOW_NONE)
        self.next_button.add (arrow)
        self.next_button.set_relief (gtk.RELIEF_NONE)
        self.pack_end (self.next_button, False, False, 0)

        # connect signals and set a default day
        self.set_day ()
        self.back_button.connect ('clicked', self.go_back_one_day)
        self.next_button.connect ('clicked', self.go_forward_one_day)


    def go_back_one_day (self, *args):
        day = self.get_day ()
        self.set_day (day - datetime.timedelta (1))
        

    def go_forward_one_day (self, *args):
        day = self.get_day ()
        self.set_day (day + datetime.timedelta (1))
    
    def set_day (self, day=None):
        """sets the current day range to be 'day'.  If day is None,
        the current day is used.  We expect day to be a tuple the
        format (year, month, date)"""

        if day == None:
            day = datetime.date.today ()

        today = datetime.date.today ()

        if day == today:
            daystr = _("Today")
        elif day + datetime.timedelta(1) == today:
            daystr = _("Yesterday")
        elif today + datetime.timedelta(1) == day:
            daystr = _("Tomorrow")
        else:
            # include the year iff it's a different year
            if day.year != today.year:
                format_str = "%A, B %d  %Y"
            else:
                format_str = "%A, %B %d"
            daystr = day.strftime (format_str)

        self.day = day

        # update the widgetry
        self.label.set_markup ("<span size=\"xx-large\" weight=\"bold\">%s</span>" % daystr)

    def get_day (self):
        return self.day
