import gconf
import xmlrpclib

import yarrr.servers
from keypair import KeyPair

class User:

    def __init__ (self, server):
        self.key_pair = KeyPair ()
        self.server = server
        
        if not self.key_pair.load ():
            key = server.getPersonKeyPair()
            self.key_pair.set_public (key['publicKey'])
            self.key_pair.set_private (key['privateKey'])
            self.key_pair.save ()

    def get_name (self):
        client = gconf.client_get_default ()
        name = client.get_string ("/apps/yarrr/user_name");
        return name

    def get_public_key (self):
        return self.key_pair.get_public ()

user = None

def get_user ():
    global user
    if not user:
        # FIXME Use the first server in the list to generate
        # key pair. This should not require a server at all
        addresses = yarrr.servers.get_addresses ()
        server = xmlrpclib.Server (addresses[0])
        user = User (server)
    return user
