import gtk
import gtk.glade
import xmlrpclib

import yarrr.user

class TopicCreator:

    def __init__ (self, topic, callback=None):
        self.xml = gtk.glade.XML ("yarrr.glade")
        self.parent_topic = topic
        self.setup ()
        self.callback = callback

    def get_subject (self):
        entry = self.xml.get_widget ('subject_entry')
        return entry.get_text ()

    def get_message (self):
        text_view = self.xml.get_widget ('message_textview')
        buffer = text_view.get_buffer ()
        return buffer.get_text (buffer.get_start_iter(),
                                buffer.get_end_iter())

    def add_topic (self):
        user = yarrr.user.get_user ()

        topic = {}
        topic['name'] = self.subject
        topic['author'] = user.get_public_key ()
        topic['allowMessages'] = True

        server = self.parent_topic.get_server ()
        desc = xmlrpclib.Binary ('');
        id = self.parent_topic.get_id ()
        server.add_topic (topic, desc, self.got_topic_id, parentId=id)

    def got_topic_id (self, id):
        user = yarrr.user.get_user ()
        message = {}
        message['authorName'] = user.get_name ()
        message['subject'] = self.subject
        message['mimetype'] = 'text/plain'
        message['author'] = user.get_public_key ()
        server = self.parent_topic.get_server ()
        server.add_message (id, message, xmlrpclib.Binary (self.text))
        if self.callback ():
            self.callback ()

    def on_send (self, widget):
        self.subject = self.get_subject ()
        self.text = self.get_message ()
        self.add_topic ()
        self.window.destroy ()

    def setup (self):
        self.window = self.xml.get_widget ('new_message_window')
        button = self.xml.get_widget ('send_button')
        button.connect('clicked', self.on_send)
        self.window.show ()
