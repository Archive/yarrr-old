import httplib, xmlrpclib

from yarrr.cached_server import CachedServer

class PersistTransport(xmlrpclib.Transport):
     '''Provides a Transport for the xmlrpclib that uses httplib supporting persistent connections
     Does not close the connection after each request.
     '''
     connection = None

     def request(self, host, handler, request_body, verbose=0):
         if not self.connection:
             host, extra_headers, x509 = self.get_host_info(host)
             self.connection = httplib.HTTPConnection(host)
             self.headers = {"User-Agent" : self.user_agent,
                "Content-Type" : "text/xml",
                "Accept": "text/xml"}
             if extra_headers:
                 for key, item in extra_headers:
                     self.headers[key] = item

         self.headers["Content-Length"] = str(len(request_body))
         self.connection.request('POST', handler, request_body, self.headers)
         r = self.connection.getresponse()
         if r.status != 200:
             self.connection.close()
             self.connection = None
             raise xmlrpclib.ProtocolError( host + handler, r.status, r.reason, '' )
         data = r.read()
         p, u = self.getparser()
         p.feed(data)
         p.close()
         return u.close()

servers = {}

def add (address):
    servers[address] = None

def get_all ():
    list = []
    for address in servers.keys ():
        list.append (get_from_address (address))
    return list

def get_addresses ():
    return servers.keys ()

def get_from_address (address):
    if servers.has_key (address):
        if not servers[address]:
            server = xmlrpclib.ServerProxy(address, transport=PersistTransport())
            #server = xmlrpclib.Server (address)
            async_server = CachedServer (server)
            servers[address] = async_server
            async_server.start ()
            return async_server
        else:
            return servers[address]
    else:
        return None
