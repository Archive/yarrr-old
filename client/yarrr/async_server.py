import gobject
import threading
import time
import Queue
import xmlrpclib

import yarrr.debug

MAX_QUEUE_SIZE = 100

last_request_id = 0

class AsyncServerRequest:

    def __init__ (self, method):
        self.method = method
        self.callback = None
        self.args = []
        self.data = None
        self.result = None
        self.cancelled = False

    def is_cancelled (self):
        return self.cancelled

    def cancel (self):
        self.cancelled = True

    def get_method (self):
        return self.method

    def get_callback (self):
        return self.callback

    def set_callback (self, callback):
        self.callback = callback

    def get_arguments (self):
        return self.args

    def add_argument (self, argument):
        self.args.append (argument)
        
    def set_data (self, data):
        self.data = data
        
    def get_data (self):
        return self.data
        
    def set_result (self, result):
        self.result = result
        
    def get_result (self):
        return self.result

    def set_name (self, name):
        self.name = name

    def get_name (self):
        return self.name

class AsyncServer (threading.Thread):

    def __init__ (self, server):
        threading.Thread.__init__ (self)
        self.setDaemon (True)
        self.server = server
        self.queue = Queue.Queue (MAX_QUEUE_SIZE)
        self.debug = True

    def run_request (self, name, *args):
        """A generic marshaller for rpc calls.  The last two arguments
        are assumed to be callback and data.  They can be None if they
        aren't wanted or needed."""
        if len (args) < 2:
            raise Exception
        method = self.server.__getattr__(name)
        request = AsyncServerRequest (method)
        request.set_name (name)
        for arg in args[:-2]:
            request.add_argument (arg)
        request.set_callback (args[-2])
        request.set_data (args[-1])
        self.queue.put (request)
        return request

    def heartbeat (self, public_key):
        return self.run_request ('heartbeat', public_key, None, None)

    def get_topic (self, id, callback, data = None):
        return self.run_request ('getTopic', id, callback, data)

    def get_active_people (self, callback, data = None):
        return self.run_request ('getActivePeople', callback, data)

    def get_resource (self, document_id, resource, version, callback, data=None):
        return self.run_request ('getResource', document_id, resource, version, callback, data)

    def get_resource_and_version (self, document_id, resource, callback, data=None):
        return self.run_request ('getResourceAndVersion', document_id, resource, callback, data)
        
    def update_resource (self, document_id, resource, based_on_version, content, callback, data=None):
        return self.run_request ('updateResource', document_id, resource, based_on_version, content, callback, data)

    def create_topic (self, name, public_key, callback, data=None):
        return self.run_request ('createTopic', name, xmlrpclib.Binary (public_key), callback, data)

    def post_message (self, topic_id, subject, reply_to, poster_public_key, content, callback, data=None):
        return self.run_request ('postMessage', topic_id, subject, reply_to, xmlrpclib.Binary (poster_public_key), content, callback, data)

    def add_link (self, source_topic_id, target_topid_id, callback, data=None):
        return self.run_request ('addLink', source_topic_id, target_topid_id, callback, data)

    def inject_email (self, contents, callback, data=None):
        return self.run_request ('injectEmail', contents, callback, data)

    def update_topic (self, topic_id, public_key, version_at_client, per_user_version_at_client, callback, data=None):
        return self.run_request ('updateTopic', topic_id, xmlrpclib.Binary(public_key), version_at_client, per_user_version_at_client, callback, data)

    def update_per_user_data (self, topic_id, public_key, message_per_user, callback, data=None):
        return self.run_request ('updatePerUserData', topic_id, public_key, message_per_user, callback, data)

    def update_topic_desription (self, public_key, callback, data=None):
        return self.run_request ('updateTopicDescription', xmlrpclib.Binary (public_key), callback, data=None)

    def get_messages (self, topic_ids, start, end, callback, data=None):
        return self.run_request ('getMessages', topic_ids, start, end, callback, data)

    def get_person_key_pair (self, callback, data=None):
        return self.run_request ('getPersonKeyPair', callback, data)

    def set_person_attribute (self, public_key, attribute_name, value, callback, data=None):
        return self.run_request ('setPersonAttribute', xmlrpclib.Binary (public_key), attribute_name, value, callback, data)

    def get_person_attribute (self, public_key, attribute_name, callback, data=None):
        return self.run_request ('getPersonAttribute', xmlrpclib.Binary (public_key), attribute_name, callback, data)

    def get_person_attributes (self, public_key, callback, data=None):
        return self.run_request ('getPersonAttributes', xmlrpclib.Binary (public_key), callback, data)

    def request_finished (self, request):
        if request.is_cancelled ():
            return
        data = request.get_data()
        callback = request.get_callback ()
        if data != None:
            callback (request.get_result(), request.get_data())
        else:
            callback (request.get_result())

    def execute_request (self, request):
        args = request.get_arguments ()
        name = request.get_name ()
        yarrr.debug.log ('Execute request ' + name + '... ')
        start_time = time.time ()
        result = request.get_method () (*args)
        end_time = time.time ()
        yarrr.debug.log ('Completed in %g \n' % (end_time - start_time))
        if request.get_callback ():
            request.set_result(result)
            gobject.idle_add (self.request_finished, request)

    def run (self):
        while True:
            request = self.queue.get ()
            if not request.is_cancelled ():
                self.execute_request (request)
