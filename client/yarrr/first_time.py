import gobject
import gtk
import gtk.glade
import gconf
import preferences

class FirstTime:

    def __init__ (self):
        self.xml = gtk.glade.XML ("yarrr.glade", "first_time_dialog")
        self.setup ()

    def fill_servers_list (self):
        liststore = gtk.ListStore (gobject.TYPE_STRING,
                                   gobject.TYPE_STRING)
        self.server_combo.set_model (liststore)
        cell = gtk.CellRendererText ()
        self.server_combo.pack_start (cell, gtk.TRUE)
        self.server_combo.add_attribute (cell, 'text', 0)

        for server in preferences.servers:
            iter = liststore.append ((server[0], server[1]))
            if server[2]:
                self.server_combo.set_active_iter (iter)

    def setup (self):
        self.dialog = self.xml.get_widget ('first_time_dialog')
        self.server_combo = self.xml.get_widget ('first_time_server_combo')
        self.name_entry = self.xml.get_widget ('first_time_name_entry')
        self.email_entry = self.xml.get_widget ('first_time_email_entry')
        self.ok_button = self.xml.get_widget ('first_time_ok_button')

        self.name_entry.connect ('changed', self.update_sensitivity)
        self.email_entry.connect ('changed', self.update_sensitivity)
        self.update_sensitivity ()
        self.fill_servers_list ()

    def update_sensitivity (self, *args):
        if (self.name_entry.get_text () == '' or
            self.email_entry.get_text() == ''):
            self.ok_button.set_sensitive (False)
        else:
            self.ok_button.set_sensitive (True)

    def run (self):
        response = self.dialog.run ()
        if response == gtk.RESPONSE_OK:
            print 'RESPONSE_OK!'
            client = gconf.client_get_default ()
            name = self.name_entry.get_text ()
            email = self.email_entry.get_text ()

            index = self.server_combo.get_active ()
            liststore = self.server_combo.get_model ()
            address = liststore[index][1]

            client.set_string ("/apps/yarrr/user_name", name);
            client.set_string ("/apps/yarrr/user_email", email);
            client.set_string ("/apps/yarrr/server_address", address);

        self.dialog.destroy ()
        return response
            

        
