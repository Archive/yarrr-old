
class Topic:
    def __init__ (self, server, info):
        self.info = info
        self.server = server

    def get_server (self):
        return self.server

    def get_id (self):
        return self.info['id']
        
    def get_summary_id (self):
        if 'descriptionID' in self.info:
            return self.info['descriptionID']
        return None
