import gobject
import gtk
import gtk.glade
import gconf

servers = ( ('Localhost', 'http://localhost:8080/yarrr/yarrr', False),
            ('Localhost (rpc)', 'http://localhost:19842/', False),
            ('PeopleSmiths', "http://www.peoplesmiths.com:8080/yarrr/yarrr", True) )

class Preferences:

    def __init__ (self):
        self.xml = gtk.glade.XML ("yarrr.glade")
        self.setup ()

    def on_close (self, widget):
        self.save_prefs ()
        self.window.destroy ()        

    def fill_servers_list (self):
        liststore = gtk.ListStore (gobject.TYPE_STRING,
                                   gobject.TYPE_STRING)
        self.server_combo.set_model (liststore)
        cell = gtk.CellRendererText ()
        self.server_combo.pack_start (cell, gtk.TRUE)
        self.server_combo.add_attribute (cell, 'text', 0)

        for server in servers:
            iter = liststore.append ((server[0], server[1]))

    def save_prefs (self):
        client = gconf.client_get_default ()
        name = self.name_entry.get_text ()
        email = self.email_entry.get_text ()

        index = self.server_combo.get_active ()
        liststore = self.server_combo.get_model ()
        address = liststore[index][1]

        client.set_string ("/apps/yarrr/user_name", name);
        client.set_string ("/apps/yarrr/user_email", email);
        client.set_string ("/apps/yarrr/server_address", address);

    def load_prefs (self):
        client = gconf.client_get_default ()
	name = client.get_string ("/apps/yarrr/user_name");
        email = client.get_string ("/apps/yarrr/user_email");
        address = client.get_string ("/apps/yarrr/server_address"); 

        if name:
            self.name_entry.set_text (name)
        if email:
            self.email_entry.set_text (email)

        liststore = self.server_combo.get_model ()
        for row in liststore:
            if row[1] == address:
                self.server_combo.set_active_iter (row.iter)

    def setup (self):
        self.window = self.xml.get_widget ('preferences_window')
        self.server_combo = self.xml.get_widget ('server_combobox')
        self.name_entry = self.xml.get_widget ('name_entry')
        self.email_entry = self.xml.get_widget ('email_entry')

        button = self.xml.get_widget ('close_button')
        button.connect('clicked', self.on_close)
        self.fill_servers_list ()
        self.load_prefs ()
        self.window.show ()
