import gtk
import gtk.glade
import pango
import gobject
import xmlrpclib

import yarrr.user

class TextStyle:
    def __init__ (self, buffer, index, name, markup, after, is_paragraph):
        self.index = index
        self.name = name
        self.markup = markup
        self.after = after
        if not self.after:
            self.after = name
        self.is_paragraph = is_paragraph

        self.tag = buffer.create_tag (name)

    def index_cmp (style_a, style_b):
        return cmp (style_a.index, style_b.index)

class SendMessage:

    def __init__ (self, topic, callback, reply_to = ""):
        self.xml = gtk.glade.XML ("yarrr.glade")
        self.parent_topic = topic
        self.callback = callback
        self.reply_to = reply_to
        self.styles = None
        self.setup ()

    def get_subject (self):
        entry = self.xml.get_widget ('subject_entry')
        return entry.get_text ()

    def get_message (self):
        text_view = self.xml.get_widget ('message_textview')
        buffer = text_view.get_buffer ()
        return buffer.get_text (buffer.get_start_iter(),
                                buffer.get_end_iter())

    def message_sent (self, id):
        self.callback ()

    def add_message (self):
        user = yarrr.user.get_user ()
        text = self.get_message ()
	content = {"main":xmlrpclib.Binary (text)}
        server = self.parent_topic.get_server ()
        id = self.parent_topic.get_id ()
        server.post_message (id, self.get_subject (), self.reply_to,
                             user.get_public_key ().data, content,
                             self.message_sent)

    def apply_style_to_range (self, style, start, end):
        buffer = self.text_view.get_buffer ()
        start.order (end)
        if style.is_paragraph:
            ## extend range to cover the paragraph
            if not start.starts_line ():
                start.set_line_offset (0)
            if not end.starts_line ():
                end.forward_line ()
        buffer.remove_all_tags (start, end)
        buffer.apply_tag_by_name (style.name,
                                  start, end)

    def set_current_style (self, name):
        self.current_style = self.styles[name]
        self.style_combo.set_active (self.current_style.index)
        
    def on_send (self, widget):
	self.add_message ()
        self.window.destroy ()

    def on_style_selected (self, widget):
        store = widget.get_model ()
        row = store[widget.get_active()]
        self.set_current_style (row[1])

        buffer = self.text_view.get_buffer ()

        selection = buffer.get_selection_bounds ()
        if len (selection) > 0:
            self.apply_style_to_range (self.current_style,
                                       selection[0], selection[1])
        
        self.text_view.grab_focus ()

    def on_insert_text (self, buffer, iter, text, length):
        # we are connected after, so the text was already inserted and
        # the iterator points just after the insertion point
        end = iter.get_offset ()
        start = end - len (text) ### probably busted in unicode... not sure of encoding issues here
        start_iter = buffer.get_iter_at_offset (start)
        end_iter = buffer.get_iter_at_offset (end)
        self.apply_style_to_range (self.current_style,
                                   start_iter, end_iter)

        ## switch to the next style if we're on a new paragraph
        if iter.starts_line ():
            self.set_current_style (self.current_style.after)

    def on_mark_set (self, buffer, iter, mark):
        ## we don't want to apply styles as we drag the selection
        if mark.get_name () == "insert" and len (buffer.get_selection_bounds()) == 0:
            tags = iter.get_tags ()
            if len (tags) > 0:
                tag_name = tags[0].get_property ("name")
                self.set_current_style (tag_name)
            else:
                self.set_current_style ("body")

    def get_styles (self, buffer):
        if self.styles:
            return self.styles

        self.styles = {}

        style = TextStyle (buffer, 0, "body",
                           "Body", None, 1)
        style.tag.set_property ("left-margin", 10)
        style.tag.set_property ("pixels-below-lines", 10)

        self.styles[style.name] = style
        
        style = TextStyle (buffer, 1, "title",
                           "<b><big>Title</big></b>",
                           "body", 1)
        style.tag.set_property ("weight", pango.WEIGHT_BOLD)
        style.tag.set_property ("scale", pango.SCALE_LARGE)
        style.tag.set_property ("justification", gtk.JUSTIFY_CENTER)
        style.tag.set_property ("pixels-below-lines", 15)

        self.styles[style.name] = style

        style = TextStyle (buffer, 2, "heading",
                           "<b><i>Heading</i></b>",
                           "body", 1)
        style.tag.set_property ("weight", pango.WEIGHT_BOLD)
        style.tag.set_property ("style", pango.STYLE_ITALIC)
        style.tag.set_property ("pixels-below-lines", 15)

        self.styles[style.name] = style

        return self.styles

    def setup (self):
        self.window = self.xml.get_widget ('new_message_window')
        button = self.xml.get_widget ('send_button')
        button.connect('clicked', self.on_send)

        text_view = self.xml.get_widget ('message_textview')
        self.text_view = text_view
        buffer = text_view.get_buffer ()

        styles = self.get_styles (buffer)
        styles_in_order = styles.values ()
        styles_in_order.sort (TextStyle.index_cmp)

        style_store = gtk.ListStore (gobject.TYPE_STRING, gobject.TYPE_STRING)
        for style in styles_in_order:
            style_store.append ((style.markup, style.name))

        cell = gtk.CellRendererText ()

        style_combo = self.xml.get_widget ('style_combo')
        self.style_combo = style_combo
        style_combo.set_model (style_store)

        style_combo.pack_start (cell, gtk.TRUE)
        style_combo.add_attribute (cell, 'markup', 0)

        style = styles["body"]
        self.current_style = style
        style_combo.set_active (style.index)

        style_combo.connect ('changed', self.on_style_selected)

        buffer.connect_after ('insert_text', self.on_insert_text)
        buffer.connect_after ('mark_set', self.on_mark_set)

        self.window.show ()
