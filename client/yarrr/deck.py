import gobject
import gtk
import yarrr.user
import yarrr.servers

PERSON_COLUMN=0
PERSON_POLLING_TIME=40*1000

class Deck(gtk.TreeView):

    def __init__ (self):
	gtk.TreeView.__init__(self)
        self.setup ()
        self.update_person_list ()
	gobject.timeout_add (PERSON_POLLING_TIME, self.update_person_list)

    def got_people (self, people):
        # FIXME clearing here works only because we have one server
        self.model.clear ()
        for person in people:
            iter = self.model.prepend ()
            self.model[iter][PERSON_COLUMN] = person
        return 0

    def update_person_list (self):
        user = yarrr.user.get_user ()

        servers = yarrr.servers.get_all ()
        for server in servers:
            server.get_active_people (self.got_people)
            server.heartbeat (user.get_public_key ())

    def update_name_cell (self, column, cell, model, iter):
        message = model[iter][PERSON_COLUMN]
	if message and message.has_key('name'):
            name = message['name']
        else:
	    name = '<i>Anonymous</i>'
        markup = "%s" % name
        cell.set_property ('markup', markup)
        cell.set_property ('yalign', 0.0)

    def setup (self):
        self.model = gtk.ListStore (object)
        self.set_model (self.model)
        cell = gtk.CellRendererText ()
        cell.yalign = 0.0
        column = gtk.TreeViewColumn ('Deck', cell)
        column.set_cell_data_func (cell, self.update_name_cell)
        self.append_column (column)
