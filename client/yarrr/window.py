#!/usr/bin/env python

import gtk
import xmlrpclib
import urlparse

from yarrr.send_message import SendMessage
from yarrr.preferences import Preferences
from yarrr.message import MessageWidget
from yarrr.message import MessageListWidget
from yarrr.document import DocumentWidget
from yarrr.document import DocumentEditor
from yarrr.topic import Topic
from yarrr.date_header import DateHeader

import yarrr.user

ui_description = '''
<ui>
    <menubar name="MenuBar">
      <menu action="File">
        <menuitem action="NewMessage"/>
        <menuitem action="NewTopic"/>
        <separator/>
        <menuitem action="Subscribe"/>
        <menuitem action="Unsubscribe"/>
        <separator/>
        <menuitem action="Close"/>
     </menu>
      <menu action="Edit">
        <menuitem action="Preferences"/>
      </menu>
    </menubar>
    <toolbar name="ToolBar">
      <toolitem action="NewMessage"/>
      <toolitem action="NewTopic"/>
      <separator/>
      <toolitem action="Subscribe"/>
      <toolitem action="Unsubscribe"/>
      <toolitem action="Summarize"/>
    </toolbar>
</ui>
'''

class Window(gtk.Window):

    def __init__ (self):
        gtk.Window.__init__ (self)
        self.setup ()

    def open (self, uri):
        # urlparse seem to work only with known schemes
        http_uri = uri.replace ('yarrr', 'http', 1)
        (scheme, netloc, path, query, fragment) = urlparse.urlsplit (http_uri)
        address = scheme + '://' + netloc + path
        for param in query.split ('&'):
            pair = param.split ('=')
            if pair[0] == 'topic_id':
                topic_id = pair[1]
        yarrr.servers.add (address)
        server = yarrr.servers.get_from_address (address)
        server.get_topic (topic_id, self.got_topic, server)

    def got_topic (self, topic, server): 
        self.topic = Topic (server, topic)
        self.message_list.set_topic (self.topic)

    def on_folder_selection (self, selection):
        (address, topic) = self.folder_view.get_selected ()

    def update_summary(self):
        self.message_list.update_summary ()

    def on_topic_selection (self, selection):
        topic = self.list_view.get_selected ()
        self.message_list.set_topic (topic)

    def setup_html_display (self):
        self.html_document = gtkhtml2.Document ()
        self.html_view = gtkhtml2.View ()
        self.html_view.set_document (self.html_document)
        self.html_view.show()
        self.html_display_clear ()

    def setup (self):
        self.connect ('destroy', self.on_destroy)
        self.set_default_size (730, 600)
        self.set_title ('Yarrr')

        vbox = gtk.VBox()
        self.add (vbox)

        uimanager = gtk.UIManager ()
        self.uimanager = uimanager
        accelgroup = uimanager.get_accel_group ()
        self.add_accel_group (accelgroup)

        actiongroup = gtk.ActionGroup ('Actions')
        self.actiongroup = actiongroup
        actiongroup.add_actions ([('Subscribe', gtk.STOCK_CONNECT, '_Subscribe', None,
                                   None, self.on_subscribe),
                                  ('Unsubscribe', gtk.STOCK_REMOVE, '_Unsubscribe', None,
                                   None, self.on_unsubscribe),
                                  ('NewMessage', gtk.STOCK_NEW, 'New _Message', None,
                                   None, self.on_new_message),
                                  ('RemoveMessage', None, '_Remove Message (for debugging)', None,
                                   None, self.on_remove_message),
                                  ('NewTopic', gtk.STOCK_ADD, 'New _Topic', None,
                                   None, self.on_new_topic),
                                  ('Close', gtk.STOCK_CLOSE, '_Close', None,
                                   None, self.on_close),
                                  ('Preferences', gtk.STOCK_PREFERENCES,
                                   '_Preferences', None,
                                   None, self.on_preferences),
                                  ('Summarize', None,
                                   '_Summarize', None,
                                   None, self.on_summarize),
                                  ('File', None, '_File'),
				  ('Edit', None, '_Edit')])
        self.uimanager.insert_action_group (actiongroup, 0)

        uimanager.add_ui_from_string (ui_description)

        menubar = uimanager.get_widget ('/MenuBar')
        vbox.pack_start (menubar, False)

        toolbar = uimanager.get_widget ('/ToolBar')
        vbox.pack_start (toolbar, False)

        self.date_header = DateHeader ()
        self.date_header.show_all ()
        vbox.pack_start (self.date_header, False)

        self.message_list = MessageListWidget ()
        self.message_list.connect('reply-clicked', self.on_reply_clicked)
        self.message_list.show ()

        vbox.pack_start (self.message_list, True)

        statusbar = gtk.Statusbar ()
        vbox.pack_start (statusbar, False)
        statusbar.show ()

        vbox.show ()

    def on_subscribe (self, action):
        #folder_creator = FolderCreator (self.folder_view)
        print 'subscribe not implemented yet'

    def on_unsubscribe (self, action):
        self.folder_view.remove ()

    def refresh_message_list (self):
        self.message_list.update()

    def refresh_list_view (self):
        self.list_view.update()

    def on_new_message (self, action):
        SendMessage (self.topic, self.refresh_message_list)
    
    def on_reply_clicked (self, message_list, message_id):
        SendMessage (self.topic, self.refresh_message_list, message_id)

    def on_remove_message (self, action):
        (type, object, parent) = self.list_view.get_selected_and_parent ()
        if type == self.list_view.MESSAGE:
            self.get_server().removeMessage (parent['id'], object['id'])

    def on_new_topic (self, action):
        topic = self.list_view.get_topic ()
        TopicCreator (topic, self.refresh_list_view)

    def on_preferences (self, action):
        preferences = Preferences ()

    def on_summarize_saved (self):
        self.update_summary()
        
    def on_summarize_close (self, button, window, doc, topic):
        doc.save_data(topic.get_server(), self.on_summarize_saved)
        window.destroy()
        
    def on_summarize (self, action):
        topic = self.list_view.get_selected ()
        if topic != None:
            window = gtk.Window()
            window.set_title("Topic summary")
            window.set_default_size(400, 400)
            vbox = gtk.VBox()
            vbox.show()
            window.add(vbox)
            doc = DocumentEditor()
            doc.get_from_document(topic.get_server(), topic.get_summary_id())
            doc.show()
            vbox.pack_start(doc)
            button = gtk.Button("_OK")
            button.show()
            vbox.pack_start(button, False, False, 0)
            button.connect("clicked", self.on_summarize_close, window, doc, topic)
            window.show()
        
    def on_close (self, action):
        self.destroy ()

    def on_destroy (self, window):
        gtk.main_quit()
