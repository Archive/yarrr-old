import gobject
import gtk
import xml
from cStringIO import StringIO
import md5
 
def color_alpha_pixbuf(pixbuf, r, g, b):
    assert(pixbuf.get_has_alpha())
    color = chr(r)+chr(g)+chr(b)
    data = pixbuf.get_pixels()
    w = pixbuf.get_width()
    h = pixbuf.get_height()
    stride = pixbuf.get_rowstride()

    file = StringIO()
    y = 0
    i = 0
    while y < h:
        x = 0
        while x < w:
            a = data[i+x*4+3]
            file.write(color)
            file.write(a)
            x = x + 1
        y = y + 1
        i = i + stride

    colored = gtk.gdk.pixbuf_new_from_data(file.getvalue(), gtk.gdk.COLORSPACE_RGB, True, 8, w, h, 4*w)
    file.close()
    return colored

def get_colors_from_pixbuf(pixbuf):
    data = pixbuf.get_pixels()
    w = pixbuf.get_width()
    if pixbuf.get_has_alpha():
        ps = 4
    else:
        ps = 3

    colors = []
    for x in range(w):
        r = ord(data[x*ps])
        g = ord(data[x*ps+1])
        b = ord(data[x*ps+2])
        colors.append( (r,g,b) )
    
    return colors

def read_pixbuf_set(name, num):
    set = []
    for i in range(1, num + 1):
        file = "data/" + name + str(i)  + ".png"
        pixbuf = gtk.gdk.pixbuf_new_from_file (file)
        set.append(pixbuf)
    return set

def n_bits_in_set(set):
    l = len(set)
    if l >= 64:
        return 6
    if l >= 32:
        return 5
    if l >= 16:
        return 4
    if l >= 8:
        return 3
    if l >= 4:
        return 2
    if l >= 2:
        return 1
    return 0
    
outer = read_pixbuf_set("o", 16)
inner = read_pixbuf_set("i", 8)
middle = read_pixbuf_set("m", 8)

n_hash_bits = (4+3+3+3*3) # nr of combinations of shapes and colors

parts = [outer, inner, middle]
colors = get_colors_from_pixbuf(gtk.gdk.pixbuf_new_from_file ("data/colors.png"))

def generate_icon(hash):
    icon = outer[0].copy()
    icon.fill(0x00000000)
    for part in parts:
        n_bits = n_bits_in_set(part)
        part_index = hash & ( (1 << n_bits) - 1)
        hash = hash >> n_bits
        
        n_bits = n_bits_in_set(colors)
        color_index = hash & ( (1 << n_bits) - 1)
        hash = hash >> n_bits
        
        color = colors[color_index]
        pic = color_alpha_pixbuf(part[part_index], color[0], color[1], color[2])
        pic.composite(icon, 0, 0, icon.get_width(), icon.get_height(), 0, 0, 1, 1, gtk.gdk.INTERP_NEAREST, 255)
    return icon

# gets n_bits from a data string of data_n_bits bits of information
# works by extracting at most n_bits for as long as we can, and
# xoring together the results
def get_n_bits(n_bits, data, data_n_bits):
    assert(data_n_bits%8 == 0)
    res = 0

    data_pos = 0;
    data_byte_bits = 0
    
    while data_n_bits > 0:
        extracted = 0
        extracted_bits = 0

        while extracted_bits < n_bits and data_n_bits != 0:
            if data_byte_bits == 0:
                data_byte = ord(data[data_pos])
                data_byte_bits = 8
                data_pos += 1

            get_bits = min (n_bits - extracted_bits, data_byte_bits)

            # extract the first get_bits bits from data_byte
            x = (data_byte >> (data_byte_bits - get_bits)) & ((1<<get_bits) - 1)
            data_byte_bits -= get_bits
                              
            extracted = (extracted << get_bits) |  x
            extracted_bits += get_bits

            data_n_bits -= get_bits

        res = res ^ extracted
    return res
    
persons = {}

def get_person(public_key, server, create = True):
    global persons
    #TODO: Need to handle persons on different servers
    #key = (server, public_key)
    key = public_key
    if key in persons:
        return persons[key]
    if create:
        person = Person(key, server)
        persons[key] = person
        return person
    return None

def escape_text(text):
    return xml.sax.saxutils.escape(text);

class Person(gobject.GObject):
    __gsignals__ = { 
        'data-changed' : (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
    }
    
    def __init__(self, key, server):
        gobject.GObject.__init__(self)
        self.public_key = key
        self.server = server
        self.have_info = False
        self.getting_info = False
        self.name = None
        self.email = None
        self.icon = None

    def got_info(self, info):
        self.getting_info = False
        self.have_info = True
        if "email" in info:
            self.email = info["email"]
        if "name" in info:
            self.name = info["name"]
            
        self.emit('data-changed')
        
    def update(self):
        if not self.have_info and not self.getting_info:
            self.getting_info = True
            self.server.get_person_attributes (self.public_key, self.got_info)
        
    def get_name(self):
        self.update()
        return self.name
        
    def get_icon(self):
        self.update()
        if self.icon == None:
            m = md5.new()
            m.update(self.public_key)
            d = m.digest()
            hash = get_n_bits(n_hash_bits, d, 128)
            self.icon = generate_icon(hash)
        return self.icon

gobject.type_register(Person)

class PersonWidget(gtk.HBox):
    def __init__(self, person):
        gtk.HBox.__init__(self)
        self.person = person
        
        icon = gtk.Image()
        icon.show()
        self.pack_start(icon)
        
        label = gtk.Label("")
        label.show()
        self.pack_start(label)
        
        self.label = label
        self.icon = icon
        
        self.person.connect('data-changed', self.on_person_changed)
        self.update()
        
    def on_person_changed(self, person):
        self.update()
        
    def update(self):
        pixbuf = self.person.get_icon()
        self.icon.set_from_pixbuf(pixbuf)
        name = self.person.get_name()
        if name == None:
            name = "Anonymous"
        self.label.set_markup("<i>" + escape_text(name) + "</i>")
        
#TODO: Handle mouseover and click => open person info window
