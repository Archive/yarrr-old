import os

def get_config_dir ():
    dir = os.environ['HOME'] + '/.gnome2/yarrr/'
    if not os.path.exists (dir):
        os.mkdir (dir)
    return dir
