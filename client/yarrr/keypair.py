import cPickle as pickle
import yarrr.config
import os

class KeyPair:

    def __init__ (self):
        self.path = yarrr.config.get_config_dir () + "keys"

    def set_public (self, key):
        self.public = key

    def set_private (self, key):
        self.private = key

    def get_public (self):
        return self.public

    def get_private (self):
        return self.private

    def save (self):
        f = file (self.path, 'w')
        pickle.dump (self.public, f)
        f.close ()

    def load (self):
        if os.path.exists (self.path):
            f = file (self.path, 'r')
            public = pickle.load (f)
            f.close ()
            self.public = public
            return 1

        return 0
