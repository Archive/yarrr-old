import sys

enable_logging = True

def log (message):
    if enable_logging:
        sys.stdout.write (message)
        sys.stdout.flush ()
