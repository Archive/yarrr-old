#!/usr/bin/env python

import gtk
import gtk.gdk
import sys
import getopt
import yarrr.window

def usage():
	print sys.argv[0] + " [--uri=yarrr://www.peoplesmiths.com:8080/yarrr/yarrr?topic_id=4]"
	sys.exit(1)

try:
	opts, args = getopt.getopt(sys.argv[1:], "u:", ["uri="])
except getopt.GetoptError:
	usage()

uri = 'yarrr://www.peoplesmiths.com:8080/yarrr/yarrr?topic_id=4'

for o, v in opts:
	if o in ('-u', '--uri'):
		uri = v

if __name__ == '__main__':
    gtk.gdk.threads_init ()
    gtk.gdk.threads_enter ()

    window = yarrr.window.Window ()
    window.show ()
    window.open (uri)

    gtk.main ()

    gtk.gdk.threads_leave ()
