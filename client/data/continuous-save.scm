; The GIMP -- an image manipulation program
; Copyright (C) 1995 Spencer Kimball and Peter Mattis
; 
; Continuous Save --- save the file which named "base-name_(number).(type)"
; Copyright (C) 2001 Iccii <iccii@hotmail.com>
; 
; This script was born of miyoken's idea.
; 
; --------------------------------------------------------------------
;   - Changelog -
; version 0.1  2001/05/20 iccii <iccii@hotmail.com>
;     - Initial relase
; version 0.2  2001/05/24 iccii <iccii@hotmail.com>
;     - Make better
; version 0.3  2001/05/24 iccii <iccii@hotmail.com>
;     - Chose the filename with directory
;     - Saved image type equal to original image type
; version 0.3a 2001/09/26 iccii <iccii@hotmail.com>
;     - Bug fixed in checking the file type
;
; --------------------------------------------------------------------
; 
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 2 of the License, or
; (at your option) any later version.
; 
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
; 
; You should have received a copy of the GNU General Public License
; along with this program; if not, write to the Free Software
; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;


(define (script-fu-continuous-save
		img			; IMAGE
		drawable		; DRAWABLE (no need)
		save-type		; saving image type
		file-name		; base file name with directory
		jpg-comp		; JPEG compression (0-100%)
		png-comp		; PNG compression (level 1-9)
		interactive		; interactive save option on each layer
		use-layer-name		; use layer name as filename
		)

  (define (to-save-bmp img layer file-name interactive-save?)
	  (file-bmp-save interactive-save? img (car (gimp-image-flatten img)) file-name ""))

  (define (to-save-jpg img layer file-name interactive-save? jpg-comp)
	  (file-jpeg-save interactive-save? img (car (gimp-image-flatten img)) file-name ""
					    (/ jpg-comp 100) 0.0 1 0 "" 0 0 0 1))

  (define (to-save-png img layer file-name interactive-save? png-comp)
	  (file-png-save interactive-save? img layer file-name "" 0 png-comp 1 1 1 1 1))

  (let* (
	 (count 0)
	 (interactive-save? (if (eqv? interactive TRUE) 0 1))
	 (number (car (gimp-image-get-layers img)))
	 (image-type (car (gimp-image-base-type img)))
	) ; end variable definition

	(while (< count number)
	  (let* (
	         (tmp-image-type
	           (cond
	             ((and (eqv? save-type 0) (eqv? image-type 2))
	               0)		;; convert to RGB because BMP can't treat INDEXED image
	             ((and (eqv? save-type 1) (or (eqv? image-type 1) (eqv? image-type 2)))
	               0)		;; convert to RGB because JPEG can't treat INDEXED and GRAY image
	             (image-type)))	;; otherwise, equal to origianl image type
	         (layer (aref (cadr (gimp-image-get-layers img)) count))
	         (tmp-img (car (gimp-image-new (car (gimp-drawable-width  layer))
	                                       (car (gimp-drawable-height layer))
	                                       tmp-image-type)))
	         (tmp-layer (car (gimp-layer-new tmp-img
	                                         (car (gimp-drawable-width  layer))
	                                         (car (gimp-drawable-height layer))
	                                         (+ 1 (* 2 tmp-image-type)) "Temp Layer" 100 NORMAL)))
	        )

	;; create an image with single layer, and remove the layer mask (if exists)
	    (gimp-image-add-layer tmp-img tmp-layer 0)
	    (gimp-drawable-fill tmp-layer TRANSPARENT-FILL)
	    (gimp-edit-copy layer)
	    (gimp-floating-sel-anchor (car (gimp-edit-paste tmp-layer 0)))
	    (if (< 0 (car (gimp-layer-mask layer)))
	      (let* ((tmp-mask (car (gimp-layer-create-mask tmp-layer 0))))
	        (gimp-edit-copy (car (gimp-layer-mask layer)))
	        (gimp-floating-sel-anchor (car (gimp-edit-paste tmp-layer 0)))
	        (gimp-image-remove-layer-mask tmp-img tmp-layer APPLY)
	        (gimp-displays-flush)))
	    ;(set! tmp-display (car (gimp-display-new tmp-img)))
	    ;(gimp-displays-flush)

	;; save a resulting image by specified image type
	    (let* (
		   (layer-file-name
		    (if (eqv? use-layer-name TRUE)
			(car (gimp-drawable-get-name layer))
			(string-append file-name (number->string count))))
		   )
	      (print use-layer-name)
	      (cond
	       ((eqv? save-type 0)
	        (to-save-bmp tmp-img tmp-layer (string-append layer-file-name ".bmp") interactive-save?))
	       ((eqv? save-type 1)
	        (to-save-jpg tmp-img tmp-layer (string-append layer-file-name ".jpg") interactive-save? jpg-comp))
	       ((eqv? save-type 2)
	        (to-save-png tmp-img tmp-layer (string-append layer-file-name ".png") interactive-save? png-comp))
	       )
	      )
	    (gimp-image-delete tmp-img)
	    ;(gimp-display-delete tmp-display)
	    )
	  (set! count (+ count 1))
	) ;; repeat until all layer is saved
  )
)

(script-fu-register
	"script-fu-continuous-save"
	"<Image>/Script-Fu/Utils/Continuous Save..."
	"Save an Image by single layer with continuous number"
	"Iccii <iccii@hotmail.com>"
	"Iccii"
	"Sep, 2001"
	"RGB* INDEXED* GRAY*"
	SF-IMAGE	"Image"			0
	SF-DRAWABLE	"Drawable"		0
	SF-OPTION	"Saved File Type"	'("BMP" "JPG" "PNG")
	SF-FILENAME	"Base File Name"	"MyPicture_"
	SF-ADJUSTMENT	"JPEG Compression"	'(75 0 100 1 5 0 0)
	SF-ADJUSTMENT	"PNG Compression"	'(9 1 9 1 1 0 0)
	SF-TOGGLE	"Interactive?"		FALSE
	SF-TOGGLE	"Use layer name as filename?"	FALSE
)
