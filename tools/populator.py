#!/usr/bin/python

import xmlrpclib
import sys

def create_test_messages(server, topic, public_key, message_num):
    content = {"main": xmlrpclib.Binary("test message contents")}
    message = server.postMessage(topic, "Test message " + str(message_num), "", public_key, content)
    
def get_local_server():
    return xmlrpclib.Server('http://localhost:19842')

def populate (server):
    topics = []
    key_hash = server.getPersonKeyPair()
    public_key = key_hash ['publicKey']

    topic1 = server.createTopic("Yarrr UI", public_key)
    topic2 = server.createTopic("Yarrr Server", public_key)
    topic3 = server.createTopic("Yarrr Build", public_key)
    topic4 = server.createTopic("Yarrr Hackers", public_key)
    test_topic = server.createTopic("Test Topic", public_key)
    
    server.addLink(test_topic, topic1)
    for i in range(10):
        create_test_messages(server, test_topic, public_key, i)
    
if __name__ == '__main__':
    s = xmlrpclib.Server (sys.argv[1])
    print populate (s)
    