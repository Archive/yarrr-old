package org.gnome.yarrr.person;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.hibernate.HibernateException;

import org.gnome.yarrr.hibernate.HibernateUtil;

/**
 * A group of persons generated from a list of attribute values.
 * Only persons with the specified attribute being in the list
 * are considered a part of the list. Useful for instance to 
 * import a list of email addresses and generate a group from.     
 * 
 * @author alex
 */
public class AttributeListGroup extends Group {
	String attributeName;
	Set attributes;

	/**
	 * Create a new AttributeListGroup with the given name.
	 * The group will be persisted if the current transaction commits.
	 * 
	 * @param name the name of the group
	 * @throws HibernateException 
	 * @attribute the name of the attribute checked for membership 
	 * @throws HibernateException
	 */	
	public AttributeListGroup(String name, String attribute) throws HibernateException {
		super(name);
		this.attributeName = attribute;
		attributes = new HashSet();
		HibernateUtil.getSession().save(this);
	}

	protected AttributeListGroup() {
		super();
		attributes = new HashSet();
	}

	
	public void addValue(String value) {
		attributes.add(value);
	}
	
	/**
	 * @return Returns the attributeName.
	 */
	public String getAttributeName() {
		return attributeName;
	}
	/**
	 * @param attributeName The attributeName to set.
	 */
	private void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	/**
	 * @return Returns the attributes.
	 */
	public Set getAttributes() {
		return attributes;
	}
	/**
	 * @param attributes The attributes to set.
	 */
	private void setAttributes(Set attributes) {
		this.attributes = attributes;
	}
	
	/* (non-Javadoc)
	 * @see org.gnome.yarrr.person.Group#isMember(org.gnome.yarrr.person.Person)
	 */
	public boolean isMember(Person person) {
		return attributes.contains(person.getAttribute(attributeName)); 
	}

	/* (non-Javadoc)
	 * @see org.gnome.yarrr.person.Group#listMembers()
	 */
	public Iterator iterator() throws HibernateException {
		return HibernateUtil.getSession()
		  .createQuery("select person from Person person, AttributeListGroup g "+
		  		       "where person.attributes[:attribute] in elements(g.attributes) " + 
		  		       "and g.id = :thegroup")
		  .setString("attribute", attributeName)
		  .setLong("thegroup", getId().longValue())
		  .iterate();
	}
}
