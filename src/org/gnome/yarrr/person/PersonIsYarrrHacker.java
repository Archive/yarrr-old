/*
 * Created on 20-Jan-05
 *
 */
package org.gnome.yarrr.person;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author hp
 *
 */
public class PersonIsYarrrHacker extends FactAboutPerson {

	List hackers;
	
	public void addYarrrHackerEmail(String email) {
		this.hackers.add(email);
	}
	
	public PersonIsYarrrHacker() {
		super("is_yarr_hacker", "true");
		this.hackers = new ArrayList();
	}
	
	public IVerifier verify(Person person) {
		boolean isYarrHacker = false; 
		Iterator iter = this.hackers.iterator();
		while (iter.hasNext()) {
			String email = (String) iter.next();
			if (person.getAttribute(Person.EMAIL).equals(email))
				isYarrHacker = true;
		}
		// we don't want to unset anything verified in the past, 
		// we only want to add verifications.
		if (isYarrHacker)
			person.setAttributeBoolean(this.getAttribute(), isYarrHacker);
		return new CompletedVerifier();
	}
}
