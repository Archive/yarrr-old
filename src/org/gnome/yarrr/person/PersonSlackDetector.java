/*
 * Created on Jan 20, 2005
 */
package org.gnome.yarrr.person;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import org.hibernate.HibernateException;

/**
 * @author caillon
 */
public class PersonSlackDetector extends Thread {
	private static final int DEFAULT_INTERVAL = 60000; // 60s
	
	public static PersonSlackDetector slackDetector;
	
	static {
		slackDetector = new PersonSlackDetector();
		slackDetector.start();
	}

	private Hashtable active;
	private int interval;

	public class PeopleNotSlacking {
		public Enumeration publicKeys;
		public long timestamp;
	}

	public PersonSlackDetector() {
		super("PersonSlackDetector");
		this.interval = DEFAULT_INTERVAL;
		this.active = new Hashtable();
	}

	public synchronized void heartbeat(PublicKey key) {
		Person person;
		
		try {
			person = Person.getPerson (key);
			this.active.put(person.getId(), new Date());
		} catch (HibernateException e) {
		}
	}

	/**
	 * Set how long somebody is kept active after their
	 * last heartbeat.
	 * @param milliseconds
	 */
	public void setMaxIdleTolerated(int milliseconds) {
		this.interval = milliseconds;
	}
	
	private synchronized void destroySlackers() {
		Enumeration elts = this.active.elements();
		Enumeration keys = this.active.keys();
		
		Date now = new Date();
		while (elts.hasMoreElements()) {
			Date heartbeat = (Date)elts.nextElement();
			Long personID = (Long)keys.nextElement();
			if (now.getTime() - heartbeat.getTime() > interval) {
				this.active.remove(personID);
			}
		}
	}

	public boolean isSlacking(PublicKey key) {
		Person person;
		
		try {
			person = Person.getPerson(key);
		} catch (Exception e) {
			// TODO can we do anything better than returning?
			return true;
		}
		
		return !active.containsKey(person.getId());
	}

	public synchronized List getNonSlackers() {
		return new ArrayList (this.active.keySet());
	}

	public void run() {
		while (true) {
			try {
				sleep(1000);
				destroySlackers();
			}
			catch (InterruptedException e) {
				break;
			}
		}
	}
}
