/*
 * Created on Feb 1, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.gnome.yarrr.person;

import java.sql.SQLException;
import java.sql.Statement;
import java.util.Iterator;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;

import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.hibernate.Persistent;

/**
 * @author alex
 */
public abstract class Group extends Persistent {
	String name;
	
	/**
	 * @param name
	 */
	public Group(String name) {
		super();
		this.name = name;
	}

	protected Group() {
		super();
	}

	static public Group getGroup(Long id) throws HibernateException {
		return (Group) HibernateUtil.getSession().createCriteria(Group.class)
    		.add( Expression.eq("id", id))
			.uniqueResult();
	}
	
	/**
	 * Verify if the person is member of the list
	 * 
	 * @param person
	 * @return
	 */
	abstract public boolean isMember(Person person);
	
	/**
	 * Get an iterator over all the persons in this group
	 * 
	 * @return
	 * @throws HibernateException 
	 */
	abstract public Iterator iterator() throws HibernateException;
		
	/**
	 * @return Returns the name.
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name The name to set.
	 */
	private void setName(String name) {
		this.name = name;
	}
	
	static public void deleteAll() throws HibernateException, SQLException {
		Statement statement = HibernateUtil.getSession().connection().createStatement();
		statement.execute("DELETE FROM group_person_mapping");
		statement.execute("DELETE FROM attribute_group");
		statement.execute("DELETE FROM groups");
	}

}
