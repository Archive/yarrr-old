package org.gnome.yarrr.person;

import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.hibernate.HibernateException;

import org.gnome.yarrr.hibernate.HibernateUtil;


/**
 * A named group of persons manually enumerated in the database
 * 
 * @author alex
 */
public class EnumeratedGroup extends Group {
	// List of the people in the group
	Set people; 
	
	protected EnumeratedGroup() {
		super();
		people = new HashSet();
	}
	
	/**
	 * Create a new EnumeratedGroup with the given name.
	 * The group will be persisted if the current transaction commits.
	 * 
	 * @param name the name of the group
	 * @throws HibernateException
	 */
	public EnumeratedGroup(String name) throws HibernateException {
		super(name);
		people = new HashSet();
		HibernateUtil.getSession().save(this);
	}
		
	/**
	 * Add a person to an enumerated group
	 * 
	 * @param person
	 */
	public void addPerson(Person person) {
		people.add(person);
	}
	
	/**
	 * @return Returns the people.
	 */
	private Set getPeople() {
		return people;
	}
	/**
	 * @param people The people to set.
	 */
	private void setPeople(Set people) {
		this.people = people;
	}

	/* (non-Javadoc)
	 * @see org.gnome.yarrr.person.Group#isMember(org.gnome.yarrr.person.Person)
	 */
	public boolean isMember(Person person) {
		// We always loaded the list, no need to query
		return people.contains(person);
	}

	/* (non-Javadoc)
	 * @see org.gnome.yarrr.person.Group#listMembers()
	 */
	public Iterator iterator() {
		return Collections.unmodifiableSet(people).iterator();
	}
}
