/*
 * Created on Jan 19, 2005
 */
package org.gnome.yarrr.person;

import java.security.PublicKey;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gnome.yarrr.Cryptography;
import org.gnome.yarrr.Cryptography.EncryptionException;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.hibernate.Persistent;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;

/**
 * @author seth
 */
public class Person extends Persistent {
	PublicKey publicKey;
	Map attributes;
	public static final Person UNKNOWN_PERSON = new Person(null);
	public static final String EMAIL = "email";
	public static final String NAME = "name";

	// Add some POJO
	protected Person () {
	}
	
	private Person(PublicKey publicKey) {
		this.publicKey = publicKey;
		attributes = new HashMap();
	}
	
//		MessageDigest sha = MessageDigest.getInstance("SHA-1");
//		sha.update(inputOne);
//		sha.update(inputTwo);
//		byte[] hash = sha.digest(inputThree);
//		
//		//next
//		KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA");
//		keyGen.initialize(1024, new SecureRandom());
//		KeyPair pair = keyGen.generateKeyPair();
//		
//		//next
//		Signature dsa = Signature.getInstance("DSA");
//		PrivateKey priv = pair.getPrivate();
//		dsa.initSign(priv);
//
//		dsa.update(data);
//		byte[] sig = dsa.sign();
//		
//		// next
//		pair.getPublic();
//		dsa.initVerify(pub);
//
//		dsa.update(data);
//		boolean verifies = dsa.verify(sig);
//		System.out.println("signature verfies: " + verifies);
	

	public PublicKey getPublicKey() {
		return publicKey;
	}
	
	private void setPublicKey(PublicKey key) {
		publicKey = key;
	}
	
	public Map getAttributes() {
		return attributes;
	}
	
	public boolean hasAttribute (String attribute) {
		return attributes.containsKey(attribute);
	}
	
	private void setAttributes(Map attributes) {
		this.attributes = attributes;
	}
	
	public String getAttribute(String attribute) {
		String s = (String) attributes.get(attribute);
		return s;	
	}
	
	public void setAttribute(String attribute, String value) {
		assert(attribute != null);
		assert(value != null);
		attributes.put(attribute, value);
	}
	public boolean getAttributeBoolean(String attribute) {
		String s = (String) attributes.get(attribute);
		if (s != null && s.equals("true")) {
			return true;
		} else {
			return false;
		}
	}
	public void setAttributeBoolean(String attribute, boolean value) {
		if (value) {
			setAttribute (attribute, "true");
		} else {
			setAttribute (attribute, "false");
		}
	}
	
	/**
	 * Hearbeat function
	 */
	public void heartbeat() {
		PersonSlackDetector.slackDetector.heartbeat(publicKey);
	}
	static public void personIsActiveHeartbeat(PublicKey callerPublicKey) {
		PersonSlackDetector.slackDetector.heartbeat(callerPublicKey);
	}

	static public List/*Person*/ getActivePeople() throws HibernateException {
		List nonSlackersIDs = PersonSlackDetector.slackDetector.getNonSlackers();
		
		if (!nonSlackersIDs.isEmpty()) {
			return HibernateUtil.getSession().createCriteria(Person.class)
        		.add( Expression.in("id", nonSlackersIDs))
				.list();
		} else {
			return new ArrayList();
		}
	}
	
	static public Person getPerson(Long id) throws HibernateException {
		return (Person) HibernateUtil.getSession().createCriteria(Person.class)
    		.add( Expression.eq("id", id))
			.uniqueResult();
	}
	
	static public Person getPerson(byte[] publicKeyBytes) throws HibernateException, EncryptionException {
		PublicKey publicKey = Cryptography.bytesToPublicKey(publicKeyBytes);
		return getPerson(publicKey);
	}
	
	static public Person getPerson(PublicKey key) throws HibernateException {
		Person person = (Person)
		HibernateUtil.getSession().createCriteria(Person.class)
			.add( Expression.eq("publicKey", key) )
			.uniqueResult();

		if (person == null) {
			person = new Person(key);
			HibernateUtil.getSession().save(person);
		}

		return person;
	}

	static public void deleteAll() throws HibernateException, SQLException {
		Statement statement = HibernateUtil.getSession().connection().createStatement();
		statement.execute("DELETE FROM userattributes");
		statement.execute("DELETE FROM Person");
	}
}
