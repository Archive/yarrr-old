/*
 * Created on 20-Jan-05
 *
 */
package org.gnome.yarrr.person;

/**
 * @author hp
 *
 * This class represents an assertion that can be made about a Person.
 * Its name is the attribute of the Person that will be set to true if 
 * the fact has been verified. Its description may be shown to the user 
 * as a description of the fact.
 */
public abstract class FactAboutPerson {

	private String attribute;
	private String description;
	
	protected FactAboutPerson(String attribute, String description) {
		this.attribute = attribute;
		this.description = description;
	}
	
	/** 
	 * Represents the process of verifying a particular fact about a particular
	 * person.
	 * 
	 * @author hp
	 *
	 */
	public interface IVerifier {
		/** 
		 * Blocks until the verifier has verified the fact (or not)
		 * TODO have some kind of progress meter
		 */
		public void waitForCompletion();
	}
	
	/** A trivial IVerifier used when the verification 
	 * is instant and we can immediately report its status.
	 * @author hp
	 *
	 */
	protected class CompletedVerifier implements IVerifier {
		public CompletedVerifier() {
		}
		public void waitForCompletion() {
			return;
		}
	}
	
	/** 
	 * Launches a verification of this fact for the given person.
	 * Returns an IVerifier which is basically a cookie to track 
	 * the verification process. Should never remove a fact about 
	 * someone; this should only set the fact to true.
	 * 
	 * @param person
	 * @returns the verification handle
	 */
	public abstract IVerifier verify(Person person);
	
	/**
	 * @return Returns the attribute.
	 */
	public String getAttribute() {
		return attribute;
	}
	
	/**
	 * @return Returns the description.
	 */
	public String getDescription() {
		return description;
	}
	
	/** 
	 * @returns Returns true if the fact has been verified about the given person
	 */
	public boolean isVerified(Person person) {
		boolean verified = person.getAttributeBoolean(this.getAttribute());
		return verified;
	}
}
