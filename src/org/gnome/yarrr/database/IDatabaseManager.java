/*
 * Created on Jan 15, 2005
 */
package org.gnome.yarrr.database;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Interface for managing instances of database servers
 * and getting a {@link Connection} to them
 * 
 * @author seth
 */
public interface IDatabaseManager {
	
	/**
	 * Represents an error creating the database
	 * @author seth
	 */
	public class DatabaseCreationException extends Exception {
		String error;
		String databasePath;
		static final long serialVersionUID = 1;
		
		protected DatabaseCreationException(String error, String databasePath) {
			this.error = error;
			this.databasePath = databasePath;
		}
		
		public String toString() {
			return error + " creating " + databasePath;
		}
	}

	/**
	 * Opens a new connection to the database
	 * managed by this instance
	 * 
	 * @return get a new JDBC {@link Connection}
	 * @throws SQLException
	 */
	public abstract Connection getConnection() throws SQLException;

	/**
	 * Start the server for the managed database
	 * @throws IOException
	 */
	public abstract void start() throws IOException;

	/**
	 * Stop the server for the managed database
	 * @throws IOException
	 */
	public abstract void stop() throws IOException;

	/**
	 * @return true if the server is currently running
	 * @throws IOException
	 */
	public abstract boolean isRunning() throws IOException;
	
	/**
	 * @return the url to the database
	 */
	public abstract String getUrl();
    
    /**
     * @return Returns true if this database was freshly created, false if the database already existed. 
     */
    public abstract boolean wasVirgin();

    /**
     * Backs up the database... somewhere.
     */
    public abstract void snapshot() throws IOException;
    
}