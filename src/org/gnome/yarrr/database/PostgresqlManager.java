/*
 * Created on Jan 14, 2005
 */
package org.gnome.yarrr.database;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

import org.apache.log4j.Logger;

/**
 * Manages an instance of the PostgreSQL server pointing at a custom
 * database.
 * 
 * @author seth
 */
public class PostgresqlManager implements IDatabaseManager {
	/**
	 * @author seth
	 */
	public class ShutdownPostgresqlThread extends Thread {
		private PostgresqlManager manager;
		ShutdownPostgresqlThread(PostgresqlManager manager) {
			this.manager = manager;
		}
		public void run(){
			System.out.println("Shutting down PostgreSQL from the shutdownHandler");
			try {
				manager.stop();
			} catch (IOException e) {
				System.err.println("Error trying to shutdown PostgreSQL server:");
				e.printStackTrace();
			}
		}
	}
	
	static Logger logger = Logger.getLogger(PostgresqlManager.class);
	
	private File databasePath;
	private int databasePort;
	private String url;
	private Properties dbConnectionProps;
	private String databaseName = "yarrr";
    private PrintStream commandLogStream = null;
	private File storePath;
    private int postgresMajor;
    private boolean wasVirgin = false;
    

	/**
	 * Manages an instance of the PostgreSQL server. Given a path, it will
	 * check if a database exists, creates one if it doesn't, and start a
	 * PostgreSQL server running.
	 * 
	 * @param databasePath location the database is stored at (will be created if it doesn't exist)
	 * @param databaseName name of the database to use (will be created if it doesn't exist)
	 * @throws IOException
	 * @throws DatabaseCreationException
	 * @throws ClassNotFoundException
	 */
	public PostgresqlManager(File storePath) throws IOException, DatabaseCreationException, ClassNotFoundException {
		this.databasePort = findAvailablePort();
		this.storePath = storePath;
		this.databasePath = new File(this.storePath, "database");
		
		this.url = "jdbc:postgresql://localhost:" + this.databasePort + "/"+ this.databaseName;
		this.dbConnectionProps = new Properties();
		this.dbConnectionProps.setProperty("user", System.getProperty("user.name"));
		
		this.commandLogStream = getCommandLogStream();
		
        Process postmaster = Runtime.getRuntime().exec("/usr/bin/postmaster --version");
        
        postmaster.getOutputStream().close();
        postmaster.getErrorStream().close();
        
        String postmasterOutput = new BufferedReader(new InputStreamReader(postmaster.getInputStream())).readLine();
        
        this.postgresMajor = 0;
        if (postmasterOutput != null) {
        if (postmasterOutput.indexOf("(PostgreSQL) 8") >= 0) {
            this.postgresMajor = 8;
        } else if (postmasterOutput.indexOf("(PostgreSQL) 7") >= 0) {
            this.postgresMajor = 7;
        } 
        if (this.postgresMajor == 0)
            throw new DatabaseCreationException("Unknown PostgreSQL major version", this.databasePath.getAbsolutePath());
        }
        
        Class.forName("org.postgresql.Driver");
		
		if(isRunning()) {
			fastStop();
			if (isRunning()) {
				throw new DatabaseCreationException("A database was already running", this.databasePath.getAbsolutePath());
			}
		}
		
		if (!postgresDBExists()) {
			createDB();
            this.wasVirgin  = true;
		}
		
		if (!isRunning()) {
			start();
		}

		if (!isRunning()) {
			throw new DatabaseCreationException ("Postgres should be running", this.url);
		} else {
			Runtime.getRuntime().addShutdownHook(new ShutdownPostgresqlThread(this));
			logger.info("Postgres for " + this.getDatabasePath() + " running on port " + this.databasePort);
		}
	
	}

	private boolean postgresDBExists() {
		return new File(this.databasePath, "PG_VERSION").exists();
	}

	private void createDB() throws IOException, DatabaseCreationException {
        generateDatabaseFiles();
        start();
        initializeDatabase();
	}

	/**
	 * Opens a new connection to the PostgreSQL database
	 * managed by this instance
	 * 
	 * @return get a new JDBC {@link Connection}
	 * @throws SQLException
	 */
	public Connection getConnection() throws SQLException {
		return DriverManager.getConnection(this.url, this.dbConnectionProps);
	}
	
	private void initializeDatabase() throws DatabaseCreationException {
		File createDBPath = new File("/usr/bin/createdb");
		try {
			int result = execOutput(createDBPath.getAbsolutePath() +
								" --encoding=UNICODE --port=" + this.databasePort + " --template template0 " + this.databaseName);
		} catch (IOException e) {
			logger.error("createdb failed", e);
			throw new DatabaseCreationException("createdb failed", this.databasePath.getAbsolutePath());
		}
	}
	
	public int findAvailablePort() throws DatabaseCreationException, IOException {
		Random random = new Random ();
		int port;
		int connection_attempts = 0;
		
		while (true) {
			port = random.nextInt(10000) + 10000;

			try {
				Socket socket = new Socket ("localhost", port);
				socket.close();				
			} catch (UnknownHostException e) {
				throw new DatabaseCreationException ("Could not connect to localhost", this.databasePath.getAbsolutePath());
			} catch (IOException e) {
				break;
			}
			
			connection_attempts ++;
			if (connection_attempts > 10)
				throw new DatabaseCreationException ("Unable to find an available port", this.databasePath.getAbsolutePath());
		}
		return port;
	}

	private void writeConfigFile(File filename, String contents) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
        writer.write(contents);
        writer.close();
	}
	
	/**
	 * @throws DatabaseCreationException 
	 * 
	 */
	private void generateDatabaseFiles() throws IOException, DatabaseCreationException {
		File initDBPath = new File("/usr/bin/initdb");
		int result;
		try {
			result = execOutput(initDBPath.getAbsolutePath() + " " + databasePath);
		} catch (IOException e) {
			throw new DatabaseCreationException("exec of initdb failed (install postgresql-server?)", this.databasePath.getAbsolutePath());
		}
		if (result != 0) {
			throw new DatabaseCreationException("initdb failed", this.databasePath.getAbsolutePath());
		}
		
		if (this.postgresMajor == 7) {
		    writeConfigFile(new File(this.databasePath, "postgresql.conf"),
		            "max_connections = 100\n" +
		            "tcpip_socket = true\n" +
		    "shared_buffers = 1000\n");
		} else {
		    writeConfigFile(new File(this.databasePath, "postgresql.conf"),
		            "max_connections = 100\n" +
		            "shared_buffers = 1000\n");
		}
		// TODO Do *not* trust local connections
		writeConfigFile(new File(this.databasePath, "pg_hba.conf"),
						"local  all    all             ident   sameuser\n" +
						"host    all         all         127.0.0.1         255.255.255.255   trust");

	}
	
	class OutputInputStream extends java.lang.Thread {
		private InputStream in;
		private OutputStream out;
		
		OutputInputStream(String name, InputStream in, OutputStream out) {
			super(name);
			this.in = in;
			this.out = out;
		}

		public void run() {
			byte buf[] = new byte[128];
			int len;
			try {
				boolean doneReading = false;
				while (!doneReading) {
					len = this.in.read(buf);
					if (len < 0) {
						doneReading = true;
					} else {
						this.out.write(buf, 0, len);
					}
				}
			} catch (IOException e) {
				
			}
		}
	}

	/**
	 * @return a singleton
	 */
	private PrintStream getCommandLogStream() {
		if (this.commandLogStream != null) return this.commandLogStream;
		
		FileOutputStream fos = null;
		try {
			this.storePath.mkdirs();
			File commandLogFile = new File(this.storePath, "yarr_command_log");
			fos = new FileOutputStream(commandLogFile);
		} catch (FileNotFoundException e) {
			logger.error("Couldn't create yarr_command_log", e);
		}
		return new PrintStream(fos);
	}
	
	private int execOutput(String cmd) throws IOException {
		logger.debug("Running '" + cmd + "'");
		
		// TODO This is what we want to do, but classpath doesn't have getenv();
//		Map envMap = System.getenv();
//		env = new String[envMap.size() + 1];
//		int i = 0;
//		for (Iterator iter = envMap.keySet().iterator(); iter.hasNext();) {
//			String key = (String)iter.next();
//			String value = (String)envMap.get(key);
//			env[i] = key + "=" + value;
//			i++;
//		}
		String[] env = new String[1];
		env[env.length - 1] = "PGPORT=" + this.databasePort;
		
		Process process = Runtime.getRuntime().exec(cmd, env);
        
		OutputInputStream stdout = new OutputInputStream("stdout " + cmd, process.getInputStream(), getCommandLogStream());
		OutputInputStream stderr = new OutputInputStream("stderr " + cmd, process.getErrorStream(), getCommandLogStream());
		
		stdout.start();
		stderr.start();
		
		int returnValue = -1;
		try {
			returnValue = process.waitFor();
		} catch (InterruptedException e) {
			logger.error("PostgresqlManager interrupted", e);
		}
		
		return returnValue;	
	}
	
	private boolean postgresCommand(String cmd) throws IOException {
		return execOutput("pg_ctl -D " + this.databasePath.getAbsolutePath() + " " + cmd) == 0;
	}
	
	/**
	 * Start the PostgreSQL server for the managed database
	 * @throws IOException
	 */
    public void start() throws IOException {
    	logger.info("Starting DB " + this.getDatabasePath());
    	//Thread.dumpStack();
    	//System.out.println();
		if (!postgresCommand("-w start")) {
			throw new IOException("Could not start PostgreSQL server for " + this.databasePath);
		}
    }
    
    /**
     * Brings the postgres server to a screetching halt
     * @throws IOException
     */
    public void fastStop() throws IOException {
		if (!postgresCommand("-m f -w stop")) {
			throw new IOException("Could not stop PostgreSQL server for " + this.databasePath);
		}	
    }
    
	/**
	 * Stop the PostgreSQL server for the managed database
	 * @throws IOException
	 */
    public void stop() throws IOException {
    	logger.info("Stopping DB " + this.getDatabasePath());
 
		if (!postgresCommand("-w stop")) {
			// Grrr, soft stop didn't work, there are probably open connections
			fastStop();
			throw new IOException("Postgresql server for " + this.databasePath + " did not respond to a soft shutdown. There are probably open connections to it. Server required a 'fast' shutdown.");
		}
    }
    
    /**
     * @return true if the PostgreSQL server is currently running
     * @throws IOException
     */
	public boolean isRunning() throws IOException {
		return postgresCommand("status");
	}
	
	protected void finalize() throws Throwable {
		System.err.println("Finalizing DB");
		try {
			if (isRunning()) {
				stop();
			}
		} finally {
			super.finalize();
		}
	}
	
	public File getDatabasePath() {
		return databasePath;
	}
	
	public String getUrl() {
		return url;
	}
    
    public boolean wasVirgin() {
        return wasVirgin;
    }

    private void copy (File srcFile, File destFile) throws IOException {
    	if (srcFile.isDirectory()) {
    		destFile.mkdirs();
    		File children[] = srcFile.listFiles();
    		for (int i=0; i < children.length; i++) {
    			copy(children[i], new File(destFile, children[i].getName()));
    		}
    	} else {
    		InputStream in = new FileInputStream(srcFile);
    		OutputStream out = new FileOutputStream(destFile);
    		
    		byte [] buffer = new byte[2048];
    		int bytesRead;
    		while ((bytesRead = in.read(buffer)) > 0) {
    			out.write(buffer, 0, bytesRead);
    		}
    		in.close();
    		out.close();
    	}
    }
    
	public void snapshot() throws IOException {
		logger.info("Snapshotting Postgresql DB");
		String date = new Long(new Date().getTime()).toString();
		File targetDir = new File(new File(this.storePath, "backups"), date);
		copy(this.databasePath, targetDir);
		logger.info("Snapshot done");
	}
}