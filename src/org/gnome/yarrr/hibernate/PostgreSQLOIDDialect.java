package org.gnome.yarrr.hibernate;

import java.sql.Types;
import org.hibernate.dialect.PostgreSQLDialect;

/**
 * We use this in order to use oid instead of bytea
 * with postgresql, as that is what the postgresql java.sql.Blob
 * uses
 * 
 * @author alex
 */
public class PostgreSQLOIDDialect extends PostgreSQLDialect {
        public PostgreSQLOIDDialect() {
                super();
                registerColumnType(Types.BLOB, "oid" );
	}
}
