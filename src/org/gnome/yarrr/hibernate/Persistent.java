package org.gnome.yarrr.hibernate;

/**
 * Base class for hibernate classes with generated id
 * 
 * @author alex
 */
public class Persistent {
    public static class NoMatchFoundException extends Exception {
        static final long serialVersionUID = 1;
        public NoMatchFoundException (String message) {
            super(message);
        }
    }

	private Long id;
	public Long getId() {
		return id;
	}
	private void setId(Long long1) {
		id = long1;
	}
	
	public boolean equals (Object object) {
		try {
			Persistent other = (Persistent) object;
			return other.id.equals(id);
		} catch (Exception e) {
			return false;
		}
	}
	
	public int hashCode() {
		return id.intValue();
	}

    public String getStringId() {
        return id.toString();
    }


}
