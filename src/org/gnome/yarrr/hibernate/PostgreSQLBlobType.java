package org.gnome.yarrr.hibernate;

import java.sql.PreparedStatement; 
import java.sql.SQLException; 
import java.sql.Blob; 

import org.hibernate.HibernateException;
import org.hibernate.type.BlobType;

/**
 * This class is a workaround for:
 * http://opensource.atlassian.com/projects/hibernate/browse/HB-955 
 */
public class PostgreSQLBlobType extends BlobType {
	private static final long serialVersionUID = 1L;
	
	public void set(PreparedStatement st, Object value, int index) throws HibernateException, SQLException {
		st.setBlob(index, (Blob) value);
	}
}
