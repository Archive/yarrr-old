package org.gnome.yarrr.hibernate;

import org.gnome.yarrr.Document;
import org.gnome.yarrr.Message;
import org.gnome.yarrr.MessagePerUser;
import org.gnome.yarrr.Resource;
import org.gnome.yarrr.Statement;
import org.gnome.yarrr.Topic;
import org.gnome.yarrr.TopicPerUser;
import org.gnome.yarrr.email.Email;
import org.gnome.yarrr.email.MailingList;
import org.gnome.yarrr.person.Group;
import org.gnome.yarrr.person.Person;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;

/**
 * @author alex
 */
public class HibernateUtil {
	private static SessionFactory sessionFactory = null;
	private static ThreadLocal threadSession = null;
	private static ThreadLocal threadTransaction = null;
	
	public static boolean initialized() {
		return sessionFactory != null;
	}
	
	private static Configuration configuration = null;
	
	public static Configuration buildConfiguration(String DbUrl) throws HibernateException {
		return new Configuration()
		.setProperty("hibernate.connection.url", DbUrl)
		.setProperty("hibernate.connection.username", System.getProperty("user.name"))
		.addClass(Document.class)
		.addClass(Resource.class)
		.addClass(Message.class)
		.addClass(MessagePerUser.class)
		.addClass(Person.class)
		.addClass(Statement.class)
		.addClass(Group.class)
		.addClass(Topic.class)
		.addClass(TopicPerUser.class)
		.addClass(Email.class)
		.addClass(MailingList.class)
		.setProperty(Environment.HBM2DDL_AUTO, "update")
		;
	}
	
	public static void init(Configuration cfg) throws HibernateException {
		if (initialized())
			return;
		
		// We would like to do all these in a static initializer, but
		// that doesn't let us use a different db for the tests
		threadSession = new ThreadLocal();
		threadTransaction = new ThreadLocal();

		//cfg.setProperty("hibernate.show_sql", "true");
	
        try {
            sessionFactory = cfg.buildSessionFactory();
        } catch (HibernateException e) {
               // Hibernate will complain here for various reasons if there are problems with the classes
               // we'll be persisting (e.g. errors in the .hbm.xml) 
               // Unfortunately this code gets called ATM from a static initialiser, and it can be difficult
               // to see where the exception gets thrown.  Printing it out here should make it easier to spot problems.
            System.out.println(e.getMessage());
            throw e;
        }
	}
	
	public static void shutdown() throws HibernateException {
		sessionFactory.close();
		sessionFactory = null;
		threadSession = null;
		threadTransaction = null;
	}

	public static Session getSession() throws HibernateException {
		assert sessionFactory != null;
		Session s = (Session) threadSession.get();
		// Open a new Session, if this thread has none yet
		if (s == null) {
			s = sessionFactory.openSession();
			threadSession.set(s);
		}
		return s;
	}
	
	/**
	 * Commits the current transaction and begins a new transaction
	 * in a new session. This is useful to really test database changes,
	 * since if you are using the same session you're just testing
	 * in-memory objects. 
	 * 
	 * @throws HibernateException
	 */
	public static void newTestTransaction() throws HibernateException {
		commitTransaction();
		closeSession();
		beginTransaction();
	}
	
	public static void closeSession() throws HibernateException {
		Session s = (Session) threadSession.get();
		threadSession.set(null);
		if (s != null && s.isOpen()) {
			
			// If there is an uncommited/rollbacked transaction open, try to roll it back
			Transaction tx = (Transaction) threadTransaction.get();
			threadTransaction.set(null);
			try {
				if (tx != null && !tx.wasCommitted() && !tx.wasRolledBack()) {
					tx.rollback();
				}
			} catch (Exception e) {
				// Whatever, we tried at least
			}
			
			s.close();
		}
	}
	
	public static void beginTransaction() throws HibernateException {
		Transaction tx = (Transaction) threadTransaction.get();
		if (tx == null) {
			Session s = getSession();
			tx = s.beginTransaction();
			threadTransaction.set(tx);
		}
	}

	public static void commitTransaction() throws HibernateException {
		Transaction tx = (Transaction) threadTransaction.get();
		try {
			if (tx != null && !tx.wasCommitted() && !tx.wasRolledBack()) {
				tx.commit();
			}
			threadTransaction.set(null);
		} catch (HibernateException ex) {
			rollbackTransaction();
			throw ex;
		}
	}
	
	public static void rollbackTransaction() throws HibernateException {
		Transaction tx = (Transaction) threadTransaction.get();
		try {
			threadTransaction.set(null);
			if (tx != null && !tx.wasCommitted() && !tx.wasRolledBack()) {
				tx.rollback();
			}
		} finally {
			closeSession();
		}
	}

	
}
