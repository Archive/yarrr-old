package org.gnome.yarrr.hibernate;

import java.io.Serializable;
import java.security.PublicKey;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gnome.yarrr.Cryptography;
import org.hibernate.HibernateException;
import org.hibernate.usertype.UserType;


/**
 * Class to store PublicKey in a database
 * 
 * @author alex
 */
public class PublicKeyUserType implements UserType {
	private static final int[] SQL_TYPES = { Types.VARBINARY };
    private static final Log log = LogFactory.getLog(PublicKeyUserType.class);
    
	public int[] sqlTypes() {
		return SQL_TYPES;
	}

	public Class returnedClass() {
		return PublicKey.class;
	}

	public boolean equals(Object x, Object y) throws HibernateException {
		if (x == y) 
			return true;
		if (x == null || y == null)
			return false;
		return x.equals(y);
	}

	public Object nullSafeGet(ResultSet resultSet, String[] names, Object owner)
			throws HibernateException, SQLException {
		if (resultSet.wasNull()) 
			return null;
		byte [] data = resultSet.getBytes(names[0]);
		try {
			return Cryptography.bytesToPublicKey(data);
		} catch (Cryptography.EncryptionException e) {
			log.error("Can't convert data to public key", e);
			return null;
		}
	}


	public void nullSafeSet(PreparedStatement statement, Object value, int index)
			throws HibernateException, SQLException {
		if (value == null) {
			statement.setNull(index, Types.BINARY);
		} else {
			PublicKey key = (PublicKey) value;
			byte [] data = null;
			try {
				data = Cryptography.publicKeyToBytes(key);
			} catch (Cryptography.EncryptionException e) {
				log.error("Can't convert public key to data", e);
			}
			statement.setBytes(index, data);
		}
	}

	public Object deepCopy(Object value) throws HibernateException {
		return value;
	}

	public boolean isMutable() {
		return false;
	}
	
	public int hashCode(Object x) throws HibernateException {
		return ((PublicKey)x).hashCode();
	}

	public Serializable disassemble(Object value) throws HibernateException {
		return ((PublicKey)value);
	}

	public Object assemble(Serializable cached, Object owner) throws HibernateException {
		return cached;
	}

	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return original;
	}
}
