/*
 * Created on Mar 13, 2005
 */
package org.gnome.yarrr;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.hibernate.Persistent;
import org.gnome.yarrr.person.Person;
import org.hibernate.HibernateException;

/**
 * @author seth
 */
public class Statement extends Persistent {
	private static final Log log = LogFactory.getLog(Statement.class);

	// Immutable
	private Person creator;
	private String statement;
	private Timestamp createdTime;
	
	// Mutable
	private Set peopleMakingStatement = new HashSet();
	
	protected Statement() { }
	
	public Statement(String statement, Person creator) {
		this(); 
		this.statement = statement;
		this.createdTime = new Timestamp(new Date().getTime());
		this.creator = creator;
		this.peopleMakingStatement.add(creator);
		
		HibernateUtil.getSession().save(this);
	}
	
	public void addPersonToStatement(Person person) {
		this.peopleMakingStatement.add(person);
	}
	
	public void dropPersonFromStatement(Person person) {
		this.peopleMakingStatement.remove(person);
	}
	
	public String getStatement() {
		return this.statement;
	}
	public void setStatement(String statement) {
		this.statement = statement;
	}
	public Set getPeopleMakingStatement() {
		return this.peopleMakingStatement;
	}
	public void setPeopleMakingStatement(Set peopleMakingStatement) {
		this.peopleMakingStatement = peopleMakingStatement;
	}
	public Timestamp getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	public Person getCreator() {
		return creator;
	}
	public void setCreator(Person creator) {
		this.creator = creator;
	}
	
    public static void deleteAll() throws HibernateException, SQLException {
        java.sql.Statement statement = HibernateUtil.getSession().connection().createStatement();
        statement.execute("DELETE FROM Statement");    
    }
}
