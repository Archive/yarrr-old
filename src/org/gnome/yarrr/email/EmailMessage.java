/*
 * Created on 22-Feb-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.gnome.yarrr.email;

import java.sql.Timestamp;

import org.gnome.yarrr.Document;
import org.gnome.yarrr.Message;
import org.gnome.yarrr.Topic;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.person.Person;
import org.hibernate.HibernateException;

/**
 * @author dmalcolm
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class EmailMessage extends Message {
    Email sourceEmail;
    
    protected EmailMessage() {
        super();
    }

    public Email getSourceEmail() {
        return sourceEmail;
    }
    private void setSourceEmail(Email sourceEmail) {
        this.sourceEmail = sourceEmail;
    }

    /**
     * @param sentTime
     * @param authorPublicKey
     * @param subject
     * @param mimetype
     * @param document
     * @throws HibernateException
     */
    public EmailMessage(Timestamp sentTime, Person author, String subject, Long replyToId, Document document, Topic topic, Email sourceEmail) throws HibernateException {
        super(sentTime, author, subject, replyToId, document, topic, false);        
        this.sourceEmail = sourceEmail;
        
        HibernateUtil.getSession().save(this);
    }
    
    public static EmailMessage getByEmailId(String emailId) throws HibernateException {
        Email email = Email.getByEmailId(emailId);
        
        if (email!=null) {
            return email.getCreatedMessage(); // could be null
        }
        return null;
    }
    
}
