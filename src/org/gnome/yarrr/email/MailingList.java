/*
 * Created on 07-Feb-2005
 *
 */
package org.gnome.yarrr.email;

import java.sql.SQLException;
import java.sql.Statement;

import org.hibernate.HibernateException;
import org.hibernate.criterion.Expression;

import org.gnome.yarrr.Topic;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.hibernate.Persistent;

/**
 * @author dmalcolm
 *
 */
public class MailingList extends Persistent {
    Topic topic;
    String postingAddress;
    
    protected MailingList() {        
    }
    
    public MailingList(Topic topic, String postingAddress) throws HibernateException {
        this.topic = topic;
        this.postingAddress = postingAddress;
        HibernateUtil.getSession().save(this);
    }
    public String getPostingAddress() {
        return postingAddress;
    }
    public void setPostingAddress(String postingAddress) {
        this.postingAddress = postingAddress;
    }
    public Topic getTopic() {
        return topic;
    }
    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public static MailingList getMailingList(String postingAddress) throws HibernateException {
        
        return (MailingList)HibernateUtil.getSession().createCriteria(MailingList.class)        
                .add(Expression.eq("postingAddress", postingAddress))
                .uniqueResult();
    }

    public static void deleteAll() throws HibernateException, SQLException {
        Statement statement = HibernateUtil.getSession().connection().createStatement();
        statement.execute("DELETE FROM MailingList");
    }
};
    
   
