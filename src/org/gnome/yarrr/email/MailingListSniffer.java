/*
 * Created on 08-Feb-2005
 *
 */
package org.gnome.yarrr.email;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * @author dmalcolm
 *
 */
public class MailingListSniffer {
    class MailingListRegex {
        String headerName;
        Pattern pattern;        
        
        public MailingListRegex(String name, String pattern) {
            this.headerName = name;
            this.pattern = Pattern.compile(pattern);            
        }

        public String matchMessage(MimeMessage mimeMessage) throws MessagingException {
            String[] headerValues = mimeMessage.getHeader(headerName);
            
            for (int i=0;i<headerValues.length;i++) {
                Matcher matcher = pattern.matcher(headerValues[i]);
                if (matcher.find()) {
                    if (matcher.groupCount()>=2) {
                        return matcher.group(1) + "@" + matcher.group(2);
                    } else {
                        return matcher.group(1);
                    }
                }
            }
            
            return null;
        }
    };
    
    MailingListRegex[] regexes;
    Matcher[] regexMatchers;    
    
    public MailingListSniffer() {
        regexes =  new MailingListRegex[] {
                /* regex to match List-Post headers e.g. "List-Post: <mailto:desktop-devel-list@gnome.org>" */
                new MailingListRegex("List-Post", "<mailto:([^@]+)@([^>]+)>")
            };
    }   

    public InternetAddress getMailingListAddress(MimeMessage mimeMessage) throws MessagingException {
        for (int i=0;i<regexes.length; i++) {
            String result = regexes[i].matchMessage(mimeMessage);
            
            if (null!=result) {
                return new InternetAddress(result);            
            }
        }    
        return null;
    }
                

}
