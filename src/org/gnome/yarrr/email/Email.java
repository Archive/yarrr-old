/*
 * Created on 24-Feb-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.gnome.yarrr.email;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import org.hibernate.HibernateException;

import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.hibernate.Persistent;

/**
 * @author dmalcolm
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Email extends Persistent {
    private byte[] rawData; // persists
    private String emailId; // generated from rawData, but persists, so we can do queries; this is different from the main Persistent id
    private String parentId; // generated from rawData, but persists, so we can do queries 
    private EmailMessage createdMessage; // created once all parent emails in thread have arrived

    MimeMessage mimeMessage; // not persistent; lazily regenerated on demand
    
    // Add some POJO
    protected Email() {        
    }
    
    public Email(byte[] rawData) throws HibernateException, MessagingException {
        setRawData(rawData);        
        HibernateUtil.getSession().save(this);
    }
    
    public byte[] getRawData() {
        return rawData;
    }
    public void setRawData(byte[] rawData) throws MessagingException {
        this.rawData = rawData;
        ensureParsed();
        this.emailId = mimeMessage.getMessageID();
        this.parentId = mimeMessage.getHeader("In-Reply-To", null);
    }
    
    private void setEmailId(String emailID) {
        this.emailId = emailID;                
    }
    public String getEmailId() {
        return emailId;            
    }
    private void setParentId(String emailID) {
        this.parentId = emailID;                
    }
    public String getParentId() {
        return parentId;            
    }

    void setCreatedMessage(EmailMessage emailMessage) {
        this.createdMessage = emailMessage;                
    }
    public EmailMessage getCreatedMessage() {
        return createdMessage;            
    }
    
    private void ensureParsed() throws MessagingException {
        // Lazily parse the message:
        if (mimeMessage==null) {
            Session session = Session.getInstance(System.getProperties());
            InputStream stream = new ByteArrayInputStream(this.rawData);        
            this.mimeMessage = new MimeMessage(session, stream);
        }
        
        assert mimeMessage!=null;
    }

    public MimeMessage getMimeMessage() throws MessagingException {
        ensureParsed();
        return mimeMessage;
    }
    
    /*
     * Should this Email instance hand a corresponding EmailMessage?
     * (true is entire ancestry (if any) has an EmailMessage)
     */
    public boolean shouldHaveMessage() throws MessagingException, HibernateException {
        String parentId = getParentId();
        if (parentId!=null) {
            Email parentEmail = Email.getByEmailId(parentId);
            if (parentEmail!=null) {
                // Should only have an EmailMessage if the parent has one:
                return parentEmail.getCreatedMessage()!=null;
            } else {
                // Parent email is not yet in our database (arrived out of order?  was lost?)
                return false;
            }
        } else {
            // If no parent, then definitely should have an EmailMessage:
            return true;
        }        
    }

    public static Email getByEmailId(String emailId) throws HibernateException {
        return (Email)HibernateUtil.getSession()
            .createQuery("from Email email WHERE email.emailId = :emailid")
            .setString("emailid", emailId).uniqueResult();
    }
    
    /**
     * Locate those Email instances which can now have EmailMessage instance
     * generated (which do not already have them):  
     * @throws HibernateException 
     */
    public static List getMessagifiableChildren(Email newlyInjectedEmail) throws HibernateException {
        // get all Emails for which both Message is null AND satisfying:
        // in-reply-to is a Email with a non-NULL Message
        return HibernateUtil.getSession()
            .createQuery("from Email email WHERE email.createdMessage is null and email.parentId = :newlyInjectedEmailId")
            .setString("newlyInjectedEmailId", newlyInjectedEmail.getEmailId())
            .list();
    }

    public static List getMessagifiableChildren(String emailId) throws HibernateException {
        // get all Emails for which both Message is null AND satisfying:
        // in-reply-to is the given email        
        return HibernateUtil.getSession()
            .createQuery("from Email email WHERE email.createdMessage is null and email.parentId = :emailId")
            .setString("emailId", emailId)
            .list();
    }
    
    /**
     * Get all Email instances for which no EmailMessage has yet been created
     * @throws HibernateException 
     */
//    private static List locateUnmessagified() throws HibernateException {
//        return HibernateUtil.getSession()
//        .createQuery("from Email email WHERE email.createdMessage = null").list();        
//    }

    public static Email getEmail(long id) throws org.gnome.yarrr.hibernate.Persistent.NoMatchFoundException, HibernateException {
        Email email = (Email)HibernateUtil.getSession().get(Email.class, new Long(id));
        if (email == null)
            throw new NoMatchFoundException("no id " + id);
        return email;
    }

    public static void deleteAll() throws HibernateException, SQLException {
        Statement statement = HibernateUtil.getSession().connection().createStatement();
        statement.execute("DELETE FROM Email");
    }
    
}
