/*
 * Created on 04-Feb-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.gnome.yarrr.email;

import java.io.IOException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.gnome.yarrr.Cryptography;
import org.gnome.yarrr.Document;
import org.gnome.yarrr.Topic;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.person.Person;
import org.hibernate.HibernateException;

/**
 * @author dmalcolm
 *
 */
public class Importer {

    public class EmailMessageInsertion {
        public EmailMessage emailMessage;
        public Topic topic;

        public EmailMessageInsertion(EmailMessage emailMessage, Topic topic) {
            this.topic = topic;
            this.emailMessage = emailMessage;
        }
    }
    
    public final class InjectionResult {
        public Email email;        
        public List messageInsertions; // EmailMessageInsertions        
                
        public InjectionResult(Email email, List messageInsertions) {
            this.email = email;
            this.messageInsertions = messageInsertions;            
        }
    }
    
    public Person getAuthorForInjectedMail(MimeMessage mimeMessage) throws NoSuchAlgorithmException, NoSuchProviderException{
        // TODO: Message instances require an author public key
        // Exactly how should this be implemented?  Dunno, so for now 
        // just generate a new keypair for each message we inject (!)
        // Ultimately we might want to be able to identify people in some smart fashion...
        // signed emails?  or heuristics?
        // Depends on how I can best wind up Seth (or Havoc)
        KeyPair pair = Cryptography.generateKeyPair();
        return Person.getPerson(pair.getPublic());
    }
    
    public Topic getTopicForInjectedMail(MimeMessage mimeMessage, EmailMessage parentEmailMessage) throws HibernateException, MessagingException {
    
        if (parentEmailMessage!=null) {
            // insert into the same topics of the parent email (allowing you to reparent a thread):
            return parentEmailMessage.getTopic();
        }
        
        
        // Otherwise, sniff the mailing list:
        List result = new LinkedList();
        
        MailingListSniffer sniffer = new MailingListSniffer();
        InternetAddress address = sniffer.getMailingListAddress(mimeMessage);
        if (address!=null) {
            // Email is from a mailing list; lazily create a topic
            MailingList list = MailingList.getMailingList(address.getAddress());
            
            if (list!=null) {
                Topic listTopic = list.getTopic();
            
                // Simply use the list topic:
                return listTopic;
            }
        }        
        
        return null;
    }
    public String createHtmlForInjectedEmail(MimeMessage mimeMessage) throws MessagingException, IOException {
        HtmlWriter writer = new HtmlWriter();
        return writer.createHtml(mimeMessage);
    }
    
    public Document createDocumentForInjectedEmail(MimeMessage mimeMessage) throws HibernateException, MessagingException, IOException {
        String htmlFragment = createHtmlForInjectedEmail(mimeMessage);
        return Document.createDocument(htmlFragment);
    }

    private Timestamp getTimestampForInjectedEmail(MimeMessage mimeMessage) throws MessagingException {
        Date date = mimeMessage.getSentDate();
        
        if (date!=null) {            
            return new Timestamp(date.getTime());
        } else {
            return null;
        }
    }

    /**
     * TODO: which user? generate a key on startup to a file?
     */
    public InjectionResult injectEmail(byte [] rawData) throws Exception {        
        Email injectedEmail;
        List messageInsertions = new LinkedList();

        try {
            HibernateUtil.beginTransaction();

            injectedEmail = new Email(rawData);

            HibernateUtil.commitTransaction();
        } finally { 
            HibernateUtil.closeSession();            
        }        

        recursivelyMessagifyEmails(injectedEmail, messageInsertions);
            
        InjectionResult result = new InjectionResult(injectedEmail, messageInsertions);            
        return result;            
    }
    
    public void recursivelyMessagifyEmails(Email email, List messageInsertions) throws HibernateException, MessagingException, IOException, NoSuchProviderException, IOException, NoSuchAlgorithmException {
        if (email.shouldHaveMessage()) {            
            EmailMessageInsertion insertion = makeMessageFromEmail(email);
            messageInsertions.add(insertion);
            
            // Now repeatedly locate the Email instances which can have EmailMessages generated for them, and generate them:
            // (and repeat, since this may allow other emails to become messagifiable)           
            List unmessagified = Email.getMessagifiableChildren(email);
            for (Iterator i=unmessagified.iterator(); i.hasNext(); ) {
                Email candidateEmail = (Email)i.next();                
                recursivelyMessagifyEmails(candidateEmail, messageInsertions);            
            }
        }
    }
    
    /**
     * @param email
     * @throws HibernateException 
     * @throws MessagingException 
     * @throws NoSuchProviderException 
     * @throws NoSuchAlgorithmException 
     * @throws IOException 
     */
    private EmailMessageInsertion makeMessageFromEmail(Email email) throws HibernateException, MessagingException, NoSuchAlgorithmException, NoSuchProviderException, IOException {
        EmailMessage emailMessage;
        Topic topic;
        try {
            EmailMessage parentEmailMessage = null;
            String parentEmailId = email.getParentId();
            
            HibernateUtil.beginTransaction();
            
            if (parentEmailId!=null) {
                parentEmailMessage = EmailMessage.getByEmailId(parentEmailId);
                assert parentEmailMessage!=null;
            }
            
            MimeMessage mimeMessage = email.getMimeMessage();
            
            // Determine which user this is
            Person author = getAuthorForInjectedMail(mimeMessage);
            
            // Determine which topic(s) to put the email in:
            topic = getTopicForInjectedMail(mimeMessage, parentEmailMessage);
            
            Document doc = createDocumentForInjectedEmail(mimeMessage);
            Timestamp timestamp = getTimestampForInjectedEmail(mimeMessage);
            String subject = mimeMessage.getSubject();
            
            // Set up message threading based on email threading:
            Long replyTo = null;
            if (parentEmailId!=null) {
                assert parentEmailMessage!=null;
                replyTo = parentEmailMessage.getId();
            }

            emailMessage = new EmailMessage(null, author, subject, replyTo, doc, topic, email);
            email.setCreatedMessage(emailMessage);
            
            // Ensure changes to email make it to the database:
            // TODO: should this be here?
            HibernateUtil.getSession().update(email);
            if (emailMessage!=null) {
                HibernateUtil.getSession().update(emailMessage);
            }
            
            HibernateUtil.commitTransaction();            
            
        } finally {
            HibernateUtil.closeSession();
        }
        
        return new EmailMessageInsertion(emailMessage, topic);        
    }    
}
