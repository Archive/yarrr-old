/*
 * Created on Feb 5, 2005
 *
 */
package org.gnome.yarrr.email;

import java.io.IOException;
import java.util.Enumeration;

import javax.mail.Header;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


/**
 * @author dmalcolm
 *
 */
public class HtmlWriter {
    public String createHtmlForHeaders(MimeMessage mimeMessage) throws MessagingException, IOException {
        StringBuffer buf = new StringBuffer();

        for (Enumeration i = mimeMessage.getAllHeaders();i.hasMoreElements(); ) {
            Header header = (Header)i.nextElement();
            buf.append("<div class=\"email-header\">");
            buf.append("<div class=\"email-header-name\">");
            buf.append(header.getName());
            buf.append(": </div>");
            buf.append("<div class=\"email-header-value\">");
            buf.append(header.getValue());
            buf.append("</div>");            
            buf.append("</div>");
        }
        return buf.toString();
    }
    public String createHtmlForContent(Object content) throws MessagingException, IOException {
        // can be various things; handcoded dispatch accordingly
        
        // java.lang.String:
        if (String.class.isInstance(content)) {
            return convertLinesToParas(escapeHtml((String)content));
        } 
        
        // javax.mail.internet.MimeMultipart:
        if (MimeMultipart.class.isInstance(content)) {
            return convertMimeMultipart((MimeMultipart)content);
        }
        
        return "Unknown content class: " + content.getClass().getName(); 
    }
    
    private String convertMimeMultipart(MimeMultipart multipart) throws MessagingException, IOException {
        StringBuffer buf = new StringBuffer();
        for (int i=0; i<multipart.getCount(); i++) {
            buf.append("<div><p>Multipart " + String.valueOf(i) + ":</p>");
            buf.append(createHtmlForContent(multipart.getBodyPart(i).getContent()));
            buf.append("</div>");
        }
        return buf.toString();        
    }
    private String convertLinesToParas(String string) {
        // FIXME: this is a ghastly hack... 
        // This chops up the email content to <p> nodes based on entirely empty
        // lines.
        return "<p>" + GCJWorkarounds.replace(string, "\n\n", "</p><p>") +"</p>";   
    }

    public String escapeHtml(String string) {
        string = GCJWorkarounds.replace(string, "&", "&amp;");
        string = GCJWorkarounds.replace(string, "'", "&apos;");
        string = GCJWorkarounds.replace(string, ">", "&gt;");
        string = GCJWorkarounds.replace(string, "<", "&lt;");
        string = GCJWorkarounds.replace(string, "\"", "&quot;");
        return string;
    }

    public String createHtml(MimeMessage mimeMessage) throws MessagingException, IOException {
        StringBuffer buf = new StringBuffer();
        buf.append("<div class=\"email\">");

        // Generate some debug HTML from the headers:
        buf.append("<div class=\"email-headers\">");
        //buf.append(createHtmlForHeaders(mimeMessage));
        buf.append("</div>"); /* class="email-headers" */
        
        buf.append("<div class=\"email-content-class\">");
        buf.append(mimeMessage.getContent().getClass().getName());
        buf.append("</div>"); /* class="email-content" */
        
        // Generate some HTML for the content:
        buf.append("<div class=\"email-content\">");
        buf.append(createHtmlForContent(mimeMessage.getContent()));
        buf.append("</div>"); /* class="email-content" */
        
        buf.append("</div>"); /* class="email" */
        return buf.toString();
    }
    
}
