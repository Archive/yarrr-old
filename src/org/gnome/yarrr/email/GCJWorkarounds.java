package org.gnome.yarrr.email;


/**
 * @author walters
 *
 */
public class GCJWorkarounds {
    
    // libgcj4-4.0.0-0.20 doesn't implement String.replace
    public static String replace(String str, CharSequence oldSeq, CharSequence newSeq)
    {
        int i;
        StringBuffer buf = new StringBuffer();
        int oldLen = oldSeq.length();
        String newSeqStr = newSeq.toString();
        
        if (oldLen == 0) {
            buf.append(str);
            return buf.toString();
        }
           
        for (i = 0; i + (oldLen-1) < str.length(); ) {
            if (str.substring(i, i + oldLen).compareTo(oldSeq) == 0) {
                buf.append(newSeqStr);
                i += oldLen;
            } else {
                buf.append(str.charAt(i));
                i++;
            }
        }
        // Append remaining bits
        buf.append(str.substring(i));
        
        return buf.toString();
    }
}
