/*
 * Created on Jan 31, 2005
 *
 */
package org.gnome.yarrr.tests;

import java.util.HashMap;

import org.gnome.yarrr.Document;
import org.gnome.yarrr.Resource;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.hibernate.StaleObjectStateException;

/**
 * @author alex
 */
public class DocumentTest extends HibernateUsingTest {

	static String testData = "This is some test data. It should not be changed during storage. Some random unicode chars: \u0090 \u0153, \u0234."; 
	static String testData2 = "This is some more test data. Some random unicode chars: \u0390 \u0133, \u0134."; 
	static String testData3 = "This is even more test data. Some random unicode chars: \u0193 \u0246, \u04ae."; 
	static String testData4 = "This is getting ridiculous. \u0193 \u0246, \u04ae."; 
	static String testData5 = "Yeah yeah.";
	static String testData6 = "Are we done yet?";
	
	public void testSimpleCreateDocument() throws Exception {
		long id;
		
		Document doc = Document.createDocument(testData.getBytes("UTF-8"));
		assertEquals(1, doc.getCurrentVersion());
		assertEquals(1, doc.getResources().size());
		id = doc.getDocumentId();
		
		HibernateUtil.newTestTransaction();
		
		Document doc2 = Document.get(id);
		assertEquals(1, doc2.getCurrentVersion());
		assertEquals(1, doc2.getResources().size());
		
		Resource res = doc2.getResource("main");
		String s = new String(res.getBytes(), "UTF-8");
		assertEquals(testData, s);
	}

	public void testSimpleUpdateDocument() throws Exception {
		long id;

		// Create document
		Document doc = Document.createDocument(testData.getBytes("UTF-8"));
		assertEquals(1, doc.getCurrentVersion());
		assertEquals(1, doc.getResources().size());
		id = doc.getDocumentId();
					
		HibernateUtil.newTestTransaction();
		
		doc = Document.get(id);
		assertEquals(1, doc.getCurrentVersion());
		assertEquals(1, doc.getResources().size());

		doc.updateDocument(testData2.getBytes("UTF-8"), doc.getCurrentVersion());
					
		HibernateUtil.newTestTransaction();

		// Read data in separate session
		Document doc2 = Document.get(id);
		assertEquals(2, doc2.getCurrentVersion());
		assertEquals(2, doc2.getResources().size());

		// Verify current version
		Resource res = doc2.getResource("main");
		String s = new String(res.getBytes(), "UTF-8");
		assertEquals(testData2, s);

		// Verify first version
		res = doc2.getResource("main", 1);
		s = new String(res.getBytes(), "UTF-8");
		assertEquals(testData, s);
	}

	public void testMultiCreateDocument() throws Exception {
		long id;
		
		HashMap map = new HashMap();
		map.put("main", testData.getBytes("UTF-8"));
		map.put("extra", testData2.getBytes("UTF-8"));
			
		Document doc = Document.createDocument(map);
		assertEquals(1, doc.getCurrentVersion());
		assertEquals(2, doc.getResources().size());
		id = doc.getDocumentId();
		
		HibernateUtil.newTestTransaction();
		
		// Read data in separate session
		Document doc2 = Document.get(id);
		assertEquals(1, doc2.getCurrentVersion());
		assertEquals(2, doc2.getResources().size());

		Resource res = doc2.getResource("main");
		String s = new String(res.getBytes(), "UTF-8");
		assertEquals(testData, s);

		res = doc2.getResource("extra");
		s = new String(res.getBytes(), "UTF-8");
		assertEquals(testData2, s);
	}

	public void testMultiUpdateDocument() throws Exception {
		long id;

		// Create document
		
		HashMap map = new HashMap();
		map.put("main", testData.getBytes("UTF-8"));
		map.put("extra", testData2.getBytes("UTF-8"));
		
		Document doc = Document.createDocument(map);
		assertEquals(1, doc.getCurrentVersion());
		assertEquals(2, doc.getResources().size());
		id = doc.getDocumentId();
		
		HibernateUtil.newTestTransaction();

		// Update document
		doc = Document.get(id);
		assertEquals(1, doc.getCurrentVersion());
		assertEquals(2, doc.getResources().size());
		
		map = new HashMap();
		map.put("main", testData3.getBytes("UTF-8"));
		map.put("extra", testData4.getBytes("UTF-8"));
		
		doc.updateDocument(map, doc.getCurrentVersion());
		
		HibernateUtil.newTestTransaction();
		
		// Update document again
		doc = Document.get(id);
		assertEquals(2, doc.getCurrentVersion());
		assertEquals(4, doc.getResources().size());
		
		// These two updates should become one version change since 
		// they are in the same transaction
		doc.updateDocument("main", testData5.getBytes("UTF-8"), doc.getCurrentVersion());
		doc.updateDocument("extra2", testData6.getBytes("UTF-8"), doc.getCurrentVersion());
		
		HibernateUtil.newTestTransaction();
				
		// Read data in separate session
		doc = Document.get(id);
		assertEquals(3, doc.getCurrentVersion());
		assertEquals(6, doc.getResources().size());

		// Verify current version (3)
		Resource res = doc.getResource("main");
		assertEquals(3, res.getVersion());
		String s = new String(res.getBytes(), "UTF-8");
		assertEquals(testData5, s);
		
		res = doc.getResource("extra");
		assertEquals(2, res.getVersion());
		s = new String(res.getBytes(), "UTF-8");
		assertEquals(testData4, s);
		
		res = doc.getResource("extra2");
		assertEquals(3, res.getVersion());
		s = new String(res.getBytes(), "UTF-8");
		assertEquals(testData6, s);
		
		// Verify version 2
		
		res = doc.getResource("main", 2);
		assertEquals(2, res.getVersion());
		s = new String(res.getBytes(), "UTF-8");
		assertEquals(testData3, s);
		
		res = doc.getResource("extra", 2);
		assertEquals(2, res.getVersion());
		s = new String(res.getBytes(), "UTF-8");
		assertEquals(testData4, s);
		
		// Verify version 1
		
		res = doc.getResource("main", 1);
		assertEquals(1, res.getVersion());
		s = new String(res.getBytes(), "UTF-8");
		assertEquals(testData, s);
		
		res = doc.getResource("extra", 1);
		assertEquals(1, res.getVersion());
		s = new String(res.getBytes(), "UTF-8");
		assertEquals(testData2, s);
	}
	
	public void testSimpleUpdateConflict() throws Exception {
		long id;
		
		// Create document
		Document doc = Document.createDocument(testData.getBytes("UTF-8"));
		assertEquals(1, doc.getCurrentVersion());
		assertEquals(1, doc.getResources().size());
		id = doc.getDocumentId();
					
		HibernateUtil.newTestTransaction();

		// Update document
		doc = Document.get(id);
		assertEquals(1, doc.getCurrentVersion());
		assertEquals(1, doc.getResources().size());

		doc.updateDocument(testData2.getBytes("UTF-8"), 1);
				
		HibernateUtil.newTestTransaction();

		boolean gotException = false;
		// Update document against the non-current version
		try {
			doc = Document.get(id);
			// We assert that its version 2, because it is, 
			// but we try to update against version 1 
			assertEquals(2, doc.getCurrentVersion());
			assertEquals(2, doc.getResources().size());

			doc.updateDocument(testData2.getBytes("UTF-8"), 1);
				
			HibernateUtil.commitTransaction();
		} catch (StaleObjectStateException e) { 
			gotException = true;
		} 
		
		assertTrue(gotException);
	}

	class DocumentUpdaterThread extends Thread {
		long id;
		String data;
		
		public DocumentUpdaterThread(long id, String data) {
			super("Document Updater");
			this.id = id;
			this.data = data;
		}
		public void run() {
			try {
				HibernateUtil.beginTransaction();
					
				Document doc = Document.get(id);
				doc.updateDocument(data.getBytes("UTF-8"), doc.getCurrentVersion());
				
				HibernateUtil.commitTransaction();
			} catch (Exception e) {
				assertTrue("Shouldn't reach this", false);
			} finally {
				try {
					HibernateUtil.closeSession();
				} catch (Exception e) {
					assertTrue("Shouldn't reach this", false);
				}
			}
		}
	}
	
	public void testConcurrentUpdateConflict() throws Exception {
		long id;
		
		// Create document
		Document doc = Document.createDocument(testData.getBytes("UTF-8"));
		assertEquals(1, doc.getCurrentVersion());
		assertEquals(1, doc.getResources().size());
		id = doc.getDocumentId();
		
		HibernateUtil.newTestTransaction();

		boolean gotException = false;
		
		// Update document in normal session
		try {
			doc = Document.get(id);
			assertEquals(1, doc.getCurrentVersion());
			assertEquals(1, doc.getResources().size());

			// Update document concurrently in another thread
			DocumentUpdaterThread thread = new DocumentUpdaterThread(id, testData2);
			thread.start();
			thread.join();
			
			doc.updateDocument(testData3.getBytes("UTF-8"), 1);
			
			// This is supposed to get a StaleObjectStateException
			HibernateUtil.commitTransaction();
		} catch (StaleObjectStateException e) { 
			gotException = true;
		} 
	
		assertTrue(gotException);

		HibernateUtil.newTestTransaction();

		// Make sure we got the data from the first update
		doc = Document.get(id);
		assertEquals(2, doc.getCurrentVersion());
		assertEquals(2, doc.getResources().size());
		
		Resource res = doc.getResource("main");
		String s = new String(res.getBytes(), "UTF-8");
		assertEquals(testData2, s);
	}
	
}
