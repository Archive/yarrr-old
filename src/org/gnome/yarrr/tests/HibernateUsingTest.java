/*
 * Created on Mar 9, 2005
 */
package org.gnome.yarrr.tests;

import org.gnome.yarrr.Document;
import org.gnome.yarrr.Message;
import org.gnome.yarrr.Statement;
import org.gnome.yarrr.Topic;
import org.gnome.yarrr.email.Email;
import org.gnome.yarrr.email.MailingList;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.person.Group;
import org.gnome.yarrr.person.Person;

/**
 * @author alex
 */
public abstract class HibernateUsingTest extends DBUsingTest {
	
	protected void setUp() throws Exception {
		super.setUp();
		
		if (!HibernateUtil.initialized())
			HibernateUtil.init(HibernateUtil.buildConfiguration(getDB().getUrl()));
		
		// Each test is (at least) one transaction
		try {
			HibernateUtil.beginTransaction();
		} catch (Exception e) {
			HibernateUtil.closeSession();
			throw e;
		}
	}

    protected void tearDown() throws Exception {
    	// Commit the test transaction and close the session
		try {
			HibernateUtil.commitTransaction();
		}  finally {
			HibernateUtil.closeSession();
		}
		
		
		// Clean up the database
		try {
			HibernateUtil.beginTransaction();
            
			MailingList.deleteAll();
			Email.deleteAll();
			Message.deleteAll();
			Topic.deleteAll();
			Document.deleteAll();
			Group.deleteAll();
			Statement.deleteAll();
			Person.deleteAll();
                    
			HibernateUtil.commitTransaction();
    	} finally {
    		HibernateUtil.closeSession();
    	}
    	
        super.tearDown();
    }

}
