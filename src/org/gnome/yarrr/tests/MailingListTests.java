/*
 * Created on 08-Feb-2005
 *
 */
package org.gnome.yarrr.tests;

import java.util.List;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.gnome.yarrr.Topic;
import org.gnome.yarrr.email.Email;
import org.gnome.yarrr.email.EmailMessage;
import org.gnome.yarrr.email.Importer;
import org.gnome.yarrr.email.MailingList;
import org.gnome.yarrr.email.MailingListSniffer;
import org.gnome.yarrr.email.Importer.InjectionResult;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.person.Person;

public class MailingListTests extends PersonUsingTest {
    private static final String TEST_THREADING_1_EMAIL_ID = "<20050203220907.GF26787@localhost.localdomain>";
    private static final String TEST_THREADING_1_1_EMAIL_ID = "<51419b2c05020314381b3eadd2@mail.gmail.com>";
    private static final String TEST_THREADING_1_2_EMAIL_ID = "<20050204003108.GM26787@localhost.localdomain>";
    private static final String TEST_THREADING_1_2_1_EMAIL_ID = "<1108140962.5627.6.camel@otto.amantes>";
    
    private static final String TEST_ML_ADDRESS1 = "mutiny-announce@avast.me.hearties.co.uk";
    private static final String TEST_ML_ADDRESS2 = "automated-translation@spanish-main.function";
    
    public void testMailingList() throws Exception {
        long topicID1;
        long topicID2;
        
        Person firstMate = getPerson1();
        Topic topic1 = TestUtils.makeTestTopic("Postings to mutiny-annouce", firstMate, "Shhh! <b>Don't</b> tell the cap'n! or 'e'll have ye guts", null);
        Topic topic2 = TestUtils.makeTestTopic("Discussion of automated translation", firstMate, "It makes with such a JavaScript commuting, it is the substitute. As for those which it is possible there is no ginger, probably will be.", null);
        MailingList list1 = new MailingList(topic1, TEST_ML_ADDRESS1);
        MailingList list2 = new MailingList(topic2, TEST_ML_ADDRESS2);
            
        topicID1 = topic1.getId().longValue();            
        topicID2 = topic2.getId().longValue();
        HibernateUtil.newTestTransaction();

        list1 = MailingList.getMailingList(TEST_ML_ADDRESS1);
        list2 = MailingList.getMailingList(TEST_ML_ADDRESS2);
        assertEquals(TEST_ML_ADDRESS1, list1.getPostingAddress());            
        assertEquals(TEST_ML_ADDRESS2, list2.getPostingAddress());
            
        topic1 = list1.getTopic();
        assertNotNull(topic1);
        assertEquals(topicID1, topic1.getId().longValue());            

        topic2 = list2.getTopic();
        assertNotNull(topic2);
        assertEquals(topicID2, topic2.getId().longValue());            
    }

    public void testEmail() throws Exception {
        long emailId;
        byte[] rawData = EmailTests.getTestEmail("multiline-subject.email");

        Email email = new Email(rawData);
        emailId = email.getId().longValue();

        assertEquals("<1103092060.19014.38.camel@localhost.localdomain>", email.getEmailId());
        assertEquals("<1102940273.41bd88711fbdc@www.cirt.net>", email.getParentId());            
            
        HibernateUtil.newTestTransaction();
        
        Email queriedEmail = Email.getEmail(emailId);
        
        assertEquals("<1103092060.19014.38.camel@localhost.localdomain>", queriedEmail.getEmailId());
        assertEquals("<1102940273.41bd88711fbdc@www.cirt.net>", queriedEmail.getParentId());
        assertEquals(rawData.length, queriedEmail.getRawData().length);
        assertEquals(rawData[42], queriedEmail.getRawData()[42]);
    }

    public void testMailingListSniffer() throws Exception {
        MimeMessage message = EmailTests.loadTestEmail("multiline-subject.email");        
        
        MailingListSniffer sniffer = new MailingListSniffer();
        assertEquals(new InternetAddress("desktop-devel-list@gnome.org"), sniffer.getMailingListAddress(message));        
    }
    
    public void testInjectionTopicSelection() throws Exception {
        MimeMessage mimeMessage = EmailTests.loadTestEmail("multiline-subject.email");
        Importer importer = new Importer();
        // Haven't yet registered desktop-devel-list@gnome.org, so don't know where to put it: 
        assertNull(importer.getTopicForInjectedMail(mimeMessage, null));
        
        MailingListSniffer sniffer = new MailingListSniffer();
        InternetAddress address = sniffer.getMailingListAddress(mimeMessage);
        
        Person firstMate = getPerson1();
        Topic createdTopic = TestUtils.makeTestTopic("desktop-devel-list", firstMate, "This is where the action is.  Or is it?", null);
        MailingList list1 = new MailingList(createdTopic, address.getAddress());
        
        Topic queriedTopic = importer.getTopicForInjectedMail(mimeMessage, null);
        
        // Email ought to be put into the topic we just created:
        assertEquals(createdTopic, queriedTopic);
    }

    public void testInjectionPersistence() throws Exception {
        byte[] rawData = EmailTests.getTestEmail("multiline-subject.email");
        //InputStream stream = new ByteArrayInputStream(rawData);
        //Session session = Session.getInstance(System.getProperties());
        //MimeMessage mimeMessage = new MimeMessage(session, stream);
        
        long emailID;
        
        Person firstMate = getPerson1();
        Topic createdTopic = TestUtils.makeTestTopic("desktop-devel-list", firstMate, "This is where the action is.  Or is it?", null);
        InternetAddress address = new InternetAddress("desktop-devel-list@gnome.org");
        MailingList list1 = new MailingList(createdTopic, address.getAddress());
        
        Importer importer = new Importer();
        InjectionResult injectionResult = importer.injectEmail(rawData);
        Email email = (Email)injectionResult.email;
        
        emailID = email.getId().longValue();
        
        // Sanity test on the email:
        assertEquals(email.getRawData(), rawData);
        
        // Email ought to be put into the topic we just created:
        // assertEquals(createdTopic, injectionResult.topic);
        
        HibernateUtil.newTestTransaction();
        
        Email queriedEmail = (Email)Email.getEmail(emailID);
            
        assertEquals(rawData.length, queriedEmail.getRawData().length);
        assertEquals(rawData[56], queriedEmail.getRawData()[56]);
    }
    
    public void testThreading() throws Exception {
    	Person firstMate = getPerson1();
    	Topic createdTopic = TestUtils.makeTestTopic("desktop-devel-list", firstMate, "This is where the action is.  Or is it?", null);
    	InternetAddress address = new InternetAddress("desktop-devel-list@gnome.org");
    	MailingList list1 = new MailingList(createdTopic, address.getAddress());
    	
    	HibernateUtil.newTestTransaction();
    	
    	Importer importer = new Importer();
    	InjectionResult result1 = importer.injectEmail(EmailTests.getTestEmail("test-threading-1.email"));
    	checkInjectionResult(result1, 1, TEST_THREADING_1_EMAIL_ID, null);
    	
    	InjectionResult result11 = importer.injectEmail(EmailTests.getTestEmail("test-threading-1-1.email"));
    	checkInjectionResult(result11, 1, TEST_THREADING_1_1_EMAIL_ID, TEST_THREADING_1_EMAIL_ID);
    	assertNotNull(result11.email.getCreatedMessage());
    	
    	InjectionResult result12 = importer.injectEmail(EmailTests.getTestEmail("test-threading-1-2.email"));
    	checkInjectionResult(result12, 1, TEST_THREADING_1_2_EMAIL_ID, TEST_THREADING_1_EMAIL_ID);
    	assertNotNull(result12.email.getCreatedMessage());
    	
    	InjectionResult result121 = importer.injectEmail(EmailTests.getTestEmail("test-threading-1-2-1.email"));
    	checkInjectionResult(result121, 1, TEST_THREADING_1_2_1_EMAIL_ID, TEST_THREADING_1_2_EMAIL_ID);
    	assertNotNull(result121.email.getCreatedMessage());
    	
    	List queriedMessages =createdTopic.getAllMessages();
    	assertEquals(4, queriedMessages.size());
    	EmailMessage emailMessage0 = (EmailMessage)queriedMessages.get(0);
    	EmailMessage emailMessage1 = (EmailMessage)queriedMessages.get(1);
    	EmailMessage emailMessage2 = (EmailMessage)queriedMessages.get(2);
    	EmailMessage emailMessage3 = (EmailMessage)queriedMessages.get(3);
    	
    	assertEquals(null, emailMessage0.getReplyToId());
    	assertEquals(emailMessage0.getId().longValue(), emailMessage1.getReplyToId().longValue());
    	assertEquals(emailMessage0.getId().longValue(), emailMessage2.getReplyToId().longValue());
    	assertEquals(emailMessage2.getId().longValue(), emailMessage3.getReplyToId().longValue());            
    }

    public void testBackwardsInsertion() throws Exception {
    	Person firstMate = getPerson1();
    	Topic createdTopic = TestUtils.makeTestTopic("desktop-devel-list", firstMate, "This is where the action is.  Or is it?", null);
    	InternetAddress address = new InternetAddress("desktop-devel-list@gnome.org");
    	MailingList list1 = new MailingList(createdTopic, address.getAddress());
    	
    	HibernateUtil.newTestTransaction();            
    	
    	Importer importer = new Importer();
    	InjectionResult result121 = importer.injectEmail(EmailTests.getTestEmail("test-threading-1-2-1.email"));
    	checkInjectionResult(result121, 0, TEST_THREADING_1_2_1_EMAIL_ID, TEST_THREADING_1_2_EMAIL_ID);
    	assertNull(result121.email.getCreatedMessage());
    	assertEquals(TEST_THREADING_1_2_1_EMAIL_ID, result121.email.getEmailId());
    	assertEquals(TEST_THREADING_1_2_EMAIL_ID, result121.email.getParentId());
    	
    	// grrr.. this isn't finding the message for some reason
    	assertEquals(1, Email.getMessagifiableChildren(TEST_THREADING_1_2_EMAIL_ID).size());
    	
    	InjectionResult result12 = importer.injectEmail(EmailTests.getTestEmail("test-threading-1-2.email"));
    	checkInjectionResult(result12, 0, TEST_THREADING_1_2_EMAIL_ID, TEST_THREADING_1_EMAIL_ID);
    	assertNull(result12.email.getCreatedMessage());
    	assertEquals(1, Email.getMessagifiableChildren(TEST_THREADING_1_EMAIL_ID).size());
    	
    	InjectionResult result11 = importer.injectEmail(EmailTests.getTestEmail("test-threading-1-1.email"));
    	checkInjectionResult(result11, 0, TEST_THREADING_1_1_EMAIL_ID, TEST_THREADING_1_EMAIL_ID);
    	assertNull(result11.email.getCreatedMessage());
    	assertEquals(2, Email.getMessagifiableChildren(TEST_THREADING_1_EMAIL_ID).size());
    	
    	InjectionResult result1 = importer.injectEmail(EmailTests.getTestEmail("test-threading-1.email"));
    	
    	assertNotNull(result1.email.getCreatedMessage());
    	checkInjectionResult(result1, 4, TEST_THREADING_1_EMAIL_ID, null);
    	
    	List queriedMessages =createdTopic.getAllMessages();
    	assertEquals(4, queriedMessages.size());
    	EmailMessage emailMessage0 = (EmailMessage)queriedMessages.get(0);
    	EmailMessage emailMessage1 = (EmailMessage)queriedMessages.get(1);
    	EmailMessage emailMessage2 = (EmailMessage)queriedMessages.get(2);
    	EmailMessage emailMessage3 = (EmailMessage)queriedMessages.get(3);
    	
    	assertEquals(TEST_THREADING_1_EMAIL_ID, emailMessage0.getSourceEmail().getEmailId());            
    	assertEquals(null, emailMessage0.getReplyToId());
    	
    	// we can't assert much more about the other 3 messages since we can't guarantee the order
    	// in which emails 1-1 and 1-2 got messagified 
    }

    private void checkInjectionResult(InjectionResult result, int numMessagesCreated, String expectedEmailId, String expectedParentId) {
        assertEquals(expectedEmailId, result.email.getEmailId());
        assertEquals(expectedParentId, result.email.getParentId());
        assertEquals(numMessagesCreated, result.messageInsertions.size());        
    }
    
}

