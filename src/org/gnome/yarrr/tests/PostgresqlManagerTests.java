/*
 * Created on Jan 14, 2005
 */
package org.gnome.yarrr.tests;

import java.io.File;

import org.gnome.yarrr.database.IDatabaseManager;
import org.gnome.yarrr.database.PostgresqlManager;

/**
 * @author seth
 */
public class PostgresqlManagerTests extends DBUsingTest {

	private IDatabaseManager manager;
	private File tempDir;
	
	public static void main(String[] args) {
		junit.textui.TestRunner.run(PostgresqlManagerTests.class);
	}

	public void testEmpty () throws Exception {
		assertTrue(manager.isRunning());
		manager.stop();
		assertFalse(manager.isRunning());
		manager.start();
		assertTrue(manager.isRunning());
	}
	
	public void testNextAvailablePort() throws Exception {
		int port = ((PostgresqlManager)manager).findAvailablePort();
		System.out.println ("Got port " + port);
	}
	
	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		this.manager = getDB();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		super.tearDown();
		
	}

}
