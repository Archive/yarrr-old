/*
 * Created on 10-Feb-2005
 */
package org.gnome.yarrr.tests;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import org.hibernate.HibernateException;

import org.gnome.yarrr.Cryptography;
import org.gnome.yarrr.Message;
import org.gnome.yarrr.Topic;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.person.Person;
import org.gnome.yarrr.xmlrpc.DefaultMethods;

/**
 * @author dmalcolm
 */
public abstract class DefaultMethodsTests extends YarrrUsingTest {
    // Create a DefaultMethods instance; this will either be "direct", or delegate via XML-RPC to the exposed handler on the server 
    abstract public DefaultMethods getDefaultMethods() throws Exception;
    
    // Fixtures:
    public Topic getTestTopic() throws HibernateException {
        return (Topic) Topic.getAllTopics().get(0);
    }

    public String getTestTopicIDString() throws HibernateException {
        return getTestTopic().getId().toString();
    }
    
    // Tests for all the methods in DefaultMethods:
    public void testMethod_getTopic() throws Exception {
        Topic testTopic = getTestTopic();
        Hashtable hash = getDefaultMethods().getTopic(getTestTopicIDString());
        
        assertEquals(testTopic.getName(), hash.get("name"));
        assertEquals(testTopic.getStringId(), hash.get("id"));
    }
    
    public void testGetMessagesWrongDateRange() throws Exception {
    	int numMessagesAdded = 10;
    	
    	Date start = new Date(5l);
    	Date end = new Date(1000000l);

		String topicId = null;
		
    	// Set up database for test
    	try {
    		HibernateUtil.beginTransaction();
    	
    		Topic testTopic = TestUtils.addTestTopic();
    		List messages = TestUtils.addNMessages(testTopic, numMessagesAdded, true);
    		topicId = testTopic.getStringId();
    		
    		HibernateUtil.commitTransaction();
    	} finally {
    		HibernateUtil.closeSession();
    	}
            
        Vector topics = new Vector();
        topics.add(topicId);
    	
        Hashtable topicIDsToMessages = getDefaultMethods().getMessages(topics, start, end);
    		
      	Vector retrievedMessages = (Vector)topicIDsToMessages.get(topicId);
        assertEquals("No messages shoud be in the store for this made up date range", 0, retrievedMessages.size());
    }
    
    protected void doTestgetMessagesDateRange() throws Exception {
    	int numMessagesAdded = 10;
    	
    	String topicId;
    	Date start = new Date();

    	// Set up database for test
    	try {
    		HibernateUtil.beginTransaction();
    	
    		Topic testTopic = TestUtils.addTestTopic();
    		List messages = TestUtils.addNMessages(testTopic, numMessagesAdded, false);

        	topicId = testTopic.getStringId(); 

    		HibernateUtil.commitTransaction();
    	} finally {
    		HibernateUtil.closeSession();
    	}
    	
    	Date end = new Date();
    	
    	Vector topics = new Vector();
    	topics.add(topicId);
    	
    	Hashtable topicIDsToMessages = getDefaultMethods().getMessages(topics, start, end);
    	
    	Vector retrievedMessages = (Vector)topicIDsToMessages.get(topicId);
    	assertEquals(numMessagesAdded, retrievedMessages.size());
    }
    
    public void testMethod_getMessage() throws Exception {
    	String id = null;
    	// Set up database for test
    	try {
    		HibernateUtil.beginTransaction();
    	
    		Topic testTopic = getTestTopic();
    		Message testMessage;
    		testMessage = TestUtils.makeTestMessage(testTopic);

    		id = testMessage.getStringId();
    		
    		HibernateUtil.commitTransaction();
    	} finally {
    		HibernateUtil.closeSession();
    	}
        
        Hashtable hash = getDefaultMethods().getMessage(id);

        // TODO: finish writing this test
        // Message hashCreatedMessage = Utils.hashToMessage(hash, testMessage.getDocument());
        // assertEquals(testMessage, hashCreatedMessage);
    }
    
    /**
     * 
     * Test for DefaultMethods.heartbeat() and .getActivePeople()
     */
    public void testActivePeople() throws Exception {
        // Generate a keypair and a new person using it: 
        KeyPair keypair = Cryptography.generateKeyPair();
        Person person = Person.getPerson(keypair.getPublic());

        // First test of getActivePeople()
        Vector activePeople = getDefaultMethods().getActivePeople();
        assertEquals(0, activePeople.size());

        // Test of heartbeat():
        getDefaultMethods().heartbeat(Cryptography.publicKeyToBytes(keypair.getPublic()));
        
        // Second test of getActivePeople()
        activePeople = getDefaultMethods().getActivePeople();
        assertEquals(1, activePeople.size());
        
        Hashtable hashedPerson = (Hashtable) activePeople.firstElement();
        
        PublicKey hashedPublicKey = Cryptography.bytesToPublicKey((byte[]) hashedPerson.get("publicKey"));
        assertEquals(keypair.getPublic(), hashedPublicKey);
        
        // TODO: test "name" attribute which can optionally be in the hash
    }

    public void testMethod_getPersonKeyPair() throws Exception {
        // The method generates a new keypair on the server: 
        Hashtable hash = getDefaultMethods().getPersonKeyPair();
        
        // Extract data from the hash:
        byte[] publicKeyBytes = (byte[]) hash.get("publicKey");
        PublicKey publicKey = Cryptography.bytesToPublicKey(publicKeyBytes);
        
        byte[] privateKeyBytes = (byte[]) hash.get("privateKey");
        PrivateKey privateKey = Cryptography.bytesToPrivateKey(privateKeyBytes);

        // FIXME: there are more tests we could do here - are they from the same pair?
    }
}
