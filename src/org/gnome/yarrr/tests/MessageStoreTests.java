/*
 * Created on Jan 15, 2005
 */
package org.gnome.yarrr.tests;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.gnome.yarrr.Message;
import org.gnome.yarrr.Topic;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.hibernate.Persistent;

/**
 * @author seth
 */
public class MessageStoreTests extends HibernateUsingTest {
	public static void main(String[] args) {
		junit.textui.TestRunner.run(MessageStoreTests.class);
	}

	private void assertUndistorted(Message original, Message backedMessage) {
		assertEquals(original.getSentTime(), backedMessage.getSentTime());
		assertEquals(original.getAuthor(), backedMessage.getAuthor());
		assertEquals(original.getSubject(), backedMessage.getSubject());
		assertEquals(original.getDocument().getDocumentId(), backedMessage.getDocument().getDocumentId());
	}
	
	public void testBackedMessageNotDistorted() throws Exception {
		Message message;
		Long id;
		
		Topic topic = TestUtils.addTestTopic();
		message = TestUtils.getTestMessage(topic, 5);
		id = message.getId();
					
		HibernateUtil.newTestTransaction();
		
		// Read data in separate session
		assertEquals(1, Message.getAllMessages().size());
		Message backedMessage = Message.getMessage(id);
		assertUndistorted(message, backedMessage); 
	}
	
	public void testGetMostRecentSentTime() throws Exception {
		Topic topic = TestUtils.addTestTopic();
		Message message1 = TestUtils.getTestMessage(topic, 1);
		Message message2 = TestUtils.getTestMessage(topic, 9);
		Message message3 = TestUtils.getTestMessage(topic, 5);
		
		Long id1 = message1.getId();
		Long id2 = message2.getId();
		Long id3 = message3.getId();
		
		assertFalse (message2.getSentTime().equals(message1.getSentTime()));
		assertFalse (message2.getSentTime().equals(message3.getSentTime()));
		assertFalse (message3.getSentTime().equals(message2.getSentTime()));
		
		assertEquals(message3.getSentTime(), topic.getLastPostTime());
	}
	
	public void testAddGetOutOfOrderSequence() throws Exception {
		Message message1, message2, message3;
		Long id1, id2, id3;
		Topic topic = TestUtils.addTestTopic();
		Long topicId = topic.getId();
		
		message1 = TestUtils.getTestMessage(topic, 1);
		id1 = message1.getId();
			
		message2 = TestUtils.getTestMessage(topic, 2);
		id2 = message2.getId();
			
		message3 = TestUtils.getTestMessage(topic, 3);
		id3 = message3.getId();
			
		HibernateUtil.newTestTransaction();
		
		topic = Topic.getTopic(topicId);

		// Read data in separate session
		Message backed1 = Message.getMessage(id1);
		Message backed3 = Message.getMessage(id3);
		
		Message message4 = TestUtils.getTestMessage(topic, 4);
		Long id4 = message4.getId();
		
		Message backed2 = Message.getMessage(id2);
		
		assertUndistorted(message1, backed1);
		assertUndistorted(message2, backed2);
		assertUndistorted(message3, backed3);
	}
	
	public void testSimpleAddRemove() throws Exception {
		Message message39, message12;
		Long id39, id12;
		Topic topic = TestUtils.addTestTopic();
		
		message39 = TestUtils.getTestMessage(topic, 39);
		message12 = TestUtils.getTestMessage(topic, 12);
			
		id39 = message39.getId();
		id12 = message12.getId();
			
		HibernateUtil.newTestTransaction();
		
		// Read data in separate session
		Message newMessage39 = Message.getMessage(id39);
		newMessage39.delete();
			
		assertEquals(1, Message.getAllMessages().size());
		
		boolean noMatchFoundExceptionThrown = false;
		try {
			Message.getMessage(id39);
		} catch (Persistent.NoMatchFoundException e) {
			noMatchFoundExceptionThrown = true;
		}
		assertTrue("After a message is deleted, we shouldn't be able to get it", noMatchFoundExceptionThrown);
		
		Message newMessage12 = Message.getMessage(id12);
		assertUndistorted(message12, newMessage12);
	}
	
	public void testAddOneHundred() throws Exception {
		List addedMessageIDs;
		Topic topic = TestUtils.addTestTopic();
		addedMessageIDs = TestUtils.addNMessages(topic, 100, false);
			
		HibernateUtil.newTestTransaction();
		
		// Read data in separate session
		List retrievedMessages = topic.getAllMessages();
			
		assertEquals(addedMessageIDs.size(), retrievedMessages.size());
			
		HashSet addedMessageSet = new HashSet(addedMessageIDs);
		
		for (Iterator i = retrievedMessages.iterator(); i.hasNext();) {
			Message retrievedMessage = (Message)i.next();
			assertTrue("Every retrieved messageID had better have been added!", addedMessageSet.contains(retrievedMessage.getId()));
		}
	}
	
	public void testGetInvalidMessageID() throws Exception {
		assertEquals("Store should be empty", 0, Message.getAllMessages().size());
		boolean noMatchFoundExceptionThrown = false;
		try {
			Message.getMessage(new Long(1039));
		} catch (Persistent.NoMatchFoundException e) {
			noMatchFoundExceptionThrown = true;
		}
		assertTrue(noMatchFoundExceptionThrown);
	}
	
	public void testDeleteAllMessages() throws Exception {
		
		Topic topic = TestUtils.addTestTopic();
		TestUtils.addNMessages(topic, 34, false);
			
		HibernateUtil.newTestTransaction();
		
		// Read data in separate session
		assertEquals(34, Message.getAllMessages().size());
		Message.deleteAll();
		assertEquals(0, Message.getAllMessages().size());
	}
}
