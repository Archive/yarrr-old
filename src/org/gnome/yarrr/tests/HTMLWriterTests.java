/*
 * Created on 07-Feb-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.gnome.yarrr.tests;

import org.gnome.yarrr.email.HtmlWriter;

import junit.framework.TestCase;

/**
 * @author dmalcolm
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class HTMLWriterTests extends TestCase {
    HtmlWriter htmlWriter;
    
    public void testEscapeSimple() {
        assertEquals("foo", htmlWriter.escapeHtml("foo"));
    }

    public void testEscapeLtGt() {
        assertEquals(":-&gt; &lt;-:", htmlWriter.escapeHtml(":-> <-:"));
    }

    public void testEscapeQuotes() {
        assertEquals("William Wordsworth (1770-1850):\n&quot;I&apos;ve measured it from side to side, \n&apos;Tis three feet long and two feet wide.&quot;\n\n", 
                    htmlWriter.escapeHtml("William Wordsworth (1770-1850):\n\"I've measured it from side to side, \n'Tis three feet long and two feet wide.\"\n\n"));        
    }
    public void testEscapeAmpersand() {
        assertEquals("George Crabbe (1754-1832): And I was ask&apos;d and authorised to go / To seek the firm of Clutterbuck &amp; Co.",
                    htmlWriter.escapeHtml("George Crabbe (1754-1832): And I was ask'd and authorised to go / To seek the firm of Clutterbuck & Co.")); 
    }
    public void setUp() {
        this.htmlWriter = new HtmlWriter();
    }
}
