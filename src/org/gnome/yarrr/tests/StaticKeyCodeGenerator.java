/*
 * Created on Jan 19, 2005
 */
package org.gnome.yarrr.tests;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import org.gnome.yarrr.Cryptography;

/**
 * @author seth
 */
public class StaticKeyCodeGenerator {
	
	public static void main(String[] args) throws NoSuchProviderException {
		int numPairs = 50;
		
		String keyFormatCode = "private static final String ENCRYPTION_ALGORITHM_USED = \"" + Cryptography.getEncryptionAlgorithmUsed() + "\";";
		String numKeysCode = "private static final int NUMKEYS = " + numPairs + ";";
		String publicKeyArrayCode = "private static final String[] publicKeyArray = new String[] {";
		String privateKeyArrayCode = "private static final String[] privateKeyArray = new String[] {";
		
		try {
			boolean firstRun = true;
			for (int i=0; i < numPairs; i++) {
				KeyPair pair = Cryptography.generateKeyPair();
				
				byte[] pubKeyBytes = Cryptography.publicKeyToBytes(pair.getPublic());
				String pubKeyString = Base64.encodeBytes(pubKeyBytes, Base64.DONT_BREAK_LINES);

				byte[] privKeyBytes = Cryptography.privateKeyToBytes(pair.getPrivate());
				String privKeyString = Base64.encodeBytes(privKeyBytes, Base64.DONT_BREAK_LINES);

				String separator = ",";
				if (firstRun) {
					separator = "";
					firstRun = false;
				}
								
				publicKeyArrayCode += separator + "\"" + pubKeyString + "\"";				
				privateKeyArrayCode += separator + "\"" + privKeyString + "\"";
			}
			publicKeyArrayCode += "};";
			privateKeyArrayCode += "};";
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (Cryptography.EncryptionException e) {
			e.printStackTrace();
		}
		System.out.println(keyFormatCode);
		System.out.println(numKeysCode);
		System.out.println(publicKeyArrayCode);
		System.out.println(privateKeyArrayCode);
	}
}
