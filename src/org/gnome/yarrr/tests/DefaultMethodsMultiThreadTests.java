/*
 * Created on Feb 23, 2005
 */
package org.gnome.yarrr.tests;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;
// FIXME gcj doesnt yet implement this #20247
//import java.util.concurrent.LinkedBlockingQueue;

import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;
import org.gnome.yarrr.Cryptography;
import org.gnome.yarrr.hibernate.HibernateUtil;

/**
 * @author seth
 */
public class DefaultMethodsMultiThreadTests extends YarrrUsingTest {
    static { 
        YarrrUsingTest.getYarrr().startXmlRpcServer();
    }
    
	//public LinkedBlockingQueue exceptions;
	
	abstract class Operation extends Thread {
		Operation(String name) {
			super(name);
		}
		protected Object remoteCall(String methodName, Vector params) throws IOException, XmlRpcException  {
			XmlRpcClient xmlrpc = new XmlRpcClient ("http://localhost:" + yarrr.getRPCServerPort());        
			return xmlrpc.execute(methodName, params);
		}
		public void doIt() throws Exception {
			throw new Exception("Not implemented");
		}
		public void run() {
			try {
				doIt();
			} catch (Exception e) {
				System.out.println("Throwing exception:");
				e.printStackTrace();
				//exceptions.add(e);
			}
		}
	}
	
	class AddTopicOperation extends Operation {
		private int id;
		private String topicID;
		
		AddTopicOperation(int id) {
			super("CreateTopic " + id);
			this.id = id;
		}
		
		public void doIt() throws Exception {
			byte[] author = Cryptography.publicKeyToBytes(TestUtils.getPublicKey(this.id));
			Hashtable topic = new Hashtable();
			topic.put("author", author);
			topic.put("name", "Topical things");
			topic.put("allowMessages", new Boolean(true));
			Vector addTopicArgs = new Vector();
			addTopicArgs.add(topic);
			addTopicArgs.add("SomeTopic".getBytes());
			this.topicID = (String)remoteCall("addTopic", addTopicArgs);
		}
		
		public String getTopicID() {
			return topicID;
		}
	}

	class AddMessageOperation extends Operation {
		private int id;
		private String topicID;
		
		AddMessageOperation(String topicID, int id) {
			super("AddMessage " + id);
			this.id = id;
			this.topicID = topicID;
		}

		
		public void doIt() throws Exception {
			byte[] author = Cryptography.publicKeyToBytes(TestUtils.getPublicKey(this.id));
			Hashtable topic = new Hashtable();
			topic.put("author", author);
			topic.put("name", "Topical things");
			topic.put("allowMessages", new Boolean(true));
			Vector addTopicArgs = new Vector();
			addTopicArgs.add(topic);
			addTopicArgs.add("SomeTopic".getBytes());
			String topicID = (String)remoteCall("addTopic", addTopicArgs);
			
			Hashtable message = new Hashtable();
			message.put("author", author);
			message.put("authorName", "Person " + this.id);
			message.put("subject", "Subject " + this.id);
			message.put("mimetype", "text/plain");
			Vector addArgs = new Vector();
			addArgs.add(topicID);
			addArgs.add(message);
			addArgs.add(("Contents #" + id).getBytes());
			String id = (String)remoteCall("addMessage", addArgs);
			
			Vector getArgs = new Vector();
			getArgs.add(id);
			Hashtable gotMessage = (Hashtable)remoteCall("getMessage", getArgs);
			assertEquals(message.get("authorName"), gotMessage.get("authorName"));
			assertEquals(message.get("subject"), gotMessage.get("subject"));
		}
	}

	class GetChangesOperation extends Operation {
		private String topicID;
		private long waitInterval;
		private Date baseDate;
		
		GetChangesOperation(String topicID, long waitInterval) {
			super("GetChangesOperation " + waitInterval);
			this.waitInterval = waitInterval;
			this.topicID = topicID;
			this.baseDate = new Date();
		}
		
		public void doIt() throws Exception {
			sleep(this.waitInterval);
			
			Vector args = new Vector();
			args.add(topicID);
			args.add(baseDate);
			Hashtable results = (Hashtable)remoteCall("getChangesSince", args);
		}
	}
	
	private void runOperations(Operation[] operationsArray, String operationName, long expectedTime) throws Exception {
		List operations = Arrays.asList(operationsArray);
		Date startDate = new Date();
		for (Iterator i = operations.iterator(); i.hasNext();) {
			Operation operation = (Operation)i.next();
			operation.start();
		}
		for (Iterator i = operations.iterator(); i.hasNext();) {
			Thread thread = (Thread)i.next();
			thread.join();
		}
		Date stopDate = new Date();
		long timeItTook = stopDate.getTime() - startDate.getTime();
		
		/*System.out.println(operationName + ", took " + timeItTook + " ms (expected " + expectedTime + ") and threw " +  exceptions.size() + " exceptions");
		for (Iterator i = exceptions.iterator(); i.hasNext();) {
			Exception e = (Exception)i.next();
			throw e;
		}*/

		assertTrue("Exceeded time limit " + expectedTime + ", took " + timeItTook, timeItTook < expectedTime);
	}
	
	public void testConcurrentAddMessagesAndGetChanges() throws Exception {
		AddTopicOperation addTopicOp = new AddTopicOperation(4);
		runOperations(new Operation[]{addTopicOp}, "Create a Topic", 500);
		String topicID = addTopicOp.getTopicID();

		Operation[] ops = new Operation[50];
		for (int i=0; i < 50; i++) {
			if (i % 2 == 0) {
				ops[i] = new AddMessageOperation(topicID, i);
			} else {
				ops[i] = new GetChangesOperation(topicID, i * 10);
			}
		}
		runOperations(ops, "Add 25 Messages / Get 25 Changes", 6000);
	}
	
	public void testConcurrentAddMessageCalls() throws Exception {
		AddTopicOperation addTopicOp = new AddTopicOperation(4);
		runOperations(new Operation[]{addTopicOp}, "Create a Topic", 1000);
		String topicID = addTopicOp.getTopicID();
		
		Operation[] addMessageOps = new Operation[10];
		for (int i=0; i < 10; i++) {
			addMessageOps[i] = new AddMessageOperation(topicID, i);
		}
		runOperations(addMessageOps, "Create 10 Messages", 10 * 200);
	}
	
	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		 //exceptions = new LinkedBlockingQueue();
         HibernateUtil.beginTransaction();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		HibernateUtil.commitTransaction();
		HibernateUtil.closeSession();
		super.tearDown();
	}

}
