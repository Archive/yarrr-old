/*
 * Created on Mar 13, 2005
 */
package org.gnome.yarrr.tests;

import java.util.List;

import org.gnome.yarrr.Statement;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.person.Person;

/**
 * @author seth
 */
public class StatementTests extends PersonUsingTest {

	private Statement makeStatement(int statementNumber, Person person) {
		Statement statement = new Statement("Four score and " + statementNumber + " years ago", person);
		HibernateUtil.getSession().flush();
		return statement;
	}
	
	public void testStatementCreation () throws Exception {
		HibernateUtil.newTestTransaction();
		Person person1 = this.getPerson1();
		
		Statement statement = makeStatement(7, person1);
	
		HibernateUtil.newTestTransaction();
		
		assertTrue(statement.getPeopleMakingStatement().contains(person1));
		assertFalse(statement.getPeopleMakingStatement().contains(this.getPerson2()));
		assertFalse(statement.getPeopleMakingStatement().contains(this.getPerson3()));	
		
	    List statements = HibernateUtil.getSession().createCriteria(Statement.class).list();
	    assertEquals(1, statements.size());
	}
	
	public void testAddPersonToStatement () throws Exception {
		HibernateUtil.newTestTransaction();
		Person person1 = this.getPerson1();
		
		Statement statement = makeStatement(7, person1);
		
		HibernateUtil.newTestTransaction();
		
		Person person2 = this.getPerson2();
		assertFalse(statement.getPeopleMakingStatement().contains(person2));
		
		HibernateUtil.newTestTransaction();
		
		statement.addPersonToStatement(person2);
		
		assertTrue(statement.getPeopleMakingStatement().contains(person2));
	}
	
	public void testRemovePersonFromStatement () throws Exception {
		HibernateUtil.newTestTransaction();
		Person person1 = this.getPerson1();
		Statement statement = makeStatement(7, person1);
		Person person2 = this.getPerson2();
		
		statement.addPersonToStatement(person2);
		statement.dropPersonFromStatement(person2);
		
		HibernateUtil.newTestTransaction();
		
		assertFalse(statement.getPeopleMakingStatement().contains(person2));
		assertTrue(statement.getPeopleMakingStatement().contains(person1));
	}

}
