/*
 * Created on 10-Feb-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.gnome.yarrr.tests;

import org.gnome.yarrr.xmlrpc.DefaultMethods;

/**
 * @author dmalcolm
 * 
 * Test the DefaultMethods interface by calling directly
 */
public class DefaultMethodsTestDirect extends DefaultMethodsTests {
    public DefaultMethods getDefaultMethods() throws Exception {
        return new DefaultMethods(getYarrr());
    }
    
    public void testGetMessagesWrongDateRange() throws Exception {
    	super.testGetMessagesWrongDateRange();
    }
    
    public void testgetMessagesDateRange() throws Exception {
    	super.doTestgetMessagesDateRange();
    }
    
}
