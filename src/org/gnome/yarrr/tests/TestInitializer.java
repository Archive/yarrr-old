/*
 * Created on 04-Feb-2005
 */
package org.gnome.yarrr.tests;

import java.io.UnsupportedEncodingException;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import org.gnome.yarrr.Cryptography;
import org.gnome.yarrr.IInitializer;
import org.gnome.yarrr.Topic;
import org.gnome.yarrr.email.MailingList;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.person.Person;
import org.hibernate.HibernateException;

/**
 * @author dmalcolm
 *
 * Initializes the database with some hard-coded test values
 */
public class TestInitializer implements IInitializer {

    public Topic createTopic(String name, Person creator, String description) throws HibernateException, UnsupportedEncodingException {
        Topic topic = new Topic(name, creator);
        HibernateUtil.getSession().flush();

        // Add an initial message, using the description document as content:
        //Message msg = new Message(creator.getPublicKey(), name, "text/html", descDoc);        
        //topic.addMessage(msg);
        
        return topic;        
    }
    
    private void createYarrrTopics (Person creator) throws HibernateException, UnsupportedEncodingException {
    	Topic topic1 = createTopic("Yarrr UI", creator, "Ideas, proposals, criticisms about the user interface of the yarrr clients.");
        Topic topic2 = createTopic("Yarrr Server", creator, "Yarrr server architecture and implementation.");
        Topic topic3 = createTopic("Yarrr Build", creator, "Build problems, required tools, dependencies.");
        Topic topic4 = createTopic("Yarrr Hackers", creator, "Yarrr development and hackers activity logs");
    }
    
    /* (non-Javadoc)
     * @see org.gnome.yarrr.Initializer#doCreation(org.gnome.yarrr.Yarrr)
     */
    public void doCreation() throws CreationException {
        try {
            HibernateUtil.beginTransaction();
            
            
            KeyPair pair = Cryptography.generateKeyPair();
            Person theCapn = Person.getPerson(pair.getPublic());            
            theCapn.setAttribute (Person.NAME, "Captain Yarrrbeard");
            
            //  Test topic and mailing list feed creation:
            {                    
                createYarrrTopics(theCapn);
            	
                Topic topic1 = createTopic("desktop-devel-list", theCapn, "An infinitely-sized brain, spreading through the multiverse");
                Topic topic2 = createTopic("fedora-list", theCapn, "For users of Fedora Core releases. If you want help with a problem installing or using Fedora Core, this is the list for you.</a>");
                
                MailingList list1 = new MailingList(topic1, "desktop-devel-list@gnome.org");
                MailingList list2 = new MailingList(topic2, "fedora-list@redhat.com");
            }
            
            HibernateUtil.commitTransaction();
        } catch (NoSuchAlgorithmException e) {
            throw new CreationException(e);
        } catch (NoSuchProviderException e) {
            throw new CreationException(e);
        } catch (HibernateException e) {
            throw new CreationException(e);
        } catch (UnsupportedEncodingException e) {
            throw new CreationException(e);
        } finally {
            try {
                HibernateUtil.closeSession();
            } catch (HibernateException e1) {
                throw new CreationException(e1);
            }
        }
    }

}
