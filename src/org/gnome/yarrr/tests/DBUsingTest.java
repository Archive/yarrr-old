/*
 * Created on Jan 18, 2005
 */
package org.gnome.yarrr.tests;

import junit.framework.TestCase;

import org.gnome.yarrr.database.IDatabaseManager;
import org.gnome.yarrr.database.PostgresqlManager;

/**
 * @author seth
 */
public abstract class DBUsingTest extends TestCase {

	private static PostgresqlManager staticDB;
	private static Exception dbException;
	
	protected IDatabaseManager getDB() throws Exception {
		if (dbException != null) throw dbException;
		return staticDB;
	}
	
	static {
		staticDB = null;
		dbException = null;
		try {
			staticDB = TestUtils.getNewPostgresqlManager();
		} catch (Exception e){
            // We can't throw exceptions from a static initialiser; make sure the developer sees them: 
            e.printStackTrace();
			dbException = e;        
		}
	}

	protected void setUp() throws Exception {
		super.setUp();
		// Throw exception if any
		IDatabaseManager db = getDB();
	}
	
	protected void finalize() throws Throwable {
        if (dbException != null) throw dbException;
        
		try {
			staticDB.stop();
		} finally {
			super.finalize();
		}
	}
	
}
