/*
 * Created on Feb 2, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.gnome.yarrr.tests;

import java.util.Iterator;

import org.hibernate.HibernateException;

import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.person.AttributeListGroup;
import org.gnome.yarrr.person.EnumeratedGroup;
import org.gnome.yarrr.person.Group;
import org.gnome.yarrr.person.Person;

/**
 * @author alex
 */
public class GroupTests extends PersonUsingTest {
	
	private Long [] createPersons() throws Exception {
		Person person1 = getPerson1();
		Person person2 = getPerson2();
		Person person3 = getPerson3();
		
		HibernateUtil.newTestTransaction();
			
		Long [] ids = new Long[3];
		ids[0] = person1.getId();
		ids[1] = person2.getId();
		ids[2] = person3.getId();
			
		return ids;
	}

	private void verifyGroupMembership(Long [] ids, Long group1id, Long group2id) throws HibernateException {
		Group g1 = Group.getGroup(group1id);
		Group g2 = Group.getGroup(group2id);
		
		Person p1 = Person.getPerson(ids[0]);
		Person p2 = Person.getPerson(ids[1]);
		Person p3 = Person.getPerson(ids[2]);
		
		assertTrue(g1.isMember(p1));
		assertTrue(!g1.isMember(p2));
		assertTrue(!g1.isMember(p3));
		Iterator i = g1.iterator();
		assertTrue(i.hasNext());
		assertEquals(p1, i.next());
		assertTrue(!i.hasNext());
		
		assertTrue(g2.isMember(p1));
		assertTrue(g2.isMember(p2));
		assertTrue(!g2.isMember(p3));
		i = g2.iterator();
		assertTrue(i.hasNext());
		Person item1 = (Person)i.next();
		assertTrue(item1.equals(p1) || item1.equals(p2));
		assertTrue(i.hasNext());
		Person item2 = (Person)i.next();
		if (item1.equals(p1))
			assertEquals(p2, item2);
		else
			assertEquals(p1, item2);
		assertTrue(!i.hasNext());
	}
	
	public void testEnumeratedGroup() throws Exception {
		Long group1id, group2id;

		Long [] ids = createPersons();

		// Create two groups
		EnumeratedGroup g1 = new EnumeratedGroup("Test group 1");
		g1.addPerson(Person.getPerson(ids[0]));
		group1id = g1.getId();
			
		EnumeratedGroup g2 = new EnumeratedGroup("Test group 2");
		g2.addPerson(Person.getPerson(ids[0]));
		g2.addPerson(Person.getPerson(ids[1]));
		group2id = g2.getId();

		HibernateUtil.newTestTransaction();

		verifyGroupMembership(ids, group1id, group2id);
	}

	public void testAttributeListGroup() throws Exception {
		Long group1id, group2id;

		Long [] ids = createPersons();

		// Create two groups
		AttributeListGroup g1 = new AttributeListGroup("Test group 1", Person.EMAIL);
		g1.addValue(Person.getPerson(ids[0]).getAttribute(Person.EMAIL));
		group1id = g1.getId();
		
		AttributeListGroup g2 = new AttributeListGroup("Test group 2", Person.EMAIL);
		g2.addValue(Person.getPerson(ids[0]).getAttribute(Person.EMAIL));
		g2.addValue(Person.getPerson(ids[1]).getAttribute(Person.EMAIL));
		group2id = g2.getId();
		
		HibernateUtil.newTestTransaction();

		verifyGroupMembership(ids, group1id, group2id);
	}
	
}
