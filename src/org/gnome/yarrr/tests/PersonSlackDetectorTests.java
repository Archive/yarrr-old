/*
 * Created on Jan 21, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.gnome.yarrr.tests;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

import org.gnome.yarrr.person.Person;
import org.gnome.yarrr.person.PersonSlackDetector;

/**
 * @author caillon
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class PersonSlackDetectorTests extends HibernateUsingTest {
	private PersonSlackDetector detector1;
	private PersonSlackDetector detector2;

	protected void setUp() throws Exception
	{
		super.setUp();
		detector1 = new PersonSlackDetector();
		detector2 = new PersonSlackDetector();
	}
	
	private void heartbeat() throws Exception {
		for (int i = 0; i < 10; i++)
		{
				PublicKey k = TestUtils.getPublicKey(i);
				detector1.heartbeat(k);
				detector2.heartbeat(k);
		}
	}
		
	public void testGetNonSlackers() throws Exception
	{
		List ids1; 
		List ids2;
		
		ids1 = new ArrayList(detector1.getNonSlackers());
		ids2 = new ArrayList(detector2.getNonSlackers());
		
		assertTrue ("Slackers list is non empty (detector1)", ids1.isEmpty());
		assertTrue ("Slackers list is non empty (detector2)", ids2.isEmpty());
		
		heartbeat();
		
		ids1 = new ArrayList(detector1.getNonSlackers());
		ids2 = new ArrayList(detector2.getNonSlackers());
		
		assertEquals (ids1,ids2);

		int size1 = ids1.size();
		for (int i = 0; i < size1; i++) {
			Person person = Person.getPerson((Long)ids1.get(i));
			assertFalse(detector2.isSlacking(person.getPublicKey()));
		}

		int size2 = ids2.size();
		assert size1 == size2;
		for (int i = 0; i < size2; i++) {
			Person person = Person.getPerson((Long)ids2.get(i));
			assertFalse(detector1.isSlacking(person.getPublicKey()));
		}
	}

	public void testIsSlacking() throws Exception {
		PublicKey k1, k2, k3, k4;
		
		heartbeat ();
		
		k1 = TestUtils.getPublicKey(1);
		k2 = TestUtils.getPublicKey(3);
		k3 = TestUtils.getPublicKey(11);
		k4 = TestUtils.getPublicKey(13);
		
		assertFalse(detector1.isSlacking(k1));
		assertFalse(detector1.isSlacking(k2));
		assertTrue(detector1.isSlacking(k3));
		assertTrue(detector1.isSlacking(k4));
	}
}
