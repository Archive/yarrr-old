/*
 * Created on 20-Jan-05
 *
 */
package org.gnome.yarrr.tests;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import org.gnome.yarrr.Cryptography;
import org.gnome.yarrr.person.Person;

/**
 * @author hp
 *
 */
public abstract class PersonUsingTest extends HibernateUsingTest {

	protected Person getPerson1() throws Exception {
		KeyPair pair = makeKeyPair();
		Person person = Person.getPerson(pair.getPublic());
		person.setAttribute (Person.NAME, "Fickle Nickell");
		person.setAttribute (Person.EMAIL, "fickl@nickl.org");
		return person;
	}

	protected Person getPerson2() throws Exception {
		KeyPair pair = makeKeyPair();
		Person person = Person.getPerson(pair.getPublic());
		person.setAttribute (Person.NAME, "Havoc the Kewl");
		person.setAttribute (Person.EMAIL, "kewl@ometer.com");
		return person;
	}
	
	protected Person getPerson3() throws Exception {
		KeyPair pair = makeKeyPair();
		Person person = Person.getPerson(pair.getPublic());
		person.setAttribute (Person.NAME, "Captain Blackbeard");
		person.setAttribute (Person.EMAIL, "keelhauler@pirates.com");
		return person;
	}
	
	protected KeyPair makeKeyPair() throws NoSuchAlgorithmException, NoSuchProviderException {
		KeyPair pair = Cryptography.generateKeyPair();
		return pair;
	}

}
