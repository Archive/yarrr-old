/*
 * Created on 10-Feb-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.gnome.yarrr.tests;

import junit.framework.TestCase;

import org.gnome.yarrr.Document;
import org.gnome.yarrr.IInitializer;
import org.gnome.yarrr.Message;
import org.gnome.yarrr.Topic;
import org.gnome.yarrr.Yarrr;
import org.gnome.yarrr.email.Email;
import org.gnome.yarrr.email.MailingList;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.person.Group;
import org.gnome.yarrr.person.Person;

/**
 * @author dmalcolm
 *
 * A test case that owns a Yarrr.
 * The yarrr is created once in the static initialiser, and every test
 * repopulates the database then empties it in the setUp/tearDown hooks.
 */
public class YarrrUsingTest extends TestCase {
    static Yarrr yarrr;
    
    public void testYarrr() {
        // Need to havce at least one test in a TestCase subclass or JUnit complains 
        assertNotNull(yarrr);
    }

    static {
        try {
            yarrr = new Yarrr(TestUtils.getTemporaryDir().toString());

            // the database will have been freshly populated using a TestInitialiser; empty it for now:
            try {
                HibernateUtil.beginTransaction();
                
                MailingList.deleteAll();
                Topic.deleteAll();
                Person.deleteAll();
                        
                HibernateUtil.commitTransaction();
            } finally {
                HibernateUtil.closeSession();
            }
            
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    protected void setUp() throws Exception {
        super.setUp();
        assertNotNull(yarrr);

        IInitializer init = new TestInitializer();            
        init.doCreation();
    }
    
    protected void tearDown() throws Exception {
        super.tearDown();
        
        // Empty the database:
        // FIXME: does this get everything?
        try {
            HibernateUtil.beginTransaction();
            
			MailingList.deleteAll();
			Email.deleteAll();
			Message.deleteAll();
			Topic.deleteAll();
			Document.deleteAll();
			Group.deleteAll();
			Person.deleteAll();
                    
            HibernateUtil.commitTransaction();
        } finally {
            HibernateUtil.closeSession();
        }
        super.tearDown();
    }

    public static Yarrr getYarrr() {
        return yarrr;
    }
}
