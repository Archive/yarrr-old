/*
 * Created on Jan 19, 2005
 */
package org.gnome.yarrr.tests;

import java.security.Key;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.SecretKey;

import junit.framework.TestCase;

import org.gnome.yarrr.Cryptography;
import org.gnome.yarrr.Cryptography.EncryptionException;

/**
 * @author seth
 */
public class CryptographyTests extends TestCase {

	static public void testGenerateKeyPair() throws Exception {
		KeyPair keyPair = Cryptography.generateKeyPair();
		assertNotNull(keyPair.getPublic());
		assertNotNull(keyPair.getPrivate());
	}
	
	static public void testPublicToBytesAndBack() throws Exception {
		KeyPair pair = Cryptography.generateKeyPair();
		PublicKey publicKey = pair.getPublic();
		byte[] publicKeyBytes = Cryptography.publicKeyToBytes(publicKey);
		PublicKey publicKeyFromBytes = Cryptography.bytesToPublicKey(publicKeyBytes);
		assertEquals(publicKey, publicKeyFromBytes);
	}
	
	static public void testPrivateToBytesAndBack() throws Exception {
		KeyPair pair = Cryptography.generateKeyPair();
		PrivateKey privateKey = pair.getPrivate();
		byte[] privateKeyBytes = Cryptography.privateKeyToBytes(privateKey);
		PrivateKey privateKeyFromBytes = Cryptography.bytesToPrivateKey(privateKeyBytes);
		assertEquals(privateKey, privateKeyFromBytes);
	}
	
	static public void testPublicToBase64BytesAndBack() throws Exception {
		KeyPair pair = Cryptography.generateKeyPair();
		PublicKey publicKey = pair.getPublic();
		pair = null;
		byte[] publicKeyBytes = Cryptography.publicKeyToBytes(publicKey);
		String publicKeyString = Base64.encodeBytes(publicKeyBytes, Base64.DONT_BREAK_LINES);
		byte[] publicKeyBytesDecoded = Base64.decode(publicKeyString);
		PublicKey publicKeyFromBytes = Cryptography.bytesToPublicKey(publicKeyBytesDecoded);
		assertEquals(publicKey, publicKeyFromBytes);
	}
	
	static private void encryptDecrypt(byte[] secretMessage, Key encryptionKey, Key decryptionKey) throws EncryptionException {
		Cryptography.EncryptedData data = Cryptography.encrypt(encryptionKey, secretMessage);
		byte[] decryptedMessage = Cryptography.decrypt(decryptionKey, data);

		// Make sure secretMessage and decryptedMessage are the same
		assertEquals(secretMessage.length, decryptedMessage.length);
		for (int i=0; i < secretMessage.length; i++) {
			assertEquals(secretMessage[i], decryptedMessage[i]);
		}
	}
	
	static public void testEncryptPublicDecryptPrivateSymmetricKey() throws Exception {
		SecretKey symmetricKey = Cryptography.generateSymmetricKey();
		byte[] encryptedSymmetricKey = Cryptography.encryptSymmetricKey(TestUtils.getPublicKey(1), symmetricKey);
		SecretKey decryptedSymmetricKey = Cryptography.decryptSymmetricKey(TestUtils.getPrivateKey(1), encryptedSymmetricKey);
		assertEquals(symmetricKey, decryptedSymmetricKey);
	}

	static public void testEncryptPrivateDecryptPublicSymmetricKey() throws Exception {
		SecretKey symmetricKey = Cryptography.generateSymmetricKey();
		byte[] encryptedSymmetricKey = Cryptography.encryptSymmetricKey(TestUtils.getPrivateKey(1), symmetricKey);
		SecretKey decryptedSymmetricKey = Cryptography.decryptSymmetricKey(TestUtils.getPublicKey(1), encryptedSymmetricKey);
		assertEquals(symmetricKey, decryptedSymmetricKey);
	}
	
	
	static public void testEncryptPublicDecryptPrivate() throws Exception {
		String secretMessage = "But Minnie had a heart as big as a whale";
		byte[] secretMessageBytes = secretMessage.getBytes("UTF-8");
		
		for (int i=5; i < 10; i++) {
			encryptDecrypt(secretMessageBytes, TestUtils.getPublicKey(i), TestUtils.getPrivateKey(i));
		}
	}
	
	static public void testEncryptPrivateDecryptPublic() throws Exception {
		String secretMessage = "Folks here's a story about Minnie the moocher, she was a red-hot hoochie coocher";
		byte[] secretMessageBytes = secretMessage.getBytes("UTF-8");
		
		for (int i=5; i < 10; i++) {
			encryptDecrypt(secretMessageBytes, TestUtils.getPrivateKey(i), TestUtils.getPublicKey(i));
		}
	}
	
	static public void testUnmatchedKeysEncryptDecryptFails() throws Exception {
		String secretMessage = "She had a dream about the King of Sweden, he gave her things she was need'n";
		byte[] secretMessageBytes = secretMessage.getBytes("UTF-8");
		
		boolean encryptionExceptionHit = false;
		try {
			encryptDecrypt(secretMessageBytes, TestUtils.getPrivateKey(5), TestUtils.getPublicKey(8));
		} catch (Cryptography.EncryptionException e) {
			encryptionExceptionHit = true;
		}
		
		assertTrue(encryptionExceptionHit);
	}
}
