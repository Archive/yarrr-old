/*
 * Created on 10-Feb-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.gnome.yarrr.tests;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Date;
import java.sql.Timestamp;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;
import org.gnome.yarrr.Yarrr;
import org.gnome.yarrr.xmlrpc.DefaultMethods;

/**
 * @author dmalcolm
 *
 * Test the DefaultMethods interface via XML-RPC on the server's exposed implementation 
 */
public class DefaultMethodsTestsRPC extends DefaultMethodsTests {
    
    /**
     * @author dmalcolm
     *
     * A DefaultMethods implementation that delegates everything via XML-RPC, to ensure that this marshalls correctly
     */
    public class DefaultMethodsWithRPC extends DefaultMethods {
        public DefaultMethodsWithRPC(Yarrr yarrr)
                throws NoSuchAlgorithmException {
            super(yarrr);           
        }

        private Object remoteCall(String methodName, Vector params) throws IOException, XmlRpcException  {
            XmlRpcClient xmlrpc = new XmlRpcClient ("http://localhost:" + yarrr.getRPCServerPort());        
            return xmlrpc.execute(methodName, params);
        }

        // Marshallers:
        private Object remoteCallVoid(String methodName) throws IOException, XmlRpcException  {
            return remoteCall(methodName, new Vector());
        }
        
        private Object remoteCallString(String methodName, String arg0) throws IOException, XmlRpcException  {
            Vector params = new Vector();
            params.add(arg0);
            return remoteCall(methodName, params);
        }

        private Object remoteCallByteArray(String methodName, byte [] arg0) throws IOException, XmlRpcException  {
            Vector params = new Vector();
            params.add(arg0);
            return remoteCall(methodName, params);
        }

        private Object remoteCallStringString(String methodName, String arg0, String arg1) throws IOException, XmlRpcException  {
            Vector params = new Vector();
            params.add(arg0);
            params.add(arg1);
            return remoteCall(methodName, params);
        }

        private Object remoteCallStringStringInt(String methodName, String arg0, String arg1, int arg2) throws IOException, XmlRpcException  {
            Vector params = new Vector();
            params.add(arg0);
            params.add(arg1);
            params.add(new Integer(arg2));
            return remoteCall(methodName, params);
        }

        private Object remoteCallStringHashtableByteArray(String methodName, String arg0, Hashtable arg1, byte[] arg2) throws IOException, XmlRpcException  {
            Vector params = new Vector();
            params.add(arg0);
            params.add(arg1);
            params.add(arg2);
            return remoteCall(methodName, params);
        }
        private Object remoteCallVector(String methodName, Vector arg0) throws IOException, XmlRpcException {
            Vector params = new Vector();
            params.add(arg0);
            return remoteCall(methodName, params);
        }
        private Object remoteCallStringDate(String methodName, String arg0, Date arg1) throws IOException, XmlRpcException  {
            Vector params = new Vector();
            params.add(arg0);
            params.add(arg1);
            return remoteCall(methodName, params);
        }
        private Object remoteCallStringIntInt(String methodName, String arg0, int arg1, int arg2) throws IOException, XmlRpcException {
            Vector params = new Vector();
            params.add(arg0);
            params.add(new Integer(arg1));
            params.add(new Integer(arg2));
            return remoteCall(methodName, params);
        }
        private Object remoteCallByteArrayStringString(String methodName, byte[] arg0, String arg1, String arg2) throws IOException, XmlRpcException {
            Vector params = new Vector();
            params.add(arg0);
            params.add(new Integer(arg1));
            params.add(new Integer(arg2));
            return remoteCall(methodName, params);
        }
        private Object remoteCallByteArrayString(String methodName, byte[] arg0, String arg1) throws IOException, XmlRpcException {
            Vector params = new Vector();
            params.add(arg0);
            params.add(new Integer(arg1));
            return remoteCall(methodName, params);
        }

        // Implementation of the DefaultMethods methods via delegation to XML-RPC:
        public Hashtable getTopic(String topicIDString) throws Exception {
            return (Hashtable) remoteCallString("getTopic", topicIDString);
        }
        public Hashtable getMessage(String messageIDString) throws Exception {
            return (Hashtable) remoteCallString("getMessage", messageIDString);
        }
        public boolean heartbeat(byte [] personKey) throws Exception {
            Boolean bool = (Boolean)(remoteCallByteArray("heartbeat", personKey));
            return bool.booleanValue();
        }
        public Vector getActivePeople() throws Exception {
            return (Vector) remoteCallVoid("getActivePeople");
        }
        public byte [] getResource (String documentID, String resource, int version) throws Exception {
            return (byte[]) remoteCallStringStringInt("getResource", documentID, resource, version);
        }
        public String getResourceAsUTF8 (String documentID, String resource) throws Exception {    
            return (String) remoteCallStringString("getResourceAsUTF8", documentID, resource);
        }
        public String addTopic(String topicId, Hashtable topicHash, byte [] description) throws Exception {    
            return (String) remoteCallStringHashtableByteArray("addTopic", topicId, topicHash, description);
        }
        public String addMessage(String topicId, Hashtable messageHash, byte [] contents) throws Exception {
            return (String) remoteCallStringHashtableByteArray("addMessage", topicId, messageHash, contents);
        }
        public boolean removeMessages(Vector messageIDs) throws Exception {
            Boolean bool = (Boolean)remoteCallVector("removeMessages", messageIDs);
            return bool.booleanValue();
        }
        public String injectEmail(byte [] contents) throws Exception {
            return (String) remoteCallByteArray("injectEmail", contents);
        }
        public Vector getTopics(String topicId) throws Exception {
            return (Vector) remoteCallString("getTopics", topicId);
        }
        public Vector getRootTopics() throws Exception {    
            return (Vector) remoteCallVoid("getRootTopics");
        }
        public Vector getMessages(String topicId) throws Exception {
            return (Vector) remoteCallString("getMessages", topicId);
        }
        public Hashtable getChangesSince(String topicId, Date lastUpdateTime) throws Exception {
            return (Hashtable) remoteCallStringDate("getChangesSince", topicId, lastUpdateTime);
        }
        public Vector getTopicsSince(String topicId, Date time) throws Exception {
            return (Vector) remoteCallStringDate("getTopicsSince", topicId, time);
        }
        public Vector getMessagesSince(String topicId, Date time) throws Exception {    
            return (Vector) remoteCallStringDate("getMessagesSince", topicId, time);
        }
        public int getLastTopicChildIndex(String topicId) throws Exception {
            Integer i = (Integer)remoteCallString("getLastTopicChildIndex", topicId);
            return i.intValue();
        }
        public Vector getTopicChildren(String topicId, int firstIndex, int lastIndex) throws Exception {
            return (Vector) remoteCallStringIntInt("getTopicChildren", topicId, firstIndex, lastIndex);
        }
        public Timestamp getLatestUpdateTime(String topicId) throws Exception {
            return (Timestamp) remoteCallString("getLatestUpdateTime", topicId);
        }
        public Hashtable getPersonKeyPair() throws Exception {
            return (Hashtable) remoteCallVoid("getPersonKeyPair");
        }
        public boolean setPersonAttribute(byte[] publicKeyBytes, String attributeName, String value) throws Exception {
            Boolean bool = (Boolean)remoteCallByteArrayStringString("setPersonAttribute", publicKeyBytes, attributeName, value);
            return bool.booleanValue();
        }
        public String getPersonAttribute(byte[] publicKeyBytes, String attributeName) throws IOException, XmlRpcException {
            return (String)remoteCallByteArrayString("getPersonAttribute", publicKeyBytes, attributeName);
        }
        public Hashtable getPersonAttributes(byte[] publicKeyBytes) throws IOException, XmlRpcException {
            return (Hashtable) remoteCallByteArray("getPersonAttributes", publicKeyBytes);
        }        
    }
    
    public DefaultMethods getDefaultMethods() throws Exception {
        return new DefaultMethodsWithRPC(getYarrr());
    }
    
    static { 
        YarrrUsingTest.getYarrr().startXmlRpcServer();
    }
}
