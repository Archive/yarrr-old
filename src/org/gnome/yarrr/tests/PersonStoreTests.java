/*
 * Created on Jan 19, 2005
 */
package org.gnome.yarrr.tests;


import java.security.KeyPair;
import java.util.List;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.person.Person;


/**
 * @author seth
 */
public class PersonStoreTests extends PersonUsingTest {

	public void testActivePeople() throws Exception
	{
		Person person1, person2;
		
		person1 = getPerson1();
		person2 = getPerson2();
		
		HibernateUtil.newTestTransaction();

		person1.heartbeat();
		
		List people = Person.getActivePeople();
		Person person = (Person)people.get(0);
			
		assertEquals (1, people.size());
		assertEquals (person, person1);
	}
	
	public void testLookupPerson() throws Exception {
		Person person1, person2;
		
		KeyPair pair = makeKeyPair();
		
		person1 = Person.getPerson(pair.getPublic());
			
		HibernateUtil.newTestTransaction();

		person2 = Person.getPerson(pair.getPublic());
		
		assertEquals (person1, person2);
	}
	
	public void testCreatePerson() throws Exception {
		Person person1, person2;
		Person person1l, person2l;
		
		person1 = getPerson1();
		person2 = getPerson2();

		HibernateUtil.newTestTransaction();

		person1l = Person.getPerson(person1.getPublicKey());
		person2l = Person.getPerson(person2.getPublicKey());
		
		assertEquals (person1, person1l);
		assertEquals (person2, person2l);
	}

	public void testClearStore() throws Exception {
		Person person1, person2;
		Long id1, id2;
		
		person1 = getPerson1();
		person2 = getPerson2();
		
		HibernateUtil.newTestTransaction();
		
		id1 = person1.getId();
		id2 = person2.getId();

		Person.deleteAll();

		HibernateUtil.newTestTransaction();
		
		person1 = Person.getPerson (id1);
		person2 = Person.getPerson (id2);
		
		assertTrue ("The store is not empty", person1 == null);
		assertTrue ("The store is not empty", person2 == null);
	}
	
	public static void main(String[] args) {
		junit.textui.TestRunner.run(PersonStoreTests.class);
	}

}
