/*
 * Created on Feb 5, 2005
 *
 */
package org.gnome.yarrr.tests;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

import junit.framework.TestCase;

/**
 * @author dmalcolm
 */
public class EmailTests extends TestCase {
    static byte [] loadAllOfStreamJavaDamnYou(InputStream input) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte [] buf = new byte[8192];
        int len;
        
        while ((len = input.read(buf)) != -1) {
            out.write(buf);
        }
        return out.toByteArray();
    }
    
    
    static byte [] getTestEmail(String filename) throws IOException {
        InputStream inputStream = EmailTests.class.getResourceAsStream("test-emails/"+filename);
        return loadAllOfStreamJavaDamnYou(inputStream);
    }
    
    static public MimeMessage loadTestEmail(String filename) throws MessagingException, IOException {
        Properties prop = System.getProperties();
        byte [] testEmail = getTestEmail(filename);
        InputStream stream = new ByteArrayInputStream(testEmail);
        Session session = Session.getInstance(System.getProperties());
        return new MimeMessage(session, stream);
    }
    
    public void testGetSession() {
        Session session = Session.getInstance(System.getProperties());        
    }
    
    public void testMultilineSubject() throws Exception {
        MimeMessage message = loadTestEmail("multiline-subject.email");        
        // FIXME: should it strip out the \r\n\t from the email Subject?  is this a bug in the javamail implementation?
        assertEquals("British English (was Re: GNOME Lovers Needed: l10n work for\r\n\tlocations database)", message.getSubject());        
    }
}
