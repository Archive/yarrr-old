/*
 * Created on Jan 14, 2005
 */
package org.gnome.yarrr.tests;

import java.io.IOException;
import java.util.Vector;

import junit.framework.TestCase;

import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;
import org.gnome.yarrr.StandaloneXmlRpcServer;

/**
 * @author seth
 */
public class RPCHandlerTests extends TestCase {
	private StandaloneXmlRpcServer rpcHandler;
	private final String prefixString = "Hello World ";
	
	public class ShareableObject {
		public String prependPrefixInt(int inValue) {
			return prefixString + inValue;
		}
		public String prependPrefixString(String inValue) {
			return prefixString + inValue;
		}
	}
	
	private Object remoteCall(String methodName, Object parameter) throws XmlRpcException, IOException {
		XmlRpcClient xmlrpc = new XmlRpcClient ("http://localhost:" + this.rpcHandler.getServerPort());
		Vector params = new Vector ();
		params.addElement (parameter);
		return xmlrpc.execute(methodName, params);
	}
	
	public void testShareObject() throws Exception {
		assertEquals(prefixString+149, remoteCall("main.prependPrefixInt", new Integer(149)));
		assertEquals(prefixString+"hi", remoteCall("main.prependPrefixString", "hi"));
	}
	
	public void testStopsPropertly() throws Exception {
		remoteCall("main.prependPrefixString", "foo");
		this.rpcHandler.stop();
		// Wait a few seconds for the server to stop
		Thread.sleep(5000);
		boolean ioExceptionHit = false;
		try {
			remoteCall("main.prependPrefixString", "foo");
		} catch (IOException e) {
			ioExceptionHit = true;
		}
		assertTrue("After stopping the server, calls to it should fail", ioExceptionHit);
	}
	
	private void testInvalidCall(String method, Object argument) throws Exception {
		boolean callFailed = false;
		try {
			remoteCall(method, argument);
		} catch (XmlRpcException e) {
			callFailed = true;
		}
		assertTrue("Should fail invoking '" + method + "' with argument '" + argument + "'", 
					callFailed);
	}

	public void testInvalidCallsFail() throws Exception {
		remoteCall("main.prependPrefixString", "foo");
		testInvalidCall("main.prependPrefixString", new Integer(5)); // wrong arg type
		testInvalidCall("main.nonexistantMethod", "argument");
	}
	
	public static void main(String[] args) {
		junit.textui.TestRunner.run(RPCHandlerTests.class);
	}

	/*
	 * @see TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		this.rpcHandler = StandaloneXmlRpcServer.getXmlRpcServer(19432); 
		this.rpcHandler.shareObject("main", new ShareableObject());
		// Wait a second for the server thread to start...
		Thread.sleep(2000);
		super.setUp();
	}

	/*
	 * @see TestCase#tearDown()
	 */
	protected void tearDown() throws Exception {
		this.rpcHandler.stop();
		super.tearDown();
	}

}
