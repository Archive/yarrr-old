/*
 * Created on 20-Jan-05
 *
 */
package org.gnome.yarrr.tests;

import org.gnome.yarrr.person.FactAboutPerson;
import org.gnome.yarrr.person.Person;
import org.gnome.yarrr.person.PersonIsYarrrHacker;

/**
 * @author hp
 *
 */
public class FactAboutPersonTests extends PersonUsingTest {
	
	public static void main(String[] args) {
		junit.textui.TestRunner.run(FactAboutPersonTests.class);
	}
	
	public void testNotYarrrHackers() throws Exception {
		FactAboutPerson fact = new PersonIsYarrrHacker(); 
		Person person1 = getPerson1();
		assert !fact.isVerified(person1);
		Person person2 = getPerson2();
		assert !fact.isVerified(person2);
		
		FactAboutPerson.IVerifier verifier = fact.verify(person1);
		verifier.waitForCompletion();
		assert !fact.isVerified(person1);
		assert !person1.getAttributeBoolean(fact.getAttribute());
		
		verifier = fact.verify(person2);
		verifier.waitForCompletion();
		assert !fact.isVerified(person2);
			assert !person2.getAttributeBoolean(fact.getAttribute());
	}
	
	public void testAreYarrrHackers() throws Exception {
		PersonIsYarrrHacker fact = new PersonIsYarrrHacker(); 
		Person person1 = getPerson1();
		assert !fact.isVerified(person1);
		Person person2 = getPerson2();
		assert !fact.isVerified(person2);
		
		// add person 1 and check that we verify 1 as yarrr hacker and not 2
		fact.addYarrrHackerEmail(person1.getAttribute(Person.EMAIL));
		
		FactAboutPerson.IVerifier verifier = fact.verify(person1);
		verifier.waitForCompletion();
		assert fact.isVerified(person1);
		assert person1.getAttributeBoolean(fact.getAttribute());
		assert !fact.isVerified(person2);		
		
		verifier = fact.verify(person2);
		verifier.waitForCompletion();
		assert !fact.isVerified(person2);
		assert !person2.getAttributeBoolean(fact.getAttribute());
		
		// now add person 2 and check that we can verify them
		fact.addYarrrHackerEmail(person2.getAttribute(Person.EMAIL));
		
		assert fact.isVerified(person1);
		assert person1.getAttributeBoolean(fact.getAttribute());
		assert !fact.isVerified(person2);		
		
		verifier = fact.verify(person2);
		verifier.waitForCompletion();
		assert fact.isVerified(person2);
		assert person2.getAttributeBoolean(fact.getAttribute());
		
		assert fact.isVerified(person1);
	}
}
