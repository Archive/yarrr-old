/*
 * Created on 28-Jan-2005
 *
 */
package org.gnome.yarrr.tests;

import java.util.List;

import org.gnome.yarrr.Message;
import org.gnome.yarrr.Topic;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.person.Person;
//import org.gnome.yarrr.tests.TestUtils;

/**
 * @author dmalcolm
 */
public class TopicTests extends PersonUsingTest {
    
    static final String topicname1 = "Get your scurvy hands on th'ar deck!";
    static final String desc1 = "<h1>Deck-cleaning rota</h1><p>Avast!  Which of yer scurvy landlubbers will swab them there decks?  Arrrr!</p>";
    
    static final String topicname2 = "Design of the new File chooser";
    static final String desc2 = "<h1>A shiny new file chooser</h1><p>Make it so we don't have to like <b>type paths for everything, everytime</b>...</p>";
    
    private Person creator1;
    private Person creator2;
    
    protected void setUp() throws Exception
	{
		super.setUp();
		this.creator1 = getPerson1();
		this.creator2 = getPerson2();
	}
    
    protected Topic getTopic1() throws Exception {
        // Can add messages here:
    	Topic topic = new Topic(topicname1, creator1);
        HibernateUtil.getSession().flush();
        topic.updateDescription(TestUtils.makeTestDocumentContent(desc1), 0);
        return topic;
    }

    protected Topic getTopic2() throws Exception {
        //  Can't add messages here:
    	Topic topic = new Topic(topicname2, creator2);
        HibernateUtil.getSession().flush();
        topic.updateDescription(TestUtils.makeTestDocumentContent(desc2), 0);
        return topic;
    }
    
    public void testAddTopic() throws Exception {
    	Long Id1, Id2;
    	
    	Topic topic1 = getTopic1();
    	Id1 = topic1.getId();
            
    	assertEquals (topic1.getName(), topicname1);
    	assertEquals (topic1.getCreator(), creator1);
            
    	Topic topic2 = getTopic2();
    	Id2 = topic2.getId();
            
    	assertEquals (topic2.getName(), topicname2);
    	assertEquals (topic2.getCreator(), creator2);

    	HibernateUtil.newTestTransaction();

    	
    	topic1 = Topic.getTopic(Id1);
            
    	assertEquals (topic1.getName(), topicname1);
    	assertEquals (topic1.getCreator(), creator1);
    	
    	topic2 = Topic.getTopic(Id2);
    	
    	assertEquals (topic2.getName(), topicname2);
    	assertEquals (topic2.getCreator(), creator2);
    	
    	/* TODO make this work
    	 * assertEquals (topic1.getDescription().getResource(Document.MAIN_RESOURCE).getBytes(),
    	 desc1.getBytes());*/
    }
    
    public void testAddMessages() throws Exception {
    	Topic topic = getTopic1();            
    	assertEquals(0, topic.size());
    	
        // Make sure the topic is in the database
    	HibernateUtil.newTestTransaction();

    	Message message = TestUtils.getTestMessage(topic, 1);
    	assertTrue(topic.containsMessage(message));
    	assertEquals(1, topic.size());
    	
    	HibernateUtil.newTestTransaction();
            
    	List topics = Topic.getAllTopics();
    	//assertEquals(1, topics.size());
            
    	Topic storedTopic = (Topic)topics.get(0);
    	assertEquals(topic.getName(), storedTopic.getName());
            
    }

    public void testPersistMessages() throws Exception {
    	long topicID[] = new long[3];
    	long messageID[][] = new long[3][5];
            
    	// Create 3 topics and fill them each with 5 test messages
    	for (int i=0;i<3;i++) {
    		Topic topic = getTopic1();
    		
    		topicID[i] = topic.getId().longValue();
    		
    		HibernateUtil.getSession().flush();
    		
    		for (int j=0;j<5;j++) {
    			Message message = TestUtils.getTestMessage(topic, j);
    			messageID[i][j] = message.getId().longValue();
    			
    			assertEquals(j+1, topic.size());
    			assertTrue(topic.containsMessage(message));
    		}
    	}
    	
    	HibernateUtil.newTestTransaction();
            
    	// Now read the data back and check that all is as we expect:
    	List topics = Topic.getAllTopics();    
    	assertEquals(3, topics.size());
    	
    	for (int i=0;i<3;i++) {
    		Topic storedTopic = Topic.getTopic(topicID[i]);
    		
    		assertEquals(5, storedTopic.size());
    		
    		for (int j=0;j<3;j++) {
    			for (int k=0;k<5;k++) {
    				Message storedMessage = Message.getMessage(new Long(messageID[j][k]));
    				
    				if (i==j) {
    					assertTrue(storedTopic.containsMessage(storedMessage));
    				}
    				else {
    					assertFalse(storedTopic.containsMessage(storedMessage));
    				}
    				
    			}
    		}
    		
    		for (int k=0;k<5;k++) {
    			Message storedMessage = Message.getMessage(new Long(messageID[i][k]));
    			Topic storedTopicForMessage = storedMessage.getTopic();
    			assertEquals(storedTopic, storedTopicForMessage);                        
    		}
    	}
    }
    
    public static void main(String[] args) {
        junit.textui.TestRunner.run(TopicTests.class);
    }
}
