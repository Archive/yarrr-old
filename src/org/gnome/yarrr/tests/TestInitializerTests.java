/*
 * Created on 04-Feb-2005
 *
 */

package org.gnome.yarrr.tests;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.gnome.yarrr.IInitializer;
import org.gnome.yarrr.Topic;
import org.gnome.yarrr.email.MailingList;

/**
 * @author dmalcolm
 *
 */
public class TestInitializerTests extends PersonUsingTest {
    public void testCreation() throws Exception {
        IInitializer init = new TestInitializer();            
        init.doCreation();
        
        List topics = Topic.getAllTopics();        
        assertEquals(10,topics.size());
        
        for (Iterator i=topics.iterator(); i.hasNext(); ) {
            Topic topic = (Topic)i.next();
            Collection messages = topic.getAllMessages();
            //assertEquals (1, messages.size());
        }        
        
        MailingList desktopDevelList = MailingList.getMailingList("desktop-devel-list@gnome.org");
        assertNotNull(desktopDevelList);
        
        MailingList fedoraList = MailingList.getMailingList("fedora-list@redhat.com");
        assertNotNull(fedoraList);        
    }   

}
