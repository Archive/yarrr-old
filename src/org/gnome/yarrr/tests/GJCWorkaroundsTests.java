package org.gnome.yarrr.tests;

import junit.framework.TestCase;

import org.gnome.yarrr.email.GCJWorkarounds;

/**
 * @author walters
 *
 */
public class GJCWorkaroundsTests extends TestCase {

    public void testReplaceNull() {
        assertEquals("", GCJWorkarounds.replace("", "foo", "bar"));
    }
    
    public void testReplaceIdentity() {
        assertEquals("Hello", GCJWorkarounds.replace("Hello", "Foo", "Bar"));
    }
    
    public void testReplaceAA() {
        assertEquals("aaaa", GCJWorkarounds.replace("aa", "a", "aa"));
    }
    
    public void testReplaceWorld() {
        assertEquals("Hello ", GCJWorkarounds.replace("Hello World!", "World!", ""));
    }
    
    public void testReplaceHello() {
        assertEquals("Baby World!", GCJWorkarounds.replace("Hello World!", "Hello", "Baby"));
    }
    
    public void testReplaceEndPartial() {
        assertEquals("Hello World", GCJWorkarounds.replace("Hello World", "World!", ""));
    }
    
    public void testTooBigReplace() {
        assertEquals("Hello World!", GCJWorkarounds.replace("Hello World!", "Hello World, This String Is Too Big!", "blah"));
    }
}
