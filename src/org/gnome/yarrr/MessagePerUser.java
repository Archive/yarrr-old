/*
 * Created on Mar 4, 2005
 */
package org.gnome.yarrr;

import java.io.Serializable;

import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.person.Person;
import org.hibernate.LockMode;

/**
 * @author alex
 */
public class MessagePerUser implements Serializable {
	private static final long serialVersionUID = 1L;
	// ID:
	private Message message;
	private Person person;
	
	public static final class Data {
		public boolean read;
		public boolean important;
	}
	
	// Data:
	private boolean read;
	private boolean important; // This is just an example data, dunno if we want this
	
	protected MessagePerUser() {
	}
	
	public MessagePerUser(Message message, Person user) {
		this.message = message;
		person = user;
		HibernateUtil.getSession().save(this);
	}
	
	public Person getPerson() {
		return person;
	}
	private void setPerson(Person person) {
		this.person = person;
	}
	public Message getMessage() {
		return message;
	}
	private void setMessage(Message message) {
		this.message = message;
	}
	public boolean getRead() {
		return read;
	}
	public void setRead(boolean read) {
		this.read = read;
	}
	public boolean getImportant() {
		return important;
	}
	public void setImportant(boolean important) {
		this.important = important;
	}

	public boolean updateFromData(Data data) {
    	// Lock topic
    	HibernateUtil.getSession().refresh(this, LockMode.UPGRADE);
    	
    	boolean readChanged = read != data.read;
		read = data.read;
		
    	boolean importantChanged = important != data.important;
		important = data.important;

		if (readChanged || importantChanged) {
			Message m = this.getMessage();
	    	HibernateUtil.getSession().refresh(m, LockMode.UPGRADE);
	    	
			boolean externalChanged = false;
			
			if (readChanged && m.updateNumReaders(data.read))
					externalChanged = true;
		
			return externalChanged;
		}
		
		return false;
	}
}
