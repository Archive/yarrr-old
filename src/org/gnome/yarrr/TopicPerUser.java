/*
 * Created on Mar 4, 2005
 */
package org.gnome.yarrr;

import java.io.Serializable;

import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.person.Person;
import org.hibernate.LockMode;

/**
 * @author alex
 */
public class TopicPerUser implements Serializable {
	private static final long serialVersionUID = 1L;
	// ID:
	private Topic topic;
	private Person person;
	
	//  Data:
	private long version; 
	private boolean followed;
	
	protected TopicPerUser() {
		
	}
	
	public TopicPerUser(Topic topic, Person person) {
		this.topic = topic;
		this.person = person;
		version = 0;
		followed = false;
		HibernateUtil.getSession().save(this);
	}
	
	public Topic getTopic() {
		return topic;
	}
	private void setTopic(Topic topic) {
		this.topic = topic;
	}
	public Person getPerson() {
		return person;
	}
	private void setPerson(Person person) {
		this.person = person;
	}
	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}
	public boolean getFollowed() {
		return followed;
	}
	private void setFollowed(boolean followed) {
		this.followed = followed;
	}
	
	public void updateFollowed(boolean followed) {
    	HibernateUtil.getSession().refresh(this, LockMode.UPGRADE);
    	
    	version += 1;
    	this.followed = followed;
	}
}
