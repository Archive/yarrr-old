/*
 * Created on 28-Jan-2005
 */
package org.gnome.yarrr;

import java.io.Serializable;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.hibernate.Persistent;
import org.gnome.yarrr.person.Person;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.StaleObjectStateException;
import org.hibernate.criterion.Expression;

/**
 * @author dmalcolm & alexl
 * 
 */
public class Topic extends Persistent {   
    private static final Log log = LogFactory.getLog(Topic.class);

    static public class TopicLink implements Serializable {
		private static final long serialVersionUID = 1L;
		private Topic source;
		private Topic target;
		private long versionAtCreation;
		
		protected TopicLink() {
		}
		
		protected TopicLink(Topic source, Topic target, long version) {
			this.source = source;
			this.target = target;
			this.versionAtCreation = version;
			HibernateUtil.getSession().save(this);
		}
		public Topic getSource() {
			return source;
		}
		private void setSource(Topic source) {
			this.source = source;
		}
		public Topic getTarget() {
			return target;
		}
		private void setTarget(Topic target) {
			this.target = target;
		}
		public long getVersionAtCreation() {
			return versionAtCreation;
		}
		private void setVersionAtCreation(long versionAtCreation) {
			this.versionAtCreation = versionAtCreation;
		}
	}
    
	static public class MessageRemoval implements Serializable {
		private static final long serialVersionUID = 1L;
		Topic parentTopic;
		long versionAtRemoval;
		long messageId;
		
		protected MessageRemoval() {
		}
		
		protected MessageRemoval(Topic parent, long version, long messageId) throws HibernateException {
			parentTopic = parent;
			this.messageId = messageId;
			this.versionAtRemoval = version;
			HibernateUtil.getSession().save(this);
		}
		
		protected MessageRemoval(Topic parent, long version, Message message) throws HibernateException {
			this (parent, version, message.getId().longValue());
		}

		public long getMessageId() {
			return messageId;
		}
		private void setMessageId(long messageId) {
			this.messageId = messageId;
		}
		public Topic getParentTopic() {
			return parentTopic;
		}
		private void setParentTopic(Topic parentTopic) {
			this.parentTopic = parentTopic;
		}
		private long getVersionAtRemoval() {
			return versionAtRemoval;
		}
		private void setVersionAtRemoval(long versionAtRemoval) {
			this.versionAtRemoval = versionAtRemoval;
		}
		
		public boolean equals(Object obj) {
			MessageRemoval other = (MessageRemoval) obj;
			
			if (!parentTopic.equals(other.parentTopic))
				return false;
			
			if (messageId != other.messageId)
				return false;
			
			return versionAtRemoval == other.versionAtRemoval;
		}
		
		public int hashCode() {
			return parentTopic.hashCode() | (int)versionAtRemoval;
		}
	}
	
	// Immutable Data:
    private String name;
    private Person creator;
    private Timestamp createdTime;
    
    // Mutable data:
    private long version; // Updated when mutable data in this or child changes
    private Document description;
    private Timestamp lastPostTime; // Example mutable data, can be used to track topic "liveness", or sort it
    
    // TODO: add a live set of participants; "presence"
    
    protected Topic() {
    	version = 0;
    	createdTime = new Timestamp(new Date().getTime());
    }
    
    public Topic (String name, Person creator) throws HibernateException  {
    	this();
    	
        this.name = name;
        this.creator = creator;
        
        HibernateUtil.getSession().save(this);        
    }
    
    public String getName () {
        return name;
    }
    private void setName (String name) {
        this.name = name;
    }
    public Person getCreator() {
        return creator;
    }
    private void setCreator(Person creator) {
        this.creator = creator;
    }    
	public Timestamp getCreatedTime() {
		return createdTime;
	}
	private void setCreatedTime(Timestamp created) {
		createdTime = created;
	}
	public long getVersion() {
		return version;
	}
	public void setVersion(long version) {
		this.version = version;
	}
	public Document getDescription() {
        return description;
    }     
    private void setDescription(Document description) {
        this.description = description;
    }    
    
	public Timestamp getLastPostTime() {
		if (lastPostTime == null)
			return createdTime;
		return lastPostTime;
	}
	public void setLastPostTime(Timestamp lastPostTime) {
		this.lastPostTime = lastPostTime;
	}
	public List getNewMessagesSince(long clientVersion) {
    	return HibernateUtil.getSession()
        .createQuery("select m from Message m  "+
        		"WHERE m.topic = :topicId and m.versionAtCreation <= :currentVersion " +
        		"and m.versionAtCreation > :clientVersion")
        .setLong("topicId", getId().longValue())
        .setLong("clientVersion", clientVersion)
        .setLong("currentVersion", this.version)
        .list();
	}
    
	public List getNewLinksSince(long clientVersion) {
    	return HibernateUtil.getSession()
        .createQuery("select target from Topic$TopicLink link join link.target target "+
        		"WHERE link.source = :source and link.versionAtCreation <= :currentVersion " +
        		"and link.versionAtCreation > :clientVersion")
        .setLong("source", getId().longValue())
        .setLong("clientVersion", clientVersion)
        .setLong("currentVersion", this.version)
        .list();
	}
	
	public List getOldMessagesChangedSince(long clientVersion) {
    	return HibernateUtil.getSession()
        .createQuery("select m from Message m " +
        		"WHERE m.topic = :topicId and m.versionAtCreation <= :clientVersion " +
        		"and m.version > :clientVersion")
        .setLong("topicId", getId().longValue())
        .setLong("clientVersion", clientVersion)
        .list();
		
	}
	
    public List getAllMessages() throws HibernateException {
    	return HibernateUtil.getSession()
        .createQuery("select m from Message m "
        		+"WHERE m.topic = :topic")
        .setLong("topic", this.getId().longValue())
        .list();
    }
    
	public List getMessages(Date start, Date end)  throws HibernateException {
    	return HibernateUtil.getSession()
        .createQuery("select m from Message m "
        		+"WHERE m.topic = :topic and "
        		+"m.sentTime >= :start and m.sentTime <= :end")
        .setLong("topic", this.getId().longValue())
        .setDate("start", start)
        .setDate("end", end)
        .list();
	}
    
    public int size() throws HibernateException {
    	return getAllMessages().size();
    }

    public boolean containsMessage(Message message) throws HibernateException {
    	return HibernateUtil.getSession()
        .createQuery("from Message m WHERE m.topic = :topic and m = :messageid")
        .setLong("topic", this.getId().longValue())
        .setLong("messageid", message.getId().longValue())
        .uniqueResult() != null;
    }
    
	/**
	 * Retrieve a topic from the store
	 * @param Id the Id of the {@link Topic} to retrieve
	 * @return a {@link Topic}
	 * @throws HibernateException 
	 * @throws NoMatchFoundException 
	 */
	static public Topic getTopic(long id) throws HibernateException {
		return getTopic(new Long(id));
	}

    /**
     * Retrieve a topic from the store
     * @param Id the Id of the {@link Topic} to retrieve
     * @return a {@link Topic}
     * @throws HibernateException 
     * @throws NoMatchFoundException 
     */
    static public Topic getTopic(Long id) throws HibernateException {
        return (Topic)HibernateUtil.getSession().get(Topic.class, id);
    }

	/**
	 * Retrieve a list of topics from the store.
	 * 
	 * @param a list of {@link Topic} Ids
	 * @return a list of {@link Topic}
	 * @throws HibernateException 
	 */
	static public List/*Topic*/ getTopics(List/*Long*/ Ids) throws HibernateException {
		return HibernateUtil.getSession().createCriteria(Topic.class)
		          .add( Expression.in("id", Ids))
		          .list();
	}

	public TopicPerUser getPerUser(Person user) {
		TopicPerUser perUser = 
			(TopicPerUser) HibernateUtil.getSession()
			   .createQuery("select topic_per_user from TopicPerUser topic_per_user join topic_per_user.topic topic "+
			   "WHERE topic = :topic and topic_per_user.person = :user")
			   .setLong("topic", getId().longValue())
			   .setLong("user", user.getId().longValue())
			   .uniqueResult();
		if (perUser == null) 
			perUser = new TopicPerUser(this, user);
		return perUser;
	}

    /**
	 * @param resources Map of resource name to resource data for the new version
	 * @param documentVersion the version the old document was based on, zero if none
	 */
	public void updateDescription(Map resources, int documentVersion) {
    	HibernateUtil.getSession().refresh(this, LockMode.UPGRADE);

    	long newVersion = version + 1;

    	if (description != null) {
    		if (documentVersion == 0) {
    			throw new StaleObjectStateException(Document.class.getName(), getId());
    		}	
    		description.updateDocument(resources, documentVersion);
    	} else {
    		if (documentVersion != 0) {
    			throw new StaleObjectStateException(Document.class.getName(), getId());
    		}	
    		description = Document.createDocument(resources);
    	}
    	
    	version = newVersion;
	}

	/**
	 * Posts a message with a specific timestamp. Only used for tests.
	 * @param author
	 * @param subject
	 * @param replyToId
	 * @param document
	 * @param time
	 * @return
	 */
	public Message postMessageWithTimestamp(Person author, String subject, Long replyToId, Document document, Timestamp time) {
    	
    	// Lock topic
    	HibernateUtil.getSession().refresh(this, LockMode.UPGRADE);

    	version = version + 1;
    	Message m = new Message(time, author, subject, replyToId, document, this);
    	this.lastPostTime = m.getSentTime();
    	
    	return m;
	}

    public Message postMessage(Person author, String subject, Long replyToId, Document document) {
    	return postMessageWithTimestamp(author, subject, replyToId, document, null);
    }
    
    public void removeMessage(long messageId) throws HibernateException {
    	//FIXME: Implement
    }

    public void removeMessage(Message message) throws HibernateException {
    	removeMessage(message.getId().longValue());
    }
    
	public void createLink(Topic linkTarget) {
    	HibernateUtil.getSession().refresh(this, LockMode.UPGRADE);
    	
		version += 1;
    	TopicLink link = new TopicLink(this, linkTarget, version);
	}
	
	public List getMessagePerUser(Person user) {
    	return HibernateUtil.getSession()
        .createQuery("select message_per_user from MessagePerUser message_per_user join message_per_user.message message "+
        		"WHERE message_per_user.person = :user and message.topic = :topic")
        .setLong("user", user.getId().longValue())
		.setLong("topic", this.getId().longValue())
        .list();
    }
    
    private List getMessagePerUser(Person user, Collection messageIds) {
    	return HibernateUtil.getSession()
        .createQuery("select message_per_user from MessagePerUser message_per_user join message_per_user.message message "+
        		"WHERE message in (:ids) and message_per_user.person = :user " +
        		"ORDER BY message.id")
        .setParameterList("ids", messageIds)
        .setLong("user", user.getId().longValue())
        .list();
    }
    
    public long updateMessagePerUserData(Person user, Map/*MessagePerUser.Data*/ dataMap) {
    	/*  The lock order is:
    	 *   per-user messages, 
    	 *   global messages
    	 *   per-user topic
    	 *   global topic
    	 */    	
    	
    	Set idSet = dataMap.keySet();
    	List perUserList = getMessagePerUser(user, idSet);
    	List changedMessages = new ArrayList();
    	
    	// Update existing per-user data
    	Iterator i = perUserList.iterator();
    	while (i.hasNext()) {
    		MessagePerUser perUser = (MessagePerUser) i.next();
    		Message message = perUser.getMessage();
    		Long messageId = message.getId();
    		idSet.remove(messageId); // We handled this message id
    		MessagePerUser.Data data = (MessagePerUser.Data) dataMap.get(messageId);
    		
    		// This will lock the MessagePerUser 
    		if (perUser.updateFromData(data)) {
    			// We also changed and locked the Message
    			changedMessages.add(message);
    		}
    	}
    	// Create new per-user data
    	i = idSet.iterator();
    	while (i.hasNext()) {
    		Long messageId = (Long)i.next();
    		Message message;
			try {
				message = Message.getMessage(messageId);
	    		MessagePerUser perUser = new MessagePerUser(message, user);
	    		MessagePerUser.Data data = (MessagePerUser.Data) dataMap.get(messageId);
	    		
	    		// This will lock the MessagePerUser 
	    		if (message.getTopic() == this &&
	    			perUser.updateFromData(data)) {
	    			// We also changed and locked the Message
	    			changedMessages.add(message);
	    		}
			} catch (NoMatchFoundException e) {
				log.warn("Trying to add per-user data for nonexistant message");
			}
    	}
    	
    	TopicPerUser topicPerUser = getPerUser(user);
    	HibernateUtil.getSession().refresh(topicPerUser, LockMode.UPGRADE);
    	
    	long newUserVersion = topicPerUser.getVersion() + 1;
    	topicPerUser.setVersion(newUserVersion);
    	
    	if (changedMessages.size() > 0) {
    		//FIXME: Isn't there a race here, if the topic wasn't fresh when 
    		//       we updated the per-user data?
    		
    		// Lock topic (changed messages are locked)
        	HibernateUtil.getSession().refresh(topicPerUser, LockMode.UPGRADE);
        	
        	// Update version of messages and topic
    		version = version + 1;
    		
    		i = changedMessages.iterator();
    		while (i.hasNext()) {
    			Message m = (Message) i.next();
    			m.setVersion(version);
    		}
    	}
    	return newUserVersion;
    }

    static private Query queryFollowedTopicsMessages(Person user, String extraQuery) {
    	return HibernateUtil.getSession()
        .createQuery("select message from TopicPerUser tu join tu.topic topic, Message message "+
        		"WHERE tu.followed = true and message.topic = topic " +
        		"and (" + extraQuery + ") " +
        		"ORDER BY topic")
        .setLong("user", user.getId().longValue());
    }

    /**
     * Returns all messages in subscribed topics that was posted after a certain time.
     * The messages are ordered by topic.
     * 
     * @param user the user that is doing the query
     * @param time return posts newer than this time
     * @return
     */
    static public List /*Message*/ getMessagesFromFollowedTopicsPostedSince(Person user, Timestamp time) {
    	return queryFollowedTopicsMessages(user, "message.sentTime > :time")
        .setTimestamp("time", time)
        .list();
    }

    static private Query queryFollowedTopicsLinks(Person user, String extraQuery) {
    	return HibernateUtil.getSession()
        .createQuery("select target from TopicPerUser tu join tu.topic source, Topic$TopicLink link join link.target target "+
        		"WHERE tu.followed = true and link.source = source " +
        		"and (" + extraQuery + ") " +
        		"ORDER BY source")
        .setLong("user", user.getId().longValue());
    }
    
    static public List /*Topic*/ getTopicLinkedFromFollowedTopicsChangeSince(Person user, Timestamp time) {
    	return queryFollowedTopicsLinks(user, "target.lastPostTime >= :time") 
    		.setTimestamp("time", time)
    		.list();
    }

    
	static public List/*Topic*/ getAllTopics() throws HibernateException {
		return HibernateUtil.getSession().createCriteria(Topic.class)
		          .list();
	}

    public static void deleteAll() throws HibernateException, SQLException {
        Statement statement = HibernateUtil.getSession().connection().createStatement();
        statement.execute("DELETE FROM TopicLink");
        statement.execute("DELETE FROM MessageRemoval");
        statement.execute("DELETE FROM Topic");    
    }

    // TODO: get liveness of a topic...

}
