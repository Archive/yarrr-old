/*
 * Created on 04-Feb-2005
 */
package org.gnome.yarrr;

/**
 * @author dmalcolm
 *
 * A class for doing first-time initializing of a new Yarrr server's database,
 * populating it with some basic initial topics.  
 */
public interface IInitializer {
    public class CreationException extends Exception {
		private static final long serialVersionUID = 3257002176772781624L;

		public CreationException(Throwable e) {
            super(e);
        }
    }

    public void doCreation() throws CreationException;    
}
