/*
 * Created on Jan 15, 2005
 */
package org.gnome.yarrr;

import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.xmlrpc.XmlRpcServer;
import org.gnome.yarrr.xmlrpc.DefaultMethods;

/**
 * @author seth
 */
public class YarrrServlet extends HttpServlet {

	private static final String MESSAGE_STORE_ATTRIBUTE = "org.gnome.yarrr.servlet.Yarrr";
	
	public static final long serialVersionUID = 1;

	private XmlRpcServer xmlrpc;
	
	public YarrrServlet() {
		super();
		
		this.xmlrpc = new XmlRpcServer ();
		try {
			Object messageStore = new DefaultMethods(new Yarrr());
			this.xmlrpc.addHandler("$default", messageStore);
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		byte[] result = this.xmlrpc.execute (request.getInputStream ());
		response.setContentType ("text/xml");
		response.setContentLength (result.length);
		OutputStream out = response.getOutputStream();
		out.write (result);
		out.flush ();
	}
}
