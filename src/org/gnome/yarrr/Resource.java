package org.gnome.yarrr;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.SQLException;

import org.gnome.yarrr.hibernate.Persistent;

/**
 * A resource is like a "file" that is part of a document.
 * Each version of a document have a set of named resources
 * that can be read. The resource just contains a binary stream.    
 * 
 * @author alex
 */
public class Resource extends Persistent {
	private Document document;
	private int version;
	private String name;
	private Blob data;
	
	protected Resource() {
	}
	
	protected Resource(Document document, int version, String name, Blob data) {
		this.document = document;
		this.version = version;
		this.name = name;
		this.data = data;
	}
	
	public Resource(Document document, int version, String name, byte [] data) {
		this(document, version, name, org.hibernate.Hibernate.createBlob(data));
	}
	
	public Document getDocument() {
		return document;
	}
	
	private void setDocument(Document document) {
		this.document = document;
	}
	
	public int getVersion() {
		return version;
	}
	
	private void setVersion(int version) {
		this.version = version;
	}
	
	public String getName() {
		return name;
	}
	
	private void setName(String name) {
		this.name = name;
	}
	
	public Blob getData() {
		return data;
	}
	
	private void setData(Blob data) {
		this.data = data;
	}
	
	/**
	 * Get the contents of the resource as an array of bytes
	 * @return the contents of the resource
	 * @throws SQLException
	 */
	public byte [] getBytes() throws SQLException {
		return data.getBytes(0L, (int)data.length());
	}
	
	/**
	 * Get a stream of the Resource contents
	 * @return an InputStream that reads from the resource data
	 * @throws SQLException
	 */
	public InputStream getInputStream() throws SQLException {
		return data.getBinaryStream();
	}
}
