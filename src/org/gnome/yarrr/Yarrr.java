package org.gnome.yarrr;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.gnome.yarrr.IInitializer.CreationException;
import org.gnome.yarrr.database.IDatabaseManager;
import org.gnome.yarrr.database.PostgresqlManager;
import org.gnome.yarrr.database.IDatabaseManager.DatabaseCreationException;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.tests.TestInitializer;
import org.gnome.yarrr.xmlrpc.DefaultMethods;


/*
 * Created on Jan 13, 2005
 */

/**
 * @author seth
 */
public class Yarrr {
	
	private static StandaloneXmlRpcServer rpcServer = null;
	
	static Logger logger = Logger.getLogger(Yarrr.class);

	private Configuration configuration;

	private IDatabaseManager dbManager;
	
	public static IDatabaseManager getDefaultDBManager() throws IOException, DatabaseCreationException, ClassNotFoundException {
		String path = System.getProperty("org.gnome.yarrr.store.path", "/tmp/yarrr-store-" + System.getProperty("user.name", "_unknownuser_"));
		return Yarrr.getDefaultDBManager(path);
	}
	
	public static IDatabaseManager getDefaultDBManager(String storePath) throws IOException, DatabaseCreationException, ClassNotFoundException {
		return new PostgresqlManager(new File(storePath));
	}
	
	
	public Yarrr() throws IOException, DatabaseCreationException, ClassNotFoundException, NoSuchAlgorithmException, HibernateException, NoSuchProviderException, CreationException {
		this(Yarrr.getDefaultDBManager());
	}
	
	public Yarrr(IDatabaseManager dbManager) throws HibernateException, CreationException {
		this.dbManager = dbManager;
		PropertyConfigurator.configure(Yarrr.class.getResource("log4j.properties"));
		
		logger.info("Initializating PostgreSQL DB");
		
		this.configuration = HibernateUtil.buildConfiguration(dbManager.getUrl());

		HibernateUtil.init(this.configuration);

        if (dbManager.wasVirgin()) {
            logger.info("Database was virgin, populating with test topics");
            IInitializer init = new TestInitializer();            
            init.doCreation();       
        }
	}
	
	public Yarrr(String storePath) throws IOException, DatabaseCreationException, ClassNotFoundException, NoSuchAlgorithmException, HibernateException, NoSuchProviderException, CreationException {
		this(Yarrr.getDefaultDBManager(storePath));
   	}
    	
	public void updateSchemas() throws IOException {
		this.dbManager.snapshot();
		new SchemaUpdate(this.configuration).execute(true, true);
	}
	
    public int getRPCServerPort() {
        return Yarrr.rpcServer.getServerPort();    
    }
    
    public void startXmlRpcServer() {
    	DefaultMethods rpcMessageStore;
    	try {
    		Yarrr.rpcServer = StandaloneXmlRpcServer.getXmlRpcServer(19842);
    		rpcMessageStore = new DefaultMethods(this);
    		Yarrr.rpcServer.shareObject("$default", rpcMessageStore);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
	
	public static void main(String[] args) {
		Yarrr yarrr = null;
		String storePath = null;
		boolean withXmlRpc = true;
		
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("--with-xmlrpc")) {
				withXmlRpc = true;
			} else {
				storePath = args[i];
			}
		}
		
		try {
			if (storePath != null) {
				yarrr = new Yarrr(storePath);
			} else {
				yarrr = new Yarrr();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (yarrr != null && withXmlRpc) {
			yarrr.startXmlRpcServer();
		}
		
		System.out.println("Press enter to terminate.");
		try {
			System.in.read();
		} catch (IOException e) {
			logger.error("Couldn't read from stdin", e);
		}

		logger.info("Yarrr shutting down.");
		
		if (Yarrr.rpcServer != null) {
			Yarrr.rpcServer.stop();
		}
	}
}
