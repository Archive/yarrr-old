/*
 * Created on Jan 19, 2005
 */
package org.gnome.yarrr;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Security;
import java.security.spec.EncodedKeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * @author seth
 */
public final class Cryptography {
	private static final String KEY_ALGORITHM = "RSA";
	private static final String SYMMETRIC_ALGORITHM = "AES";
	
	
	private static KeyFactory keyFactory = null;
	private static KeyPairGenerator keyPairGenerator = null;
	private static KeyGenerator symmetricKeyGenerator = null;
	
	static {
		// Try to load Free Software JCE providers
		try {
			Provider bouncyCastleProvider = (Provider)Class.forName("org.bouncycastle.jce.provider.BouncyCastleProvider").newInstance();
			Security.insertProviderAt(bouncyCastleProvider, Security.getProviders().length);
		} catch (Exception e1) {
			try {
				Provider gnuCryptoProvider = (Provider)Class.forName("gnu.crypto.jce.GnuCrypto").newInstance();
				Security.insertProviderAt(gnuCryptoProvider, Security.getProviders().length);
			} catch (Exception e2) {
			}
		}
	}
	
	public static class EncryptedData {
		private byte[] encryptedContents;
		private byte[] encryptedKey;

		public EncryptedData(byte[] encryptedKey, byte[] encryptedContents) {
			this.encryptedKey = encryptedKey;
			this.encryptedContents = encryptedContents;
		}
		public byte[] getEncryptedContents() {
			return encryptedContents;
		}

		public byte[] getEncryptedSymmetricKey() {
			return encryptedKey;
		}
	}
	
	public static class EncryptionException extends Exception {
		private static final long serialVersionUID = 3544948878907355702L;

		EncryptionException(Exception e) {
			super(e);
		}
	}
	
	public static byte[] encryptSymmetricKey(Key encryptingKey, SecretKey symmetricKey) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException {
		Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
		cipher.init(Cipher.ENCRYPT_MODE, encryptingKey);
		byte[] symmetricKeyBytes = symmetricKey.getEncoded();
		try {
			return cipher.doFinal(symmetricKeyBytes);
		} catch (BadPaddingException e) {
			// this should never happen, only on decryption
			return null;
		}
	}
	
	public static SecretKey decryptSymmetricKey(Key decryptingKey, byte[] encryptedSymmetricKey) throws IllegalBlockSizeException, BadPaddingException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException {
		Cipher cipher = Cipher.getInstance(KEY_ALGORITHM);
		cipher.init(Cipher.DECRYPT_MODE, decryptingKey);
		byte[] symmetricKeyBytes = cipher.doFinal(encryptedSymmetricKey);
		return new SecretKeySpec(symmetricKeyBytes, SYMMETRIC_ALGORITHM);
	}
	
	public static EncryptedData encrypt(Key key, byte[] contents) throws EncryptionException {
		try {
			// Encrypt the data using a one-time symmetric key
			SecretKey symmetricKey = Cryptography.generateSymmetricKey();
			Cipher symmetricCipher = Cipher.getInstance(SYMMETRIC_ALGORITHM);
			symmetricCipher.init(Cipher.ENCRYPT_MODE, symmetricKey);
			byte[] encryptedContents = symmetricCipher.doFinal(contents);
			
			// Encrypt the one-time symmetric key using the key
			byte[] encryptedSymmetricKey = encryptSymmetricKey(key, symmetricKey);
			
			return new EncryptedData(encryptedSymmetricKey, encryptedContents);
		} catch (Exception e) {
			throw new EncryptionException(e);
		}
	}
	
	public static byte[] decrypt(Key key, EncryptedData data) throws EncryptionException {
		try {
			// Decrypt the one-time symmetric key using the key
			SecretKey symmetricKey = decryptSymmetricKey(key, data.getEncryptedSymmetricKey());
			
			// Decrypt the data using the one-time symmetric key
			Cipher symmetricCipher = Cipher.getInstance(SYMMETRIC_ALGORITHM);
			symmetricCipher.init(Cipher.DECRYPT_MODE, symmetricKey);
			return symmetricCipher.doFinal(data.getEncryptedContents());
		} catch (Exception e) {
			throw new EncryptionException(e);
		}
	}	
	
	public static String getEncryptionAlgorithmUsed() {
		return KEY_ALGORITHM;
	}
	
	private static void dumpAvailableProviders() {
		// be all friendly and dump a list of avail algorithms
		Provider[] providers = Security.getProviders();
		System.err.println("Registered providers:");
		for (int ii=0; ii<providers.length; ii++) {
			Provider pp = providers[ii];
			System.err.println("Name: " + pp.getName());
			System.err.println("Info: " + pp.getInfo());
			System.err.println("Properties");
			List props = new ArrayList(pp.keySet());
			Collections.sort(props);
			for (Iterator keys=props.iterator(); keys.hasNext(); ) {
				String key = (String) keys.next();
				System.err.println(key + ": " + pp.getProperty(key));
			}
			System.err.println("=======================================================");
		}		
	}
	
	private static KeyFactory getKeyFactory() throws NoSuchAlgorithmException {
		if (keyFactory == null) {
			try {
				keyFactory = KeyFactory.getInstance(KEY_ALGORITHM);
			} catch (NoSuchAlgorithmException e) {
				dumpAvailableProviders();
				throw e;
			}
		}
		return keyFactory;
	}

	public static byte[] privateKeyToBytes(PrivateKey privateKey) throws EncryptionException {
		try {
			EncodedKeySpec privateKeySpec = (EncodedKeySpec)getKeyFactory().getKeySpec(privateKey, PKCS8EncodedKeySpec.class);
			return privateKeySpec.getEncoded();
		} catch (Exception e) {
			throw new EncryptionException(e);
		}
	}
	
	public static byte[] publicKeyToBytes(PublicKey publicKey) throws EncryptionException {
		try {
			EncodedKeySpec publicKeySpec = (EncodedKeySpec)getKeyFactory().getKeySpec(publicKey, X509EncodedKeySpec.class);
			return publicKeySpec.getEncoded();
		} catch (Exception e) {
			throw new EncryptionException(e);
		}
	}
	
	public static PublicKey bytesToPublicKey(byte[] publicKeyBytes) throws EncryptionException {
		try {
			EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);
			return getKeyFactory().generatePublic(publicKeySpec);
		} catch (Exception e) {
			throw new EncryptionException(e);
		}			
	}
	
	public static PrivateKey bytesToPrivateKey(byte[] privateKeyBytes) throws EncryptionException {
		try {
			EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
			return getKeyFactory().generatePrivate(privateKeySpec);
		} catch (Exception e) {
			throw new EncryptionException(e);
		}			
	}
	
	public static KeyPair generateKeyPair() throws NoSuchAlgorithmException, NoSuchProviderException {
		if (keyPairGenerator == null) {
			keyPairGenerator = KeyPairGenerator.getInstance(KEY_ALGORITHM);
		}

		return keyPairGenerator.generateKeyPair();	
	}
	
	public static SecretKey generateSymmetricKey() throws NoSuchAlgorithmException, NoSuchProviderException {
		if (symmetricKeyGenerator == null) {
			symmetricKeyGenerator  = KeyGenerator.getInstance(SYMMETRIC_ALGORITHM);
		}
			
		return symmetricKeyGenerator.generateKey();
	}
}
