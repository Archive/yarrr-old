/*
 * Created on Jan 15, 2005
 */
package org.gnome.yarrr.xmlrpc;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xmlrpc.Invoker;
import org.apache.xmlrpc.XmlRpcHandler;
import org.gnome.yarrr.Cryptography;
import org.gnome.yarrr.Document;
import org.gnome.yarrr.Message;
import org.gnome.yarrr.MessagePerUser;
import org.gnome.yarrr.Resource;
import org.gnome.yarrr.Topic;
import org.gnome.yarrr.TopicPerUser;
import org.gnome.yarrr.Yarrr;
import org.gnome.yarrr.email.Importer;
import org.gnome.yarrr.email.Importer.InjectionResult;
import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.person.Person;

/**
 * @author seth
 */
public class DefaultMethods implements XmlRpcHandler {

	static Log logger = LogFactory.getLog(DefaultMethods.class);
	
	public Hashtable getTopic(String topicIDString) throws Exception {
		Topic topic = Topic.getTopic(new Long(topicIDString));
		Hashtable hash = Utils.topicToHash(topic);
			
		return hash;
	}
	
	public Hashtable getMessage(String messageIDString) throws Exception {
		Message m = Message.getMessage(messageIDString);
		Hashtable hash = Utils.messageToHash(m);
		
		return hash;
	}

	public boolean heartbeat(byte [] personKey) throws Exception {
		Person person = Person.getPerson(personKey);
		person.heartbeat();
		return true;
	}
	
	public Vector getActivePeople() throws Exception {
		List people = Person.getActivePeople();
		Vector peopleHashes = new Vector();
		Iterator iter = people.iterator();
			
		while (iter.hasNext()) {
			Hashtable hash = Utils.personToHash((Person)iter.next());
			peopleHashes.add(hash);
		}
				
		return peopleHashes;
	}
	
	public byte [] getResource (String documentID, String resource, int version) throws Exception {
		if (resource == null)
			resource = "main";
		
		byte [] data = null;
		long id = Long.parseLong(documentID);  
		Document doc = Document.get(id);
		Resource res = doc.getResource(resource, version);
		data = res.getBytes();
		
		return data;
	}
	
	public Vector getResourceAndVersion (String documentID, String resource) throws Exception {
		if (resource == null)
			resource = "main";
		
		byte [] data = null;
			
		long id = Long.parseLong(documentID);  
		Document doc = Document.get(id);
		int version = doc.getCurrentVersion();
		Resource res = doc.getResource(resource);
		data = res.getBytes();
			
		Vector v = new Vector();
		v.add (new Integer(version));
		v.add (data);
		return v;
	}
	
	public boolean updateResource (String documentID, String resource, int basedOnVersion, byte [] data) throws Exception {
		long id = Long.parseLong(documentID);  
		Document doc = Document.get(id);
		doc.updateDocument (resource, data, basedOnVersion);
		return true;
	}

	public String createTopic(String name, byte[] personPublicKeyBytes) throws Exception {
		Person person = Person.getPerson(personPublicKeyBytes);
		Topic topic = new Topic(name, person); 
			
		return topic.getId().toString();
	}

	public String postMessage(String topicId, String subject, String replyTo, byte[] posterPublicKeyBytes, Hashtable content) throws Exception {
		PublicKey publicKey = Cryptography.bytesToPublicKey(posterPublicKeyBytes);
		
		Topic topic = Topic.getTopic( new Long(topicId));
		Person poster = Person.getPerson(publicKey);
			
		Document doc = Document.createDocument(content);
		Long replyToId = null;
		if (replyTo != null && replyTo.length() != 0) 
			replyToId = new Long(replyTo);
		Message message = 
			topic.postMessage(poster, subject, replyToId, doc);
			
		return message.getId().toString();
	}
    
	public String addLink(String sourceTopicId, String targetTopicId) {
		Topic source = Topic.getTopic( new Long(sourceTopicId));
		Topic target = Topic.getTopic( new Long(targetTopicId));
			
		source.createLink(target);
		
		return target.getId().toString();
	}
	
    public String injectEmail(byte [] contents) throws Exception {
    	Importer importer = new Importer();
        InjectionResult injectionResult = importer.injectEmail(contents);
        String stringResult = "avast! added email:\""+injectionResult.email.getMimeMessage().getSubject()+"\"\n";
        
        for (Iterator i = injectionResult.messageInsertions.iterator(); i.hasNext();) {
            Importer.EmailMessageInsertion insertion = (Importer.EmailMessageInsertion)i.next();
        
            stringResult += "added message:\""+insertion.emailMessage.getSubject()+"\"";
            if (insertion.topic != null) {
                stringResult += " to topic \"" + insertion.topic.getName() + "\" ";;
            } else {
                stringResult += " to database (not added to any topics)";
            }        
        }
        logger.info(stringResult);

        return stringResult;
    }
	
	public Vector updateTopic(String topicId, byte[] userPublicKeyBytes, String versionAtClientString, String perUserVersionAtClientString) throws Exception {
		long versionAtClient = new Long(versionAtClientString).longValue();
		long perUserVersionAtClient = new Long(perUserVersionAtClientString).longValue();

		Vector result = new Vector();

		Topic topic = Topic.getTopic(new Long(topicId));
		Person user = Person.getPerson(userPublicKeyBytes);
		TopicPerUser topicPerUser = topic.getPerUser(user);

		long newVersion = topic.getVersion();
		long newPerUserVersion = topicPerUser.getVersion();
			
		List newMessages = topic.getNewMessagesSince(versionAtClient);
		List newLinks = topic.getNewLinksSince(versionAtClient);
		List changedMessages = topic.getOldMessagesChangedSince(versionAtClient);
		List removedMessages = new Vector(); // Will add removal support later
			
		// New topic versions
		result.add(Long.toString(newVersion));
		result.add(Long.toString(newPerUserVersion));
			
		// Newly added messages
		Iterator i = newMessages.iterator();
		Vector v = new Vector();
		while (i.hasNext()) {
			Message message = (Message) i.next();
			MessagePerUser perUser = message.getPerUser(user);
				
			Hashtable hash = Utils.newData(message.getId());
			Utils.messageImmutableToHash(message, hash);
			Utils.messageMutableToHash(message, hash);
			if (perUser != null)
				Utils.messagePerUserToHash(perUser, hash);
			v.add(hash);
		}
		result.add(v);
			
		// Newly added links
		i = newLinks.iterator();
		v = new Vector();
		while (i.hasNext()) {
			Message message = (Message) i.next();
			MessagePerUser perUser = message.getPerUser(user);
				
			Hashtable hash = Utils.newData(message.getId());
			Utils.messageImmutableToHash(message, hash);
			Utils.messageMutableToHash(message, hash);
			if (perUser != null)
				Utils.messagePerUserToHash(perUser, hash);
			v.add(hash);
		}
		result.add(v);
		
		// Changed old topic
		i = changedMessages.iterator();
		v = new Vector();
		while (i.hasNext()) {
			Message message = (Message) i.next();
			Hashtable hash = Utils.newData(message.getId());
			Utils.messageMutableToHash(message, hash);
			v.add(hash);
		}
		result.add(v);
		
		// Changed per-user data
		v = new Vector();
		if (topicPerUser.getVersion() != perUserVersionAtClient) {
			List changedPerUser = topic.getMessagePerUser(user);
			
			i = changedPerUser.iterator();
			
			while (i.hasNext()) {
				MessagePerUser perUser = (MessagePerUser) i.next();
				Message message = perUser.getMessage();
				Hashtable hash = Utils.newData(message.getId());
				Utils.messagePerUserToHash(perUser, hash);
				v.add(hash);
			}
		}
		result.add(v);

		// Removals (list of message ids)
		// FIXME: implement this
		result.add(removedMessages);
			
		return result;
	}

	public boolean updateSchemas() throws Exception {
		this.yarrr.updateSchemas();
		return true;
	}

	/**
	 * 
	 * @param topicId
	 * @param userPublicKeyBytes
	 * @param messagePerUserData hash table mapping string id => hash table of message per-user data
	 * @return
	 * @throws Exception
	 */
	public String updatePerUserMessageData(String topicId, byte[] userPublicKeyBytes, Hashtable messagePerUserData) throws Exception {
		
		Topic topic = Topic.getTopic(new Long(topicId));
		Person user = Person.getPerson(userPublicKeyBytes);

		/* Convert xmlrpc hash to MessagePerUser.Data Map */
			
		HashMap dataMap = new HashMap();
		Iterator i = messagePerUserData.entrySet().iterator();
		while (i.hasNext()) {
			Map.Entry entry = (Map.Entry) i.next();
			long id = Long.parseLong((String)entry.getKey());
			Hashtable hashData = (Hashtable) entry.getValue(); 
			MessagePerUser.Data data = Utils.messagePerUserDataFromHash(hashData);
			dataMap.put(new Long(id), data);
		}
			
		long newUserVersion = topic.updateMessagePerUserData(user, dataMap);

		return Long.toString(newUserVersion);
	}

	public boolean updateFollowTopic(String topicId, byte[] userPublicKeyBytes, boolean follow) throws Exception {
		Person user = Person.getPerson(userPublicKeyBytes);
		Topic topic = Topic.getTopic(new Long(topicId));
		TopicPerUser topicPerUser = topic.getPerUser(user);
		
		topicPerUser.updateFollowed(follow);
		
		return true;
	}

	
	public String updateTopicDescription(String topicId, byte[] userPublicKeyBytes /* etc */) {
		//TODO: Implement
		return "";
	}
	
	public Hashtable getMessages(Vector topicIDs, Date start, Date end) throws Exception {
		Hashtable topicIDsToMessages = new Hashtable();
			
		for (Iterator i = topicIDs.iterator(); i.hasNext();) {
			String topicID = (String)i.next();
				
			Topic topic = Topic.getTopic(new Long(topicID));
				
			Collection messages = topic.getMessages(start, end);
			Vector convertedMessages = new Vector();
			for (Iterator messageIter = messages.iterator(); messageIter.hasNext();) {
				Hashtable message = Utils.messageToHash((Message)messageIter.next());
				convertedMessages.add(message);
			}
			topicIDsToMessages.put(topicID, convertedMessages);
		}
			
		return topicIDsToMessages;
	}
	
	public Hashtable getPersonKeyPair() throws Exception {
		
		Hashtable hashtable = new Hashtable();
		KeyPair keyPair = Cryptography.generateKeyPair();
		hashtable.put("publicKey", Cryptography.publicKeyToBytes(keyPair.getPublic()));
		hashtable.put("privateKey", Cryptography.privateKeyToBytes(keyPair.getPrivate()));
		return hashtable;
	}
	
	public boolean setPersonAttribute(byte[] publicKeyBytes, String attributeName, String value) throws Exception {
		Person person = Person.getPerson(publicKeyBytes);
		person.setAttribute(attributeName, value);
			
		return true;
	}
	
	public String getPersonAttribute(byte[] publicKeyBytes, String attributeName) throws Exception {
		Person person = Person.getPerson(publicKeyBytes);
		String value = person.getAttribute(attributeName);
		return value;
	}
	
	public Hashtable getPersonAttributes(byte[] publicKeyBytes) throws Exception {
		Hashtable attributes = new Hashtable();
		Person person = Person.getPerson(publicKeyBytes);
		attributes = new Hashtable(person.getAttributes());
		
		return attributes;
	}
	
	public Hashtable getPersonAttributes(byte[] publicKeyBytes, Vector attributes) throws Exception {
		Person person = Person.getPerson(publicKeyBytes);
		Hashtable result = new Hashtable();
			
		Iterator i = attributes.iterator();
		while (i.hasNext()) {
			String attr = (String) i.next();
			if (person.hasAttribute(attr)) { 
				String val = person.getAttribute(attr);
				result.put(attr, val);
			}
		}
			
		return result;
	}
	
	/* Functions for managing the XML-RPC interface, not calls */
	
	private String parameterAsString(Object o) throws NoSuchAlgorithmException {
		String oString = o.toString();
		
		// Special case public keys:
		try {
			// Try to convert to byte array
			byte [] bytes = (byte [])o;
			// Try to convert it to a public key
			PublicKey publicKey = Cryptography.bytesToPublicKey(bytes);
			// It succeeded, lets make a digest of it to display
			byte[] digest = MessageDigest.getInstance("SHA-1").digest(bytes);
			String digeststr = new BigInteger(digest).abs().toString(16);
			oString = "[SHA-1:" + digeststr + "]";
		} catch (Cryptography.EncryptionException e) {
			// Wasn't a key
		} catch (ClassCastException e) {
			// Wasn't a byte[]
		} 
		
		return oString;
	}
	
	/**
	 * Called in order to invoke any methods of this class. Performs
	 * introspection, and pre-processes a few arguments.
	 * 
	 * @see XmlRpcHandler.execute
	 */
	public Object execute(String methodName, Vector params) throws Exception {
		try {
			HibernateUtil.beginTransaction();
			
			if (logger.isInfoEnabled()) {
				logger.info("invoking xmlRpc method " + methodName + "() with " + params.size()  +" arguments:");
				Iterator i;	int n;
				for (n = 1, i = params.iterator(); i.hasNext(); n++) {
					Object o = i.next();
					logger.info(n +": " + o.getClass().getName() + " = " + parameterAsString(o));
				}
			}
			
			Object res = this.invoker.execute(methodName, params);
			HibernateUtil.commitTransaction();
			return res;
		} catch (Exception e) {
			logger.error("XML-RPC failure", e);
			throw e;
		} finally {
			HibernateUtil.closeSession();
		}
	}
	
	private Yarrr yarrr;
	private Invoker invoker;
	
	public DefaultMethods (Yarrr yarrr) throws NoSuchAlgorithmException {
		this.invoker = new Invoker(this);
		this.yarrr = yarrr;
	}
}
