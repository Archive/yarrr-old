/*
 * Created on Feb 6, 2005
 */
package org.gnome.yarrr.xmlrpc;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import org.gnome.yarrr.Cryptography;
import org.gnome.yarrr.Document;
import org.gnome.yarrr.Message;
import org.gnome.yarrr.MessagePerUser;
import org.gnome.yarrr.Topic;
import org.gnome.yarrr.TopicPerUser;
import org.gnome.yarrr.person.Person;

/**
 * @author seth
 */
public abstract class Utils {
	
	public static Hashtable newData(Long id) {
		Hashtable hash = new Hashtable();
		hash.put("id", id.toString());
		return hash;
	}

	public static void topicImmutableToHash(Topic topic, Hashtable hash) {
		hash.put("name", topic.getName());
		Person creator = topic.getCreator();
		PublicKey key = creator.getPublicKey();
		assert key != null;
		hash.put("creator", key.getEncoded());
		hash.put("createdTime", topic.getCreatedTime());
	}

	public static void topicMutableToHash(Topic topic, Hashtable hash) {
		hash.put("version", Long.toString(topic.getVersion()));
	    Document description = topic.getDescription();
	    if (description != null) {
	    	hash.put("descriptionId", description.getId().toString());
	    	hash.put("descriptionVersion", Long.toString(description.getCurrentVersion()));
	    }
		hash.put("lastPostTime", topic.getLastPostTime());
	}

	public static void topicPerUserToHash(TopicPerUser perUser, Hashtable hash) {
		hash.put("user:version", Long.toString (perUser.getVersion()));
	}
	
	public static Hashtable personToHash(Person person) {
		Hashtable hash = new Hashtable();
		
		assert person != null;
		
		if (person.hasAttribute(Person.NAME)) {
			hash.put("name", person.getAttribute(Person.NAME));
		}
		
		PublicKey key = person.getPublicKey();
		assert key != null;
		hash.put("publicKey", key.getEncoded());

		return hash;
	}

	public static Hashtable topicToHash(Topic topic) throws Cryptography.EncryptionException {
		Hashtable hash = newData(topic.getId());
		Utils.topicImmutableToHash(topic, hash);
		Utils.topicMutableToHash(topic, hash);
		return hash;
	}

	public static void messageImmutableToHash(Message message, Hashtable hash) {
		Person author = message.getAuthor();
		PublicKey key = author.getPublicKey();
		assert key != null;
		hash.put("author", key.getEncoded());
		hash.put("subject", message.getSubject());
		if (message.getReplyToId() != null)
			hash.put("replyTo", message.getReplyToId().toString());
		hash.put("documentID", Long.toString(message.getDocument().getDocumentId()));
		hash.put("sentTime", message.getSentTime());
	}
	
	public static void messageMutableToHash(Message message, Hashtable hash) {
		// External Mutable Data
		hash.put("version", Long.toString(message.getVersion()));
		hash.put("manyReaders", new Boolean(message.getManyReaders()));
	}

	public static void messagePerUserToHash(MessagePerUser perUser, Hashtable hash) {
		hash.put("user:read", new Boolean (perUser.getRead()));
		hash.put("user:important", new Boolean (perUser.getImportant()));
	}
	
	public static Hashtable messageToHash(Message message) throws Cryptography.EncryptionException {
		Hashtable hash = newData(message.getId());
		Utils.messageImmutableToHash(message, hash);
		Utils.messageMutableToHash(message, hash);
		return hash;
	}
	public static MessagePerUser.Data messagePerUserDataFromHash(Hashtable hash) {
		MessagePerUser.Data data = new MessagePerUser.Data();
		
		Boolean read = (Boolean) hash.get("read"); 
		data.read = read.booleanValue();
		
		Boolean important = (Boolean) hash.get("important"); 
		data.important = important.booleanValue();
		
		return data;
	}
	
	private static Object getField(Hashtable hash, String fieldName, boolean fieldIsRequired, Class type) throws ClassCastException, NullPointerException {
		Object fieldValue = hash.get(fieldName);
		if (fieldValue == null && fieldIsRequired) {
			throw new NullPointerException("Required field '" + fieldName + "' was not present"); 
		}
		if (fieldValue != null && !(type.isInstance(fieldValue))) {
			throw new ClassCastException("Field '" + fieldName + "' expected to be of type " + type.getName() + " but was " + fieldValue.getClass().getName());
			// FIXME GCJ getSimpleName is 1.5 only
			// throw new ClassCastException("Message field '" + fieldName + "' expected to be of type " + type.getSimpleName() + " but was " + fieldValue.getClass().getSimpleName());
		}
		return fieldValue;
	}
	
	public static ArrayList/*Long*/ messageIDStringVectorToList(Vector messageIDs) {
		ArrayList list = new ArrayList();
		for (int i=0; i < messageIDs.size(); i++) {
			list.add(new Long((String)messageIDs.get(i)));
		}
		return list;
	}
}
