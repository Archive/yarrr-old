package org.gnome.yarrr;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

import org.gnome.yarrr.hibernate.HibernateUtil;
import org.gnome.yarrr.hibernate.Persistent;

import org.hibernate.criterion.Expression;
import org.hibernate.criterion.Order;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.StaleObjectStateException;

/**
 * Each document consists of a set of named resources, which can contain
 * any binary data, and is identified by a unique identifier (a 64bit integer) 
 * that is stable over time. The main data of a document is the resource
 * named "main".
 *  
 * Document resources are generally immutable, that is, once created they don't 
 * change. However, documents are versioned, so you can update the document with
 * new versions of a set of resources. The old version is still accessible, but
 * is not the most recent one. The version of a document is a integer that 
 * increments on each change.
 *  
 * Changes/additions of documents are atomic as a whole, either they apply in
 * full, or you get an exception and can rollback the transaction. 
 * 
 * @author alex
 *
 */
public class Document extends Persistent {
	private int currentVersion;
	private List resources; // list of Resource
    
    public static final String MAIN_RESOURCE ="main";
	
	protected Document() {
		currentVersion = 1;
		resources = new ArrayList();
	}
	
	public long getDocumentId() {
		return getId().longValue(); 
	}
	
	public int getCurrentVersion() {
		return currentVersion;
	}
	public void setCurrentVersion(int version) {
		currentVersion = version;
	}
	public List getResources() {
		return resources;
	}
	public void setResources(List resources) {
		this.resources = resources;
	}
    
	/**
	 * Get a specific version of a named resource
	 * 
	 * @param resource the name of the resource
	 * @param version the version of the document to get the resource of
	 * @return the resource
	 * @throws HibernateException
	 */
	public Resource getResource(String resource, int version) throws HibernateException {
		Resource res = (Resource)
		HibernateUtil.getSession().createCriteria(Resource.class)
		.add( Expression.eq("document.id", getId()) )
		.add( Expression.eq("name", resource) )
		.add( Expression.le("version", new Integer (version)) )
		.addOrder ( Order.desc("version") )
		.setMaxResults(1)
		.uniqueResult();
		return res;
	}
	
	/**
	 * Get the most recent version of a named resource
	 * 
	 * @param resource the name of the resource
	 * @return the resource
	 * @throws HibernateException
	 */
	public Resource getResource(String resource)  throws HibernateException {
		return getResource(resource, currentVersion);
	}
	
	
	private void insertResources (Map resources, int version) {
		Iterator iter = resources.entrySet().iterator();
		
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry)iter.next();
			Resource res = new Resource(this, version,
					(String)entry.getKey(), (byte [])entry.getValue());
			this.resources.add(res);
		}
	}
	
	static public Document get(long id) throws HibernateException {
		return (Document) HibernateUtil.getSession().get(Document.class, new Long (id));
	}

	/**
	 * Create a new document given some data.
	 * Note: This function should be run in a transaction
	 * 
	 * @param data the data
	 * @return a new document
	 * @throws HibernateException
	 */
	static public Document createDocument (byte [] data) throws HibernateException {
		HashMap map = new HashMap();
		map.put(Document.MAIN_RESOURCE, data);
		return createDocument (map);
	}

	/**
	 * Create a new empty document.
	 * Note: This function should be run in a transaction
	 * 
	 * @param data the data
	 * @return a new document
	 * @throws HibernateException
	 */
	static public Document createDocument () throws HibernateException {
		byte [] data = null;
		try {
			data = "".getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
		}
		return createDocument (data);
	}

    static public Document createDocument (String htmlFragment) throws HibernateException {
        try {
            return Document.createDocument(htmlFragment.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new Error();            
        }
    }
	
	/**
	 * Create a new document with several resources.
	 * Note: This function should be run in a transaction
	 * 
	 * @param resources mapping from name of resource to contents (as a byte array)
	 * @return a new document
	 * @throws HibernateException
	 */
	static public Document createDocument (Map resources) throws HibernateException {
		Document doc = new Document();
		HibernateUtil.getSession().save(doc);
		/* Resources with this id are protected against concurrency since this is a new
		 * id from the counter. Nobody can be creating resources for this id before
		 * we've returned the id from this function and commited the transaction.
		 */
		doc.insertResources (resources, 1);
		return doc;
	}
	
	/**
	 * Update document with a new set of resources, creating a new version.
	 * Pass in a set of the resources that are to be updated or 
	 * added. Any existing resources not in the set are kept as 
	 * they are. There is no way to remove a resource.
	 * 
	 * @param resources the updated/new resources as a mapping from resource 
	 *        to content (as byte array)
	 * @param documentVersion the version of the document this update was based on
	 * @throws StaleObjectStateException if documentVersion is not the latest version of the document
	 * @throws HibernateException
	 */
	public void updateDocument (Map resources, int documentVersion) throws HibernateException {
		// Make sure the in-session object has the right version
		// Concurrent changes to the document is handled by optimistic locking with versioning
		if (documentVersion != currentVersion) {
			throw new StaleObjectStateException(Document.class.getName(), getId());
		}
		
		// Force lazy association to be read so we update dirty flag on insert
		Hibernate.initialize(getResources());
		
		int newVersion = currentVersion + 1;
				
		insertResources (resources, newVersion);
	}

	public void updateDocument (String resource, byte [] data, int documentVersion) throws HibernateException {
		HashMap map = new HashMap();
		map.put(resource, data);
		updateDocument(map, documentVersion);
	}

	public void updateDocument (byte [] data, int documentVersion) throws HibernateException {
		HashMap map = new HashMap();
		map.put(Document.MAIN_RESOURCE, data);
		updateDocument(map, documentVersion);
	}

	
	public void delete() throws HibernateException {
		Iterator i = resources.iterator();
		while (i.hasNext()) {
			Resource r = (Resource) i.next();
			i.remove();
			HibernateUtil.getSession().delete(r);
		}
		HibernateUtil.getSession().delete(this);
	}


	/**
	 * WARNING: Removes all documents 
	 * 
	 * @throws HibernateException
	 * @throws SQLException 
	 */
	static public void deleteAll() throws HibernateException, SQLException {
		Statement statement = HibernateUtil.getSession().connection().createStatement();
		statement.execute("DELETE FROM Resource");
		statement.execute("DELETE FROM Document");
	}
}
