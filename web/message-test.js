// DOM free

function getMessage(index) {
	return new Message(index, index, new Date("7/" + index + "/1981"), "Subject " + index, "Contents " + index);
}

function testMessage() {
	var message = getMessage(4);
}
function testMessages() {
	var messages = new Messages();
	var numAdds = 0;
	var numRemoves = 0;
	messages.addAddMessagesCallback(function (messages) {
		numAdds += messages.length;
	});
	messages.addRemoveMessagesCallback(function (messages) {
		numRemoves += messages.length;
	});
	messages.addMessages([getMessage(1), getMessage(2), getMessage(3)]);
	messages.addMessages([getMessage(4)]);
	messages.removeMessages([getMessage(3)]);
	if (numAdds != 4) {
		throw "Expected " + 4 + " but got " + numAdds;
	}

	if (numRemoves != 1) {
		throw "Expected " + 1 + " but got " + numRemoves;
	}

	var numMessages = messages.getMessages();
	if (numMessages.length != 3) {
		throw "Expected " + 3 + " but got " + numMessages.length;
	}
}