// DOM free

var xmlrpc = importModule("xmlrpc");
var codecs = importModule("codecs");
var yarrr = new xmlrpc.ServiceProxy("./yarrr", ["heartbeat", "getResourceAsUTF8", "getRootTopics", "getPersonKeyPair", "addMessage", "removeMessages", "getChangesSince"]);

var numResponsesReceived = 0;

function setKeyPair(newPublicKey, privateKey) {
  setCookie("publicKey", newPublicKey, new Date("7/10/2081"));
  setCookie("privateKey", privateKey, new Date("7/10/2081"));
}

function getPublicKey() {
  var publicKey = getCookie("publicKey");
  if (getCookie("publicKey")) {
    try {
      var keyPair = yarrr.getPersonKeyPair();
      setKeyPair(keyPair['publicKey'], keyPair['privateKey']);
      publicKey = keyPair['publicKey'];
    } catch (e) {
      reportException(e.message);
    }
  }
  if (publicKey == null) {
    reportError("Couldn't fetch keys from the Yarrr server");
  }
  return publicKey;
}

function updateTopics() {
  var callback = function (topicsLocal, err) {
    if (err == null){
      topics = topicsLocal;
      if (currentTopicID == null && topics.length > 0) {
        currentTopicID = String(topics[0].id);
      }
      addNewTopics(topics);      
      setTimeout("initiateUpdate()", 50);      
    } else {
      reportException(err.message);
    }
  }

  yarrr.getRootTopics(callback);
}

function update(callback) {
  var callback = function (changes, err) {
    if (err != null) reportException(err);
    try {
      addNewMessages(changes.addedMessages);
      addNewTopics(changes.addedTopics);
      setActiveUserList(changes.activePeople);
      lastUpdateTime = changes.timestamp;
    } catch (e) {
      reportException(e);
    }
    requestPending = false;
  }
  
  yarrr.getChangesSince(String(currentTopicID), lastUpdateTime, callback);
}

var requestPending = false;