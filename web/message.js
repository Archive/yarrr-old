// DOM free

Message.prototype.setAuthorName = function (authorName) {
	this.authorName = authorName;
}

Message.prototype.toString = function() {
	return "(" + this.date + "," + this.subject + ")";
}

Message.prototype.equals = function(other) {
	return other.messageID == this.messageID;
}

function Message(messageID, authorKey, date, subject, contents) {
	this.messageID = messageID;
	this.authorKey = authorKey;
	this.date = date;
	this.subject = subject;
	this.contents = contents;
}

function messagesDoCallbacks(callbacks, argument, dontCall) {
	for (var i = 0; i < callbacks.length; i++) {
		if (callbacks[i] != dontCall) {
			(callbacks[i])(argument);
		}
	}
}

Messages.prototype.getMessages = function () {
	return this.messages;	
}

Messages.prototype.addMessages = function (messages, dontCall) {
	for (var i = 0; i < messages.length; i++) {
		this.messages.push(messages[i]);
	}
	messagesDoCallbacks(this.addMessagesCallbacks, messages, dontCall);
}

Messages.prototype.removeMessages = function (messages, dontCall) {
	for (var i = 0; i < messages.length; i++) {
		for (var j = 0; j < this.messages.length; j++) {
			if (this.messages[j].equals(messages[i])) {
				this.messages.splice(j, 1);
			}
		}
	}
	messagesDoCallbacks(this.removeMessagesCallbacks, messages, dontCall);
}

Messages.prototype.addAddMessagesCallback = function (callback) {
	this.addMessagesCallbacks.push(callback);
}

Messages.prototype.addRemoveMessagesCallback = function (callback) {
	this.removeMessagesCallbacks.push(callback);
}

function Messages() {
	this.messages = new Array();
	this.addMessagesCallbacks = new Array();
	this.removeMessagesCallbacks = new Array();
}
