/*
 * Created on Feb 10, 2005
 */
package org.gnome.yarrr.tests;

import java.io.File;
import java.io.FileReader;
import java.io.Reader;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.Scriptable;

import junit.framework.TestCase;

/**
 * @author seth
 */
public class JavaScriptClientTests extends TestCase {
	Context context;
	Scriptable scope;
	
	public void testMessages() throws Exception {
		loadModules(new String[] {"message.js", "message-test.js"});
		run("testMessages()");
	}
	
	public void testMessage() throws Exception {
		loadModules(new String[] {"message.js", "message-test.js"});
		run("testMessage()");
	}
	
	private Object run(String cmd) {
		return this.context.evaluateString(this.scope, cmd, "<cmd>", 1, null);
	}
	
	private void loadModules(String[] jsModules) throws Exception {
		for (int i = 0; i < jsModules.length; i++) {
			Reader reader = new FileReader(new File("./web/" + jsModules[i]));
			this.context.evaluateReader(this.scope, reader, jsModules[i], 1, null);
		}
	}

	protected void setUp() throws Exception {
		super.setUp();
		this.context = Context.enter();
		this.scope = this.context.initStandardObjects(); 
	}

	protected void tearDown() throws Exception {
		super.tearDown();
		Context.exit();
	}

}
