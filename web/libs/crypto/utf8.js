
// UTF-8

var utf8sets = [0x800,0x10000,0x110000];

function utf8Encrypt(){
  if (i==0) { prgr='UTF-8'; bData=[]; tot=sData.length; j=0; }
  var z = Math.min(i+100,tot);
  while (i<z) {
    var c = sData.charCodeAt(i++);
    if (c<0x80){ bData[j++]=c; continue; }
    var k=0; while(k<utf8sets.length && c>=utf8sets[k]) k++;
    if (k>=utf8sets.length) throw( "UTF-8: "+unExpChar(c) );
    for (var n=j+k+1;n>j;n--){ bData[n]=0x80|(c&0x3F); c>>>=6; }
    bData[j]=c+((0xFF<<(6-k))&0xFF)
    j += k+2;
  }
}

function utf8Decrypt(){
  if (i==0){ prgr='UTF-8'; sData=""; tot=bData.length; }
  var z=Math.min(i+100,tot);
  while (i<z){
    var c = bData[i++];
    var e = '0x'+c.toString(16);
    var k=0; while(c&0x80){ c=(c<<1)&0xFF; k++; }
    c >>= k;
    if (k==1||k>4) throw('UTF-8: invalid first byte '+e);
    for (var n=1;n<k;n++){
      var d = bData[i++];
      e+=',0x'+d.toString(16);
      if (d<0x80||d>0xBF) break;
      c=(c<<6)+(d&0x3F);
    }
    if ( (k==2&&c<0x80) || (k>2&&c<utf8sets[k-3]) ) throw("UTF-8: invalid sequence "+e+'; c='+c);
    sData+=String.fromCharCode(c);
  }
}
