
// Math

var wMax = 0xFFFFFFFF;
function rotb(b,n){ return ( b<<n | b>>>( 8-n) ) & 0xFF; }
function rotw(w,n){ return ( w<<n | w>>>(32-n) ) & wMax; }

function getNrBits(i){ var n=0; while (i>0){ n++; i>>>=1; } return n; }
function getMask(n){ return (1<<n)-1; }

var bMax=0xFFFF;
var bMaxBits=getNrBits(bMax);

function bGetNrBits(a){ return (a.length-1)*bMaxBits+getNrBits(a[a.length-1]); }

function bPlusIs( a, b, shift ){
  var i;
  if (!shift) shift=0;
  var n = Math.max(a.length, b.length+shift)
  var c = 0;
  for ( i=0; i<n; i++ ){
    c += (a[i]?a[i]:0) + (b[i-shift]?b[i-shift]:0);
    a[i] = c&bMax;
    c >>>= bMaxBits;
  }
  if (c) a[i]=c;
  return a;
}

function bMinIs(a,b){
  var n = Math.max(a.length, b.length)
  var c = 0;
  var i;
  for ( i=0; i<n; i++ ){
    c += (a[i]?a[i]:0) - (b[i]?b[i]:0);
    a[i] = c&bMax;
    c >>= bMaxBits;
  }
  if (c) a[i]=c;
  while (a[a.length-1]==0&&a.length>1) a.pop();
  return a;
}

function bShlIs(a,n){
  if (a==0) return;
  var c = 0;
  var i;
  while (n>=bMaxBits){ a.splice(0,0,0); n-=bMaxBits; }
  for ( i=0; i<a.length; i++ ){
    var t = a[i]
    a[i] = ( t<<n | c ) & bMax;
    c = t>>>(bMaxBits-n);
  }
  if (c) a[i]=c;
  return a;
}

function bShrIs(a,n){
  var c = 0;
  while (n>=bMaxBits){ a.splice(0,1); n-=bMaxBits; }
  for ( var i=a.length-1; i>=0; i-- ){
    var t = a[i];
    a[i] = t>>>n | c;
    c = (t<<(bMaxBits-n)) & bMax;
  }
  while (a[a.length-1]==0&&a.length>1) a.pop();
  return a;
}

function bCmp(a,b){
  var al=a.length;
  var bl=b.length;
  if ( al>bl ) return 1;
  if ( al<bl ) return -1;
  for ( var i=al-1; i>=0; i-- )
    if (a[i]>b[i]) return 1;
    else if (a[i]<b[i]) return -1;
  return 0;
}

function sbMul(i,a){
  var r=[0];
  var c = 0;
  var n = 0;
  while ( n<a.length ){
    c+=i*a[n];
    r[n++]=c&bMax;
    c >>>= bMaxBits;
  }
  if (c) r[n]=c;
  return r;
}

function bMul(a,b){
  var r=[0];
  for ( var n=0; n<b.length; n++ ) bPlusIs( r, sbMul( b[n], a ), n );
  return r;
}

function bDiv(a,b){
  var q=[0];
  var r=a.slice(0);
  var n=1+bGetNrBits(a)-bGetNrBits(b);
  if ( n<1 ) return [[0],a];
  bShlIs(b,n);
  for (var i=0;i<n;i++){
    bShrIs(b,1);
    bShlIs(q,1);
    if (bCmp(r,b)>=0){
      q[0]|=1
      bMinIs(r,b);
    }
  }
  return [q,r];
}


var bSeeds = [(new Date()).getTime()&bMax, 1];
var bSeedsRIdx=0;
var bSeedsWIdx=1;

function bSetSeed(){
  bSeeds[bSeedsWIdx] = (bSeeds[bSeedsWIdx]<<8) | ((new Date()).getTime()&0xFF);
  if (bSeeds[bSeedsWIdx]>bMax){
    bSeeds[bSeedsWIdx] &= bMax;
    bSeeds[++bSeedsWIdx]=1;
  }
}

function bRnd(a, bits, ones){
  var n = Math.ceil(bits/bMaxBits);
  var hMask = bMax>>>(n*bMaxBits-bits);
  var i;

  for (i=0; i<n; i++){
    a[i]=(Math.floor((bMax+1)*Math.random())+bSeeds[bSeedsRIdx])&bMax;
    bSeedsRIdx = (bSeedsRIdx+1)%bSeeds.length;
  }
  a.length=i; a[--i] &= hMask;
  hMask = (hMask+1)>>>1;
  while (ones && ones-->0){ a[i]|=hMask; hMask>>>=1; if (hMask==0){ hMask=(bMax+1)>>>1; i--; } }
  while (a[a.length-1]==0&&a.length>1) a.pop();
}

function bFromBytes(a){
  var k=a.length;
  var r=[0];
  while (k>0){ bShlIs(r,8); r[0]|=a[--k]; }
  return r;
}

function bToBytes(a){
  var b=a.slice(0);
  var r=[];
  while (b.length>1||b[0]!=0){ r[r.length]=b[0]&0xFF; bShrIs(b,8); }
  return r;
}

function insLen(len, bits){
  var n=(bits+7)>>>3;
  while (bits<n*8){ len=((len&0xFF)<<bits)|len; bits*=2; }
  while (n-->0) bData.unshift( (len>>>(n*8))&0xFF );
}

function getLen(bits){
  var n = (bits+7)>>>3;
  var r=0;
  for (var i=0; i<n; i++) r += bData.shift()<<(i*8);
  return r&getMask(bits);
}

function unExpChar(c){
  return "unexpected character '"+String.fromCharCode(c)+"' (code 0x"+c.toString(16)+")";
}
