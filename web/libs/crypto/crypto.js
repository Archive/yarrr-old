/*
 *  Copyright @ 2004 Michiel van Everdingen
 *
 *  This program is free software; you can redistribute it and/or modify it under the terms of the
 *  GNU General Public License as published by the 'Free Software Foundation; either version 2 
 *  of the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 *  Public License for more details.

 *  You should have received a copy of the GNU General Public License along with this program; if not,
 *  write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. 
 */

var rsaExp;
var rsaExp2;
var rsaPubExp;
var rsaPrivExp;
var rsaMod;
var rsaNbits;
var rsaBlockIn;
var rsaBlockIn2;
var rsaBlockOut;
var rsaMaxi;
function rsaFreeMem(){
  rsaExp=rsaExp2=rsaPubExp=rsaPrivExp=rsaMod=null;
  rsaBlockIn=rsaBlockIn2=rsaBlockOut=null;
}

function rsaCrypt(){
  if ( rsaExp2[0]&1 ) rsaBlockOut = bDiv( bMul(rsaBlockOut,rsaBlockIn), rsaMod )[1];
  rsaBlockIn = bDiv( bMul(rsaBlockIn,rsaBlockIn), rsaMod )[1];
  bShrIs(rsaExp2,1);
}

function rsaInit(){
  var err = 'RSA key: invalid format';
  prgr = 'RSA';
  if (key.length<2) throw(err);
  var expLen=key[0]+256*key[1];
  var modLen=key.length-2-expLen;
  if (modLen<1 || expLen<1) throw(err);
  rsaExp = bFromBytes( key.slice(2, 2+expLen) );
  rsaMod = bFromBytes( key.slice(2+expLen) );
  rsaNbits = bGetNrBits(rsaMod);
}

function rsaGetBlock(nrBits){
  var k=i+nrBits;
  var block=[0];
  while ((k%8!=0)&&(k>i)){ k--; bShlIs(block,1); block[0]|=(bData[k>>>3]>>(k%8))&1; }
  while (k>i+7){ k-=8; bShlIs(block,8); block[0]|=bData[k>>>3]; }
  while (k>i){ k--; bShlIs(block,1); block[0]|=(bData[k>>>3]>>(k%8))&1; }
  return block;
}

function rsaSetBlock(nrBits, block){
  if (bGetNrBits(block)>nrBits) throw('RSA: invalid key/data combination');
  var k=0;
  var m=1<<(j%8)
  while ((j%8!=0)&&(k<nrBits)){ bData[j>>>3]|=m*(block[0]&1); bShrIs(block,1); j++; k++; m<<=1; }
  while (k<nrBits-7){ bData[j>>>3]=block[0]&0xFF; bShrIs(block,8); j+=8; k+=8; }
  bData[j>>>3]=0;
  m=1; while (k<nrBits){ bData[j>>>3]|=m*(block[0]&1); bShrIs(block,1); j++; k++; m<<=1; }
}

function rsaEncrypt(bData){
  if (i==0){
    rsaInit();
    if (lenInd){
      var mod=(rsaNbits+13)>>>3;
      insLen( bData.length%mod, getNrBits(mod-1) );
    }
    tot = bData.length*8;
    if ( tot%(rsaNbits-1)>0 ) tot+=rsaNbits-1-tot%(rsaNbits-1);
    bData.splice(0,0,0);
    i=8; tot+=8; j=0;
    while (bData.length*8<tot) bData.push(0);
    rsaExp2=[0];
  }

  if ( rsaExp2.length==1 && rsaExp2[0]==0 ){
    rsaBlockIn = rsaGetBlock(rsaNbits-1);
    rsaMaxi=i+rsaNbits-2;
    rsaBlockOut = [1];
    rsaExp2 = rsaExp.slice(0);
  }
  rsaCrypt();
  if (i<rsaMaxi) i++;

  if ( rsaExp2.length==1 && rsaExp2[0]==0 ){
    rsaSetBlock(rsaNbits, rsaBlockOut);
    i=rsaMaxi+1;
    if (j>i-8){ bData.splice((j+7)>>>3,0,0); i+=8; tot+=8; }
    if (i>=tot){ bData.length=(j+7)>>>3; rsaFreeMem(); }
  }
  return bData;
}

