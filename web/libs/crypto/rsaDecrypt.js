
function rsaGetBlock(bData, nrBits){
  var k=i+nrBits;
  var block=[0];
  while ((k%8!=0)&&(k>i)){ k--; bShlIs(block,1); block[0]|=(bData[k>>>3]>>(k%8))&1; }
  while (k>i+7){ k-=8; bShlIs(block,8); block[0]|=bData[k>>>3]; }
  while (k>i){ k--; bShlIs(block,1); block[0]|=(bData[k>>>3]>>(k%8))&1; }
  return block;
}


function rsaSetBlock(bData, nrBits, block){
  if (bGetNrBits(block)>nrBits) throw('RSA: invalid key/data combination');
  var k=0;
  var m=1<<(j%8)
  while ((j%8!=0)&&(k<nrBits)){ bData[j>>>3]|=m*(block[0]&1); bShrIs(block,1); j++; k++; m<<=1; }
  while (k<nrBits-7){ bData[j>>>3]=block[0]&0xFF; bShrIs(block,8); j+=8; k+=8; }
  bData[j>>>3]=0;
  m=1; while (k<nrBits){ bData[j>>>3]|=m*(block[0]&1); bShrIs(block,1); j++; k++; m<<=1; }
}

function rsaDecrypt(key, bData){
  // FROM RSAINIT
  var err = 'RSA key: invalid format';
  if (key.length<2) throw(err);
  var expLen=key[0]+256*key[1];
  var modLen=key.length-2-expLen;
  if (modLen<1 || expLen<1) throw(err);
  var rsaExp = bFromBytes( key.slice(2, 2+expLen) );
  var rsaMod = bFromBytes( key.slice(2+expLen) );
  var rsaNbits = bGetNrBits(rsaMod);
  // END RSAINIT
  
  bData.splice(0,0,0);

  var j=0; 
  var i=8; 
  var tot=bData.length*8;
  var rsaExp2=[0];

  var rsaBlockIn;
  var rsaMaxi;

  if ( rsaExp2.length==1 && rsaExp2[0]==0 ){
    rsaBlockIn = rsaGetBlock(bData, rsaNbits)
    rsaMaxi = i+rsaNbits-1;
    rsaBlockOut=[1];
    rsaExp2=rsaExp.slice(0);
  }
  
  // FROM RSACRYPT
  if ( rsaExp2[0]&1 ) rsaBlockOut = bDiv( bMul(rsaBlockOut,rsaBlockIn), rsaMod )[1];
  rsaBlockIn = bDiv( bMul(rsaBlockIn,rsaBlockIn), rsaMod )[1];
  bShrIs(rsaExp2,1);
  // END RSACRYPT
  
  if (i<rsaMaxi) i++;
  if ( rsaExp2.length==1 && rsaExp2[0]==0 ){
    rsaSetBlock(bData, rsaNbits-1, rsaBlockOut);
    i=rsaMaxi+1;
    if (tot-i<rsaNbits) i=tot;
  }

  if (i>=tot){
    bData.length = (j+7)>>>3;

    var mod=(rsaNbits+13)>>>3;
    var r=getLen(getNrBits(mod-1));
    if (r>=mod) throw('invalid RSA data');
    while (bData.length%mod!=r) bData.pop();
  }
  
  return bData;
}
