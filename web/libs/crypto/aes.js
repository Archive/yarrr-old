// AES

var aesNb=4;
var aesNk;
var aesNr;

var aesInCo=[0xB,0xD,0x9,0xE];
var aesPows;
var aesLogs;
var aesSBox;
var aesSBoxInv;
var aesRco;
var aesFtable;
var aesRtable;
var aesFi;
var aesRi;
var aesFkey;
var aesRkey;

function aesPack(b){ return b[3]<<24|b[2]<<16|b[1]<<8|b[0]; }
function aesUnpack(w){ return [ w&0xFF, (w>>>8)&0xFF, (w>>>16)&0xFF, (w>>>24)&0xFF ] }
function aesMult(x, y){ return (x&&y) ? aesPows[(aesLogs[x]+aesLogs[y])%255]:0; }

function aesPackBlock() {
  var packed = new Array( aesNb );
  var j,k;
  for ( j=0,k=i; j<aesNb; j++,k+=4 )
    packed[j] = bData[k+3]<<24 | bData[k+2]<<16 | bData[k+1]<<8 | bData[k];
  return packed;
}

function aesUnpackBlock(packed){
  for ( var j=0; j<aesNb; j++,i+=4){
    w = packed[j]
    bData.splice( i, aesNb, w&0xFF, (w>>>8)&0xFF, (w>>>16)&0xFF, (w>>>24)&0xFF );
  }
}

function aesXTime(p){
  p <<= 1;
  return p&0x100 ? p^0x11B : p;
}

function aesSubByte(w){
  var b = aesUnpack(w);
  b[0]=aesSBox[b[0]];
  b[1]=aesSBox[b[1]];
  b[2]=aesSBox[b[2]];
  b[3]=aesSBox[b[3]];
  return aesPack(b);
}

function aesProduct(w1,w2){
  var b1 = aesUnpack(w1);
  var b2 = aesUnpack(w2);
  return aesMult(b1[0],b2[0])^aesMult(b1[1],b2[1])^aesMult(b1[2],b2[2])^aesMult(b1[3],b2[3]);
}

function aesInvMixCol(x){
  var m = aesPack(aesInCo);
  var b = new Array(4);
  b[3]=aesProduct(m,x)&0xFF; m=rotw(m,24);
  b[2]=aesProduct(m,x)&0xFF; m=rotw(m,24);
  b[1]=aesProduct(m,x)&0xFF; m=rotw(m,24);
  b[0]=aesProduct(m,x)&0xFF;
  return aesPack(b);
}

function aesByteSub(x){
  var y=aesPows[255-aesLogs[x]];
  x=y;  x=rotb(x,1);
  y^=x; x=rotb(x,1);
  y^=x; x=rotb(x,1);
  y^=x; x=rotb(x,1);
  y^=x; y^=0x63;
  return y;
}

function aesGenTables(){
  var i;
  aesPows = [ 1,3 ];
  aesLogs = [ 0,0,null,1 ];
  aesSBox = new Array(256);
  aesSBoxInv = new Array(256);
  aesFtable = new Array(256);
  aesRtable = new Array(256);
  aesRco = new Array(30);

  for ( i=2; i<256; i++){
    aesPows[i]=aesPows[i-1]^aesXTime( aesPows[i-1] );
    aesLogs[aesPows[i]]=i;
  }

  aesSBox[0]=0x63;
  aesSBoxInv[0x63]=0;
  for ( i=1; i<256; i++){
    var y=aesByteSub(i);
    aesSBox[i]=y; aesSBoxInv[y]=i;
  }

  for (i=0,y=1; i<30; i++){ aesRco[i]=y; y=aesXTime(y); }

  for ( i=0; i<256; i++){
    var y = aesSBox[i];
    aesFtable[i] = aesPack( [ aesXTime(y), y, y, y^aesXTime(y) ] );
    y = aesSBoxInv[i];
    aesRtable[i]= aesPack( [ aesMult(aesInCo[3],y), aesMult(aesInCo[2],y),
                             aesMult(aesInCo[1],y), aesMult(aesInCo[0],y) ] );
  }
}

var bDataSave;

function aesInit(){
  key = key.slice(0,256);
  var i,m;
  var j = 0;
  var l = key.length;

  while ( l!=16 && l!=24 && l!=32 ) key[l++]=key[j++];
  aesGenTables();

  aesNk = key.length >>> 2;
  aesNr = 6 + aesNk;

  var N=aesNb*(aesNr+1);

  aesFi = new Array(3*aesNb);
  aesRi = new Array(3*aesNb);
  aesFkey = new Array(N);
  aesRkey = new Array(N);

  for (m=j=0;j<aesNb;j++,m+=3){
    aesFi[m]=(j+1)%aesNb;
    aesFi[m+1]=(j+2)%aesNb;
    aesFi[m+2]=(j+3)%aesNb;
    aesRi[m]=(aesNb+j-1)%aesNb;
    aesRi[m+1]=(aesNb+j-2)%aesNb;
    aesRi[m+2]=(aesNb+j-3)%aesNb;
  }

  for (i=j=0;i<aesNk;i++,j+=4) aesFkey[i]=aesPack(key.slice(j,j+4));

  for (var k=0,j=aesNk;j<N;j+=aesNk,k++){
    aesFkey[j]=aesFkey[j-aesNk]^aesSubByte(rotw(aesFkey[j-1], 24))^aesRco[k];
    if (aesNk<=6)
      for (i=1;i<aesNk && (i+j)<N;i++) aesFkey[i+j]=aesFkey[i+j-aesNk]^aesFkey[i+j-1];
    else{
      for (i=1;i<4 &&(i+j)<N;i++) aesFkey[i+j]=aesFkey[i+j-aesNk]^aesFkey[i+j-1];
      if ((j+4)<N) aesFkey[j+4]=aesFkey[j+4-aesNk]^aesSubByte(aesFkey[j+3]);
      for (i=5;i<aesNk && (i+j)<N;i++) aesFkey[i+j]=aesFkey[i+j-aesNk]^aesFkey[i+j-1];
    }
  }

  for (j=0;j<aesNb;j++) aesRkey[j+N-aesNb]=aesFkey[j];
  for (i=aesNb;i<N-aesNb;i+=aesNb){
    k=N-aesNb-i;
    for (j=0;j<aesNb;j++) aesRkey[k+j]=aesInvMixCol(aesFkey[i+j]);
  }
  for (j=N-aesNb;j<N;j++) aesRkey[j-N+aesNb]=aesFkey[j];
}

function aesFreeMem(){
  aesPows=aesLogs=aesSBox=aesSBoxInv=aesRco=null;
  aesFtable=aesRtable=aesFi=aesRi=aesFkey=aesRkey=null;
}

function aesRounds( block, key, table, inc, box ){
  var tmp = new Array( aesNb );
  var i,j,m;

  for ( r=0; r<aesNb; r++ ) block[r]^=key[r];
  for ( i=1; i<aesNr; i++ ){
    for (j=m=0;j<aesNb;j++,m+=3){
      tmp[j]=key[r++]^table[block[j]&0xFF]^
             rotw(table[(block[inc[m]]>>>8)&0xFF], 8)^
             rotw(table[(block[inc[m+1]]>>>16)&0xFF], 16)^
             rotw(table[(block[inc[m+2]]>>>24)&0xFF], 24);
    }
    var t=block; block=tmp; tmp=t;
  }

  for (j=m=0;j<aesNb;j++,m+=3)
    tmp[j]=key[r++]^box[block[j]&0xFF]^
           rotw(box[(block[inc[m  ]]>>> 8)&0xFF], 8)^
           rotw(box[(block[inc[m+1]]>>>16)&0xFF],16)^
           rotw(box[(block[inc[m+2]]>>>24)&0xFF],24);
  return tmp;
}

function aesEncrypt(cbc){
  if (tot==0){
    prgr = 'AES'+(cbc?'c':'e');
    if (key.length<1) return;
    if (lenInd) insLen( bData.length%(aesNb*4), getNrBits(aesNb*4-1) );
    if (cbc) for (i=0; i<aesNb*4; i++) bData.unshift( Math.floor(Math.random()*256) );
    while( bData.length%(aesNb*4)!=0 ){ bData.push(0); }
    tot = bData.length;
    aesInit();
  }else{
    if (cbc) for (j=i; j<i+aesNb*4; j++) bData[j] ^= bData[j-aesNb*4];
    aesUnpackBlock( aesRounds(aesPackBlock(), aesFkey, aesFtable, aesFi, aesSBox ) );
  }
  if (i>=tot) aesFreeMem();
}

function aesDecrypt(cbc){
  if (tot==0){
    prgr = 'AES'+(cbc?'c':'e');
    if (key.length<1) return;
    if (cbc) i=aesNb*4;
    tot = bData.length;
    if ( (tot%(aesNb*4)) || tot<i ) throw 'AES: Incorrect length';
    aesInit();
  }else{
    if (cbc) i=tot-i;
    aesUnpackBlock( aesRounds(aesPackBlock(), aesRkey, aesRtable, aesRi, aesSBoxInv ) );
    if (cbc){
      for (j=i-aesNb*4; j<i; j++) bData[j] ^= bData[j-aesNb*4] ;
      i = tot+aesNb*8-i;
    }
  }
  if (i>=tot){
    if (cbc) bData.splice(0,aesNb*4);
    if (lenInd){
      var ol = bData.length;
      var k = getLen(getNrBits(aesNb*4-1));
      while((k+ol-bData.length)%(aesNb*4)!=0) bData.pop();
    }
    else while(bData[bData.length-1]==0) bData.pop();;
    aesFreeMem();
  }
}

function aeseEncrypt(){ aesEncrypt(false); }
function aeseDecrypt(){ aesDecrypt(false); }
function aescEncrypt(){ aesEncrypt(true); }
function aescDecrypt(){ aesDecrypt(true); }
