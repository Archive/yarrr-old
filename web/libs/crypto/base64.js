
// Base64

function b64Encrypt(){
  if (i==0) { prgr='Base64'; sData=""; tot=bData.length; }
  var z=Math.min(i+100,tot);
  while (i<z){
    var x = [ bData[i]>>2, (bData[i]&3)<<4, 64, 64 ];
    if (++i<bData.length){x[1]+=(bData[i]&240)>>4;x[2]=(bData[i]&15)<<2;}
    if (++i<bData.length){x[2]+=(bData[i]&192)>>6;x[3]=bData[i]&63;}
    for ( j in x ){
      var y=x[j];
      sData += String.fromCharCode(y<26?65+y:y<52?71+y:y<62?y-4:y<63?43:y<64?47:61);
    }
    i++;
  }
}

function b64Decrypt(){
  var x = new Array(4);
  var z = i+100;
  if (i==0){ prgr='Base64'; j=0; tot=sData.length; bData=[]; }
  while(i<z){
    for (var k=0;k<4;k++){
      var n = sData.substr(i).search(/\S/);
      i = n<0?tot:i+n;
      if (i==tot){
        if (k!=0) throw( "Base64: unexpected #chars." );
        return;
      }
      var c = sData.charCodeAt(i++);
      x[k] = c==43?62:c==47?63:c==61?64:c>47&&c<58?c+4:c>64&&c<91?c-65:c>96&&c<123?c-71:-1;
      if (x[k]<0||(x[k]==64&&k<2)) throw( "Base64: "+unExpChar(c)
        +"\nAllowed characters:\n['A'-'Z'], ['a'-'z'], ['0'-'9'], '+', '-' and '='"  );
    }
    bData[j++] = (x[0]<<2)+(x[1]>>4);
    if (x[2]<64) bData[j++] = ((x[1]&15)<<4)+(x[2]>>2);
    if (x[3]<64) bData[j++] = ((x[2]&3)<<6)+x[3];
  }
}

function setKey(p, b64){
  var tmp = bData;
  if (b64){
    sData=p.replace(/\s/g,'');
    if (sData.length%4==1) sData+='A';
    while (sData.length%4) sData+='=';
  }
  else sData=p;
  i=tot=0;
  do{ if (b64) b64Decrypt(); else utf8Encrypt(); } while (i<tot);
  key = bData;
  bData = tmp;
}

function bytesToB64(b){
  bData=b;
  i=tot=0;
  do{ b64Encrypt() } while (i<tot);
  return sData.replace(/(.{40})/g,'$1\n');
}
