// Uses DOM

var debugWindow = null;

function ensureDebugWindowExists() {
  if (debugWindow == null) {
    debugWindow = window.open();
    debugWindow.document.write("<html><head><title>Yarrr Debug Events</title></head><body>");
  }  
}

function reportEvent(details) {
  try {
    ensureDebugWindowExists();
    debugWindow.document.write("<p><b>Event (at " + new Date() + "):</b><br/><pre>");
    debugWindow.document.write(details);
    debugWindow.document.write("</pre></p>");
  } catch (e) {
  }
}

function reportError(error) {
  try {
    ensureDebugWindowExists();
    debugWindow.document.write("<p><b>Error (at " + new Date() + "):</b><br/><pre>");
    debugWindow.document.write(error);
    debugWindow.document.write("</pre></p>");
  } catch (e) {
  }
}
