// Uses DOM

function displayAllTopics() {
  //reportEvent("displayAllTopics(\"" + pendingTopicHTML + "\")");
  topics = document.getElementById("topics");
  topics.innerHTML = pendingTopicHTML;
  pendingTopicHTML = "";
}


function changeToTopic(topicID) {	
	if (topicID!=currentTopicID) {
		currentTopicID=topicID;	
		
		messages = document.getElementById("messages");
		messages.innerHTML = "";
				
		lastUpdateTime = new Date("7/10/1981");
		initiateUpdate();
	}
}

function appendTopic(name,description, topicID) {
  var isSelected = (topicID==currentTopicID);
  
  topic = "<div class=\"";
  if (isSelected) {
	  topic += "selected-topic";
  } else {
	  topic += "unselected-topic";
  }
  topic += "\">";
  
  topic +=   "<a href=\"javascript:changeToTopic(" + topicID + ");\" class=\"topic-name\">";
  topic +=     name;
  topic +=   "</a>";
  
  /* Don't show descriptions for now:
  topic +=   "<div class=\"topic-desc\">";
  topic +=     description;
  topic +=   "</div>";  
  */
  
  topic += "</div>";
  
  pendingTopicHTML += topic;
}

function addNewTopics(newTopics) {
  if (newTopics.length <= 0) return;
  for (var i = 0; i < newTopics.length; i++) {
    var topic = newTopics[i];
    appendTopic(topic.name, "" /*FIXME: no topic.description currently*/, topic.id);
  }
  displayAllTopics()
}

var topics = null;
var currentTopicID = null;