// Uses DOM

function getPrettyDate(date) {
  var weekday = new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
  return weekday[date.getDay()] + " @ " + date.getHours() + ":" + date.getMinutes();
}

function getHeader(author, subject, date) {
  val = "<div class='messageHeader'>";
  val +=    "<div class='subject_from'>";
  val +=        getSubject(subject);
  val +=        getFrom(author);
  val +=    "</div>";
  val +=    "<div class='date'>";
  val +=        getPrettyDate(date);
  val +=    "</div>";
  val += "</div>";
  return val;
}

function getFrom(author) {
  return "<div class='from'>" + author + "</div>";
}

function getSubject(subject) {
  return "<div class='subject'>" + subject + "</div>";
}

function getContents(contents, mimetype) {
  // FIXME: only works w/ text/plain
  return "<div class='content'>" + contents + "</div>";
}

function getMessageTag(date) {
  if (date > lastReadMessages) {
    return "<div class='entry'>";
  } else {
    return "<div class='readEntry'>";
  }
}

function getRemoveButton(messageID) {
  return "<div align='right' class='removeButton'><a href=\"javascript:removeMessage('" + messageID + "');\">Remove</a></div>"
}

var pendingMessageHTML = "";
var pendingTopicHTML = "";

function displayPendingMessages() {
  messages = document.getElementById("messages");
  var localPendingMessageHTML = pendingMessageHTML;
  pendingMessageHTML = "";
  messages.innerHTML = localPendingMessageHTML + messages.innerHTML;
 
  //setCookie("lastReadMessages", date, new Date(timestamp.getTime() + 157784630000L));
  setCookie("lastReadMessages", lastUpdateTime, new Date("7/10/2081"));
}

function appendPendingMessage(message, contents) {
  var messageHTML;
  messageHTML  = getMessageTag(message.sentTime); 
  messageHTML +=    getHeader(message.authorName, message.subject, message.sentTime);
  messageHTML +=    "<br clear='all'>"
  messageHTML +=    getContents(contents, message.mimetype);
  messageHTML +=    getRemoveButton(message.id);
  messageHTML += "</div><br/><br/>";

  pendingMessageHTML = messageHTML + pendingMessageHTML;
}

function addNewMessages(newMessages) {
  for (var i = 0; i < newMessages.length; i++) {
    var message = newMessages[i];
    contents = yarrr.getResourceAsUTF8(message.documentID, "main");
    appendPendingMessage(message, contents);
  }
  displayPendingMessages();
}