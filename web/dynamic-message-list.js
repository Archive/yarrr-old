function toggleVisible(elementId) {
  var element = document.getElementById(elementId);
  if (element.style.display == 'block' || element.style.display == '') {
    element.style.display = 'none';
  } else {
    element.style.display = 'block';
  }
}

function setActiveUserList(activeUsers) {
  activeUserListHTML = "";
  for (var i = 0; i < activeUsers.length; i++) {
    activeUserListHTML += "<div class='activeUserEntry'>" + activeUsers[i].name + "</div>";
  }
  
  document.getElementById("activeUserList").innerHTML = activeUserListHTML;
}

function heartbeat() {
  var callback = function (retval, err) {
    if (err != null) reportException(err);
    statusBox = document.getElementById("statusBox");
    statusBox.innerHTML = statusBox.innerHTML + ".";
  }
  yarrr.heartbeat(wrapAsBase64(getPublicKey()), callback);
}

function initiateUpdate() {
  if (!requestPending) {
    requestPending = true;

    update();
    heartbeat();
       
    setTimeout("initiateUpdate()", 5000);
  }
}

var lastUpdateTime = new Date("7/10/1981");
var lastReadMessages;
var publicKey = getCookie("publicKey");

document.poster.author.value = getCookie("author");

lastReadMessagesString = getCookie("lastReadMessages");
if (lastReadMessagesString != null) {
    lastReadMessages = new Date(lastReadMessagesString);
} else {
	lastReadMessages = new Date();
}

updateTopics();

